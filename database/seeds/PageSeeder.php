<?php

	use \Illuminate\Support\Collection;
	use App\Models\Page\Page;
	use App\Models\Page\PageLang;
	use App\Repositories\PageRepository;

	class PageSeeder extends AbstractLanguageableSeeder
	{
		private $existsPages;
		/**
		 * @var PageRepository
		 */
		private $repository;
		/**
		 * @var \Faker\Generator
		 */
		private $factory;

		public function __construct(PageRepository $repository, \Faker\Generator $factory)
		{
			parent::__construct();
			$this->setExistsPages($repository->all());
			$this->repository = $repository;
			$this->factory = $factory;
		}

		/**
		 * @throws \Illuminate\Contracts\Container\BindingResolutionException
		 */
		public function run()
		{
			$pages = [
				[
					'notification'    => 'На данной странице не используется изображение',
					'name'            => 'Главная',
					'url'             => '/',
					'title'           => 'Merais group - современная логистическая компания с полным циклом услуг, а так же надеждный партнёр',
					'page_type'       => 'main',
					'description'     => ' Merais group - надежная компания, которой доверяют лидеры рынка. Мы знаем, как важно для клиента вовремя получать продукт, поэтому довели до совершенства каждый этап. Работаем оперативно, качественно и уверенно с соблюдением сроков. Гарантируем прозрачность сотрудничества и консультирование клиентов в удобное время. Решаем вопросы, связанные с доставкой. Заказчик занимается производственными процессами и повышением продаж. ',
					'sub_description' => ' Профессионалы в логистической отраслиПродуманная логистика – это успешное развитие любой производственной и торговой компании.Мы совершенствуемся и идем в ногу со временем. Наши сотрудники повышают квалификацию на семинарах, тренингах, конференциях и курсах, а внутри компании проводятся круглые столы и мозговые штурмы. ',
					'manual'          => false,
				],
				[
					'notification'    => 'На данной странице не используется описание и доп. описание, а так же изображение',
					'name'            => 'Контакты',
					'url'             => '/contacts',
					'title'           => 'Контакты',
					'page_type'       => 'contacts',
					'description'     => 'Не используется на данной странице',
					'sub_description' => 'Не используется на данной странице',
					'manual'          => false,
				],
				[
					'name'            => 'О компании',
					'url'             => '/about',
					'title'           => 'О компании',
					'image'           => url('/images/slide1.jpg'),
					'page_type'       => 'about',
					'description'     => $this->factory->text(500),
					'sub_description' => $this->factory->text(500),
					'manual'          => false,
				],
			];
			$this->loop($pages);
		}

		/**
		 * @param array     $pages
		 * @param Page|null $parentPage
		 * @throws \Illuminate\Contracts\Container\BindingResolutionException
		 */
		private function loop(array $pages, Page $parentPage = null)
		{
			foreach ($pages as $page) {
				if ($this->pageExistsByUrl($page['url'] ?? '')) {
					continue;
				}
				$pageModel = $this->createPage($page, $parentPage);
				if (Arr::has($page, 'pages')) {
					$this->loop(Arr::get($page, 'pages'), $pageModel);
				}
			}
		}

		/**
		 * @param array     $pageData
		 * @param Page|null $parentPage
		 * @return Page|mixed
		 * @throws \Illuminate\Contracts\Container\BindingResolutionException
		 */
		private function createPage(array $pageData, Page $parentPage = null): Page
		{
			$pageData = array_merge(['manual' => 1], $pageData);
			if ($parentPage) {
				$pageData['parent_id'] = $parentPage->getKey();
			}
			$pageModel = $this->repository->create($pageData);

			return $pageModel;
		}

		private function setExistsPages(Collection $collection)
		{
			$this->existsPages = $collection->keyBy('url');
		}

		private function pageExistsByUrl(string $url)
		{
			return $this->existsPages->offsetExists($url);
		}
	}
