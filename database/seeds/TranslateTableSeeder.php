<?php

use App\Builders\Translate\TranslateBuilder;
use App\Models\Language;
use App\Repositories\TranslateRepository;
use Illuminate\Database\Seeder;
use \App\Models\Translate\Translate;
use \App\Models\Translate\TranslateLang;

class TranslateTableSeeder extends Seeder
{
	private $translates;
	/**
	 * @var TranslateRepository
	 */
	private $repository;
	/**
	 * @var \Faker\Generator
	 */
	private $factory;

	public function __construct(TranslateRepository $repository, \Faker\Generator $factory)
	{
		$this->translates = $repository->all()->keyBy('key');
		$this->repository = $repository;
		$this->factory = $factory;
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$groups = [
			'global' => [
				'show-more'    => 'Показать еще',
				'back'         => 'назад',
				'yes'          => 'Да',
				'no'           => 'Нет',
				'download'     => 'Скачать',
				'404'          => 'Страница не найдена',
				$this->getBuilder('404-description')->setValue('Страница не найдена, попробуйте другой адресс')->setTypeTextarea()->build(),
				'view-details' => 'Подробнее',
				'see-more'     => 'Показать еще',
				'work-hours'   => 'График работы:',
				$this->getBuilder('home-bread-name')->setValue('Главная')->setDisplayName('Название сайта в хлебных крошках')->build(),
			],

			'forms'           => [
				'fio'     => 'ФИО',
				'name'    => 'Имя',
				'phone'   => 'Телефон',
				'email'   => 'E-mail',
				'message' => 'Сообщение',
				'send'    => 'Отправить',
			],
			'pages'           => [
				'about'    => 'О нас',
				'contacts' => 'Контакты',
			],
			'main-advantages' => [
				$this->getBuilder('first')->setValue('Тщательно подобранные специалисты с опытом работы в сфере логистики')->build(),
				$this->getBuilder('second')->setValue('Автоматизированные логистические процессы')->build(),
				$this->getBuilder('third')->setValue('Современный автомобильный парк')->build(),
			],
			'main'            => [
				$this->getBuilder('about.title')->setValue('О компании ')->build(),
			],
			'services'        => [
				$this->getBuilder('title')->setValue('Услуги')->build(),
				$this->getBuilder('others')->setValue('Другие услуги')->build(),
			],
			'advantages'      => [
				$this->getBuilder('first.title')->setValue('Гибкость')->build(),
				$this->getBuilder('first.description')->setValue('У нас сложился разнообразный опыт: от деревообрабатывающей промышленности до пищевых продуктов. ')->build(),
				$this->getBuilder('second.title')->setValue('Скорость')->build(),
				$this->getBuilder('second.description')->setValue('Так же есть опыт в урегулировании страховых случаев различной тяжести, общей аварии в мореходстве.')->build(),
				$this->getBuilder('third.title')->setValue('Безопасность')->build(),
				$this->getBuilder('third.description')->setValue('В нашем рынке легко попасть на различные простои и мы понимаем, что это первое на чем надо экономить. ')->build(),
			],

			'feedback'       => [
				'throttle-message'  => 'Отправлять заявку можно не чаще 1 раза в минуту.',
				'send-success'      => 'Спасибо за заявку',
				'send-success-sub'  => 'Мы скоро с вами свяжемся',
				'send-btn-close'    => 'Понятно',
				'send-failed'       => 'Заявка не была отправлена, произошла ошибка на сервере, пожалуйста попробуйте позже, или позвоните нам по одному из номеров телефона',
				'modal-description' => 'Заполните поля ниже и мы свяжемся с вами в ближайшее время',
				'title-contact-us'  => 'Связаться с нами',
			],
			'contacts'       => [
				'phone'   => 'Телефон',
				'email'   => 'Почта',
				'address' => 'Адрес',
			],
			'about'          => [
				'leads'             => 'Руководители',
				'leads.title'       => 'Мы открылись в 2013 году. Два учредителя - Анна и Мария. ',
				'leads.description' => 'На тот момент у Марии был опыт в работе с клиентами, в том время как Анна - финансовый профессионал. Соединив экспертизу в итоге образовался хороший тандем - семейный бизнес стратега и тактика.  нас очень высокие требования и стандарты в первую очередь к себе, что и транслируется в бизнес. Мы не приходим к клиентам со словом «невозможно», мы ищем варианты как выполнить задачу.  работаем быстро, так как понимаем что если наши клиенты в условиях жесткой конкуренции сегодня не продадут, завтра мы не повезем. Мы вовлекаемся в работу с клиентом по всей цепочке и тесно взаимодействуем по каждой его сделке. ',
			],
			'service-detail' => [
				$this->getBuilder('title')->setValue('Детальнее о подходе ')->build(),
				$this->getBuilder('text')->setValue($this->factory->text(500))->setTypeTextarea()->build(),
				$this->getBuilder('text-two')->setValue($this->factory->text(500))->setTypeTextarea()->build(),
			],
			'main-counters'  => [
				$this->getBuilder('first')->setDisplayName('Первый счетчик')->setValue('Лет опыта')->build(),
				$this->getBuilder('second')->setDisplayName('Второй счетчик')->setValue('Сотрудников')->build(),
				$this->getBuilder('third')->setDisplayName('Третий счетчик')->setValue('Стран')->build(),
			],

		];
		foreach ($groups as $groupName => $group) {
			foreach ($group as $key => $item) {
				if (!is_array($item)) {
					$item = ['key' => $key, 'value' => $item];
				}
				$item['group'] = $groupName;
				$item['key'] = $this->addPrefixGroupToKey($item['key'], $groupName);
				if ($this->translateExists($item['key'])) {
					continue;
				}
				$translate = $this->repository->create($item);
				$this->addTranslate($translate->getAttribute('key'), $translate);
			}
		}
	}

	private function getTranslateByKey(string $key): ?Translate
	{
		return \Arr::get($this->translates, $key);
	}

	private function translateExists(string $key): bool
	{
		return (bool)$this->getTranslateByKey($key);
	}

	private function addPrefixGroupToKey(string $key, string $prefix): string
	{
		return implode('.', [$prefix, $key]);
	}

	private function getBuilder(?string $key = null): TranslateBuilder
	{
		$builder = new TranslateBuilder();
		if ($key) {
			$builder->setKey($key);
		}
		return $builder;
	}

	private function addTranslate(string $key, Translate $translate): void
	{
		$this->translates->offsetSet($key, $translate);
	}
}
