<?php /** @var $services \Illuminate\Support\Collection*/ ?>
<?php /** @var $service App\Models\Service\Service */ ?>


@isset($services)
    @if($edit->service ?? null)
        <label> <a href="{{ urlEntityEdit($edit->service) }}">{{ __('modules.service.title_singular') }}</a></label>
    @else
        <label>{{ __('modules.service.title_singular') }}</label>
    @endif
    <select name="service_id" class="form-control selectpicker" data-live-search="true" autocomplete="off">
        <option>Выберите услугу</option>
        @foreach($services as $service)
            @php
            	$isSelected = ($edit->service_id ?? $utmServiceId ?? null) == $service->getPrimaryValue()
            @endphp
            <option {!! selectedIfTrue($isSelected) !!} value="{{ $service->id }}">{{ $service->name }}</option>
        @endforeach
    </select>
@endisset
