<?php

	namespace App\Http\Controllers\Auth;

	use App\Http\Controllers\Controller;
	use App\Http\Controllers\SiteController;
	use App\Providers\RouteServiceProvider;
	use App\Repositories\UserRepository;
	use App\Services\User\Reset\UserResetStepsService;
	use Illuminate\Foundation\Auth\ResetsPasswords;
	use Illuminate\Http\Request;

	class ResetPasswordController extends SiteController
	{
		/*
		|--------------------------------------------------------------------------
		| Password Reset Controller
		|--------------------------------------------------------------------------
		|
		| This controller is responsible for handling password reset requests
		| and uses a simple trait to include this behavior. You're free to
		| explore this trait and override any methods you wish to tweak.
		|
		*/

		use ResetsPasswords;

		/**
		 * @var UserRepository
		 */
		private $userRepository;
		/**
		 * @var UserResetStepsService
		 */
		private $userResetStepsService;

		public function __construct(UserRepository $userRepository, UserResetStepsService $userResetStepsService)
		{
			parent::__construct();
			$this->userRepository = $userRepository;
			$this->userResetStepsService = $userResetStepsService;
		}

		/**
		 * Where to redirect users after resetting their password.
		 *
		 * @var string
		 */
		protected $redirectTo = RouteServiceProvider::HOME;

		public function showResetForm(Request $request, $token = null)
		{
			if (!$this->userResetStepsService->isPhoneConfirmed()) {
				return  $this->onNotConfirmed();
			}
			$data['content'] = view('auth.passwords.reset');
			return $this->main($data);
		}

		protected function rules()
		{
			return [
				'password' => 'required|confirmed|min:8',
			];
		}

		public function reset(Request $request)
		{
			if (!$this->userResetStepsService->isPhoneConfirmed()) {
				return  $this->onNotConfirmed();
			}
			$request->validate($this->rules(), $this->validationErrorMessages());
			$user = $this->userRepository->find($this->userResetStepsService->getUserId());
			$password = $request->get('password');
			$this->resetPassword($user, $password);
			$this->setSuccessMessage('Пароль успешно изменен');
			return redirect(route('cabinet.purchases'))->with($this->getResponseMessage());
		}

		private function onNotConfirmed()
		{
			$this->setFailMessage('Телефон не подтвержден');
			return redirect(route('password.reset.confirm'))->with($this->getResponseMessage());
		}
	}
