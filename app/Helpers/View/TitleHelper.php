<?php


	namespace App\Helpers\View;


	use Illuminate\Support\Arr;
	use Illuminate\Support\Str;

	class TitleHelper
	{

		public static function resolve(?string $title = ''): self
		{
			return app(self::class)->setTitle($title);
		}

		private $limitBreak = 18;

		/** @var string */
		private $title;

		/**
		 * @return string
		 */
		public function getTitle(): string
		{
			return $this->title;
		}

		/**
		 * @param string $title
		 */
		public function setTitle(string $title): self
		{
			$this->title = $title;
			return $this;
		}

		public function isNeedSplit()
		{
			return $this->getLength() > $this->limitBreak;
		}

		public function isDoesntNeedSplit()
		{
			return !$this->isNeedSplit();
		}

		public function getFirstChapter()
		{
			if ($this->isDoesntNeedSplit()) {
				return $this->getTitle();
			}
			$words = array_reverse(explode(' ', $this->getTitle()));
			foreach ($words as $index => $word) {
				unset($words[ $index ]);
				if (Str::length(implode(' ', $words)) > $this->limitBreak) {
					continue;
				}
				return trim(implode(' ', array_reverse($words)));
			}
		}

		public function getSecondChapter()
		{
			if ($this->isDoesntNeedSplit()) {
				return '';
			}
			return Str::of($this->getTitle())->replaceFirst($this->getFirstChapter(), '')->trim();
		}

		public function getLength(): int
		{
			return Str::length($this->title);
		}

		/**
		 * @param int $limitBreak
		 */
		public function setLimitBreak(int $limitBreak): void
		{
			$this->limitBreak = $limitBreak;
		}

		public function getNthWord(int $nth)
		{
			$words = $this->getWords();

			return Arr::get($words, $nth - 1, '');
		}

		public function getNthWords(int $nth): array
		{
			$words = $this->getWords();

			return array_slice($words, 0, $nth);
		}

		public function getLastNthWords(int $nth): array
		{
			$words = $this->getWords();

			return array_slice($words, $nth, -1);
		}

		public function getLastNthWordsString(int $nth): string
		{
			$words = $this->getWords();

			return $this->toString(array_slice($words, -$nth, $nth, true));
		}

		public function getNthWordsString(int $nth): string
		{
			return $this->toString($this->getNthWords($nth));
		}

		public function getFirstWord()
		{
			return $this->getNthWord(1);
		}

		public function getWithoutFirstWord()
		{
			return trim(Str::replaceFirst($this->getFirstWord(), '', $this->getTitle()));
		}

		public function getWithoutFirstNthWords(int $num): string
		{
			return trim(Str::replaceFirst($this->getNthWordsString($num), '', $this->getTitle()));
		}

		public function getWithoutLastNthWordsString(int $num): string
		{
			return trim(Str::replaceLast($this->getLastNthWordsString($num), '', $this->getTitle()));
		}

		public function getLengthFirstWord()
		{
			return Str::of($this->getTitle())->words(1)->length();
		}

		public function getLengthToLastNthWords(int $nth): int
		{
			$lastWords = $this->getLastNthWordsString($nth);
			$lastWordsLen = Str::length($lastWords);

			return $this->getLength() - $lastWordsLen;
		}

		public function getWords(): array
		{
			return explode(' ', $this->getTitle());
		}

		public function countWords(): int
		{
			return count($this->getWords());
		}

		private function toString(array $array): string
		{
			return implode(' ', $array);
		}

	}