<?php

namespace App\Traits;


use App\Contracts\HasLocalized;
use App\Helpers\Debug\LoggerHelper;

trait EloquentExtend
{

	protected static $schema = [];

	public function getUnguardedColumns(): array
	{
		return array_diff($this->getTableColumns(), $this->getGuarded());
	}

	public function getTableColumns()
	{
		if (!\Arr::has(static::$schema, $this->getTable())) {
			$columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
			\Arr::set(static::$schema, $this->getTable(), $columns);
		}
		return \Arr::get(static::$schema, $this->getTable());
	}


	/**
	 * @param array $attributes
	 * @param bool $overrideFilled
	 * @return $this
	 */
	public function fillExisting(array $attributes, $overrideFilled = true): self
	{

		$schema = array_flip($this->getTableColumns());
		$attributes = array_filter($attributes, function ($value, $column) use ($schema) {
			$exists = \Arr::exists($schema, $column);
			$isGuarded = $this->isGuarded($column);
			return ($exists && !$isGuarded);
		}, ARRAY_FILTER_USE_BOTH);

		foreach ($attributes as $attribute => $value) {
			if (!$overrideFilled && $this->isDirty($attribute)){
				continue;
			}
			$this->setAttribute($attribute, $value);
		}

		return $this;
	}

	public function getKey()
	{
		$key = $this->getKeyName();
		$key = \Arr::wrap($key);
		$key = array_shift($key);
		return $this->getAttribute($key);
	}

	public static function findByUrl(string $url): ?self
	{
		return self::whereUrl($url)->first();
	}


	public function getLangColumn(string $column)
	{
		try{
			if (classImplementsInterface($this, HasLocalized::class) && $this->lang) {
				return $this->lang->{$column} ?? '';
			}
			if (\Arr::has($this->attributes, $column)) {
				return \Arr::get($this->attributes, $column);
			}
		} catch (\Exception $e){
		    app(LoggerHelper::class)->error($e);
		}
		return '';
	}

}

