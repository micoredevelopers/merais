<script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js" integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>
{{--Seo данные для всех страниц, выводятся из админки, Настройки -> вкладка "seo" --}}
{!! getSetting('seo.footer-global-codes') !!}

@yield('js-before')

@stack('js-before')

@include('public.layout.includes.assets.js-libraries')

@include('partials.flash-message')

{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>--}}

{{-- Scripts from another templates, which "extends" this --}}
@yield('js')

@stack('js')


<script defer>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>
<script>
    AOS.init();
</script>


<!-- meta footer -->
{!! showMeta('', 'footer') !!}
<!-- end meta footer -->