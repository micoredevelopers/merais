<?php

	namespace App\Repositories;

	use App\Criteria\ActiveCriteria;
	use App\Criteria\SortCriteria;
	use App\Models\Advantage\Advantage;
	use App\Models\Advantage\AdvantageLang;


	class AdvantageRepository extends AbstractRepository
	{
		public function modelLang()
		{
			return AdvantageLang::class;
		}

		public function model()
		{
			return Advantage::class;
		}

		public function addPublicCriteriaToQuery(): self
		{
			$this->pushCriteria($this->app->make(SortCriteria::class));
			$this->pushCriteria($this->app->make(ActiveCriteria::class));
			return $this;
		}

		public function addAdminCriteriaToQuery()
		{
			$this->pushCriteria($this->app->make(SortCriteria::class));
		}

		public function getListAdmin()
		{
			$this->addAdminCriteriaToQuery();
			return $this->with('lang')->paginate();
		}

		public function getListPublic()
		{
			$this->addPublicCriteriaToQuery();
			return $this->with('lang')->get();
		}

	}

