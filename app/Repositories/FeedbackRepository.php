<?php

	namespace App\Repositories;

	use App\Models\Feedback\Feedback;
	use Illuminate\Support\Collection;


	class FeedbackRepository extends AbstractRepository
	{

		public function model()
		{
			return Feedback::class;
		}

		public function addAdminCriteriaToQuery()
		{

		}

		public function getListAdmin()
		{
			$this->addAdminCriteriaToQuery();
			return $this->latest()->paginate();
		}

		public function getByType(?string $type = '' ): Collection
		{
			$this->addAdminCriteriaToQuery();
			return $this->where('type', $type)->paginate();
		}




	}

