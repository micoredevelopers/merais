<?php

	namespace App\Http\Controllers\Admin;

	use App\Helpers\Media\ImageRemover;
	use App\Http\Requests\Admin\AdvantageRequest;
	use App\Models\Advantage\Advantage;
	use App\Repositories\AdvantageRepository;
	use App\Traits\Authorizable;
	use App\Traits\Controllers\SaveImageTrait;

	class AdvantageController extends AdminController
	{
		use Authorizable;
		use SaveImageTrait;

		protected $withThumbnails;

		private $name;

		protected $key = 'advantages';

		protected $routeKey = 'admin.advantages';

		protected $permissionKey = 'advantages';
		/**
		 * @var AdvantageRepository
		 */
		private $repository;

		public function __construct(AdvantageRepository $repository)
		{
			parent::__construct();
			$this->withThumbnails = false;
			$this->name = __('modules.advantages.title');
			$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
			$this->shareViewModuleData();
			$this->repository = $repository;
		}

		public function index()
		{
			$this->setTitle($this->name);
			$vars['list'] = $this->repository->getListAdmin();
			$data['content'] = view('admin.advantages.index', $vars);
			return $this->main($data);
		}

		public function create()
		{
			$data['content'] = view('admin.advantages.create');
			return $this->main($data);
		}

		public function store(AdvantageRequest $request)
		{
			$input = $request->except('image');
			if ($advantage = $this->repository->create($input)) {
				$this->setSuccessStore();
				$this->saveImage($request, $advantage);
				$this->fireEvents();
			}
			return $this->redirectOnCreated($advantage);
		}

		public function edit(Advantage $advantage)
		{
			$edit = $advantage;
			$title = $this->titleEdit($edit);
			$this->addBreadCrumb($title)->setTitle($title);

			$data['content'] = view('admin.advantages.edit', compact(
				'edit'
			));
			return $this->main($data);
		}

		public function update(AdvantageRequest $request, Advantage $advantage)
		{
			$input = $request->except('image');
			$this->saveImage($request, $advantage);

			if ($this->repository->update($input, $advantage)) {
				$this->setSuccessUpdate();
				$this->fireEvents();
			}

			return $this->redirectOnUpdated($advantage);
		}

		public function destroy(Advantage $advantage, ImageRemover $imageRemover)
		{
			if ($this->repository->delete($advantage->id)) {
				$imageRemover->removeImage($advantage->image);
				$this->setSuccessDestroy();
				$this->fireEvents();
			}

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		private function fireEvents()
		{
		}
	}
