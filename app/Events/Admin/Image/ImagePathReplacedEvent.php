<?php

namespace App\Events\Admin\Image;

use App\Models\Model;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ImagePathReplacedEvent
{
	use Dispatchable, InteractsWithSockets, SerializesModels;
	/**
	 * @var Model
	 */
	private $model;
	/**
	 * @var string|null
	 */
	private $requestKey;

	/**
	 * Create a new event instance.
	 *
	 * @param Model $model
	 */
	public function __construct(Model $model, ?string $requestKey = null)
	{
		$this->model = $model;
		$this->requestKey = $requestKey;
	}


	public function getModel(): Model
	{
		return $this->model;
	}

	/**
	 * @return string|null
	 */
	public function getRequestKey(): ?string
	{
		return $this->requestKey;
	}
}
