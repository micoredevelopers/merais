<?php /** @var $advantages \Illuminate\Support\Collection | \App\Models\Advantage\Advantage[] */ ?>
@if($advantages ?? false)
    <section class="information-section">
        <div class="container">
            @foreach($advantages as $advantage)
                @if($loop->odd)
                    @includeIf('public.home.includes.advantages.left')
                @else
                    @includeIf('public.home.includes.advantages.right')
                @endif
            @endforeach
        </div>
    </section>
@endif