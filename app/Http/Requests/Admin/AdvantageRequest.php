<?php

	namespace App\Http\Requests\Admin;

	use App\Builders\Validation\ValidationRulesBuilder;
	use App\Http\Requests\AbstractRequest;

	class AdvantageRequest extends AbstractRequest
	{
		protected $toBooleans = ['active'];

		public function rules()
		{
			$builder = app(ValidationRulesBuilder::class);
			return $builder
				->image()
				->name()
				->description()
				->build();

			return $rules;
		}

	}
