<?php

use Illuminate\Database\Seeder;

class AbstractLanguageableSeeder extends Seeder
{
	/**
	 * @var \App\Models\Language[]|\Illuminate\Database\Eloquent\Collection
	 */
	protected $languages;

	public function __construct()
	{
		$this->languages = \App\Models\Language::all();
	}
}
