<footer>
    <div class="callback-form_wrap" data-aos="clipRight" data-aos-duration="1250" data-aos-once="true"
         data-aos-offset="-50">
        <div class="callback-form_title">{{ getTranslate('feedback.contact-with-us', 'Связаться с нами') }}</div>
        <form class="callback-form" id="footer-callback__form" action="{{ route('feedback') }}" data-ajax="true"
              method="post">
            @csrf
            <div class="callback-form_input-wrap">
                <input class="callback-form_input phone" name="phone" value="{{ old('phone') }}"
                       placeholder="{{ getTranslate('forms.phone', 'Телефон') }}" required/>
                <button type="submit"><img class="svg" src="{{ asset('images/icons/send.svg') }}" alt=""/></button>
            </div>
        </form>
    </div>
    <div class="container" data-aos="fromOpacity" data-aos-duration="1250" data-aos-once="true" data-aos-delay="200"
         data-aos-offset="-50">
        <div class="footer-content">
            <div class="footer-content_head">
                <img class="footer-logo" src="{{ asset('images/logo2.png') }}" alt=""/>
                <div class="footer-nav">
                    <x-layout.footer-menu></x-layout.footer-menu>
                </div>
            </div>
            <div class="footer-snetworks">
                <a class="footer-snetwork" href="{{ getSetting('socials.facebook') }}">
                    <img class="svg" src="{{ asset('images/icons/facebook.svg') }}" alt=""/>
                </a>
                <a class="footer-snetwork" href="{{ getSetting('socials.instagram') }}">
                    <img class="svg" src="{{ asset('images/icons/instagram.svg') }}" alt=""/>
                </a>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="footer-text">merais-group.com © 2013-{{ date('Y') }}</div>
            <div class="footer-text">
                <a href="http://micorestudio.com">Made with love by MantiCore Development</a>
            </div>
        </div>
    </div>
</footer>
<div class="backdrop"></div>

<div id="callback-modal" class="modal callback-modal fade" tabindex="-1" role="dialog"
     aria-labelledby="callback-modal__label"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                <img aria-hidden="true" class="svg" src="{{ asset('images/icons/Combined_Shape.svg') }}" alt=""/>
            </button>
            <div class="modal-title">{{ getTranslate('feedback.title-contact-us', 'Связаться с нами') }}</div>
            <p class="modal-description">{{ getTranslate('feedback.modal-description', 'Заполните поля ниже и мы свяжемся с вами в ближайшее время') }}</p>


            <form class="callback-modal__form" action="{{ route('feedback') }}" data-ajax="true" method="post"
                  autocomplete="off">

                <input class="callback-modal__form-input" required type="text" name="name"
                       placeholder="{{ getTranslate('forms.name', 'Имя') }}">

                <input class="callback-modal__form-input" type="text" name="email"
                       placeholder="Email" required>

                <input class="callback-modal__form-input phone" type="text" name="phone"
                       placeholder="{{ getTranslate('forms.phone', 'Телефон') }}" required>

                <input class="callback-modal__form-input" type="text" name="message"
                       placeholder="{{ getTranslate('forms.message', 'Сообщение') }}">

                <button class="common-btn" type="submit">{{ getTranslate('forms.send', 'Отправить') }}</button>
            </form>


        </div>
    </div>
</div>

<div id="callback-answer" class="modal callback-modal fade" tabindex="-1" role="dialog"
     aria-labelledby="callback-answer__label"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                <img aria-hidden="true" class="svg" src="{{ asset('images/icons/Combined_Shape.svg') }}" alt=""/>
            </button>
            <div class="modal-title">{{ getTranslate('feedback.send-success', 'Спасибо за заявку') }}</div>
            <p class="modal-description">{{ getTranslate('feedback.send-success-sub', 'Мы скоро с вами свяжемся') }}</p>
            <button class="common-btn close" data-dismiss="modal" aria-label="Close"
                    type="button">{{ getTranslate('feedback.send-btn-close', 'Понятно') }}</button>
        </div>
    </div>
</div>

@stack('footer-after')
{!! getSetting('js.scripts.body.footer') !!}
<!-- meta from admin panel to footer -->
{!! showMeta('', 'footer') !!}
<!-- end meta footer -->