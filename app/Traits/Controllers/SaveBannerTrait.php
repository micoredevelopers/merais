<?php

namespace App\Traits\Controllers;

use App\Models\Model;
use Illuminate\Http\Request;

/**
 * Trait SaveBannerTrait
 * @package App\Traits\Controllers
 */
trait SaveBannerTrait
{
    protected function saveBanner(Request $request, Model $model, $requestKey = 'image'): string
    {
        $file = $request->file($requestKey);
        if(!$file){
            return "";
        }

        $path = $model->getTable();
        $filename = md5($requestKey . $model->getKey()) . "." . $file->getClientOriginalExtension();
        $file->storeAs($path, $filename);
        $fullImagePath = $path . '/' . $filename;

        $model->$requestKey = $fullImagePath;
        $model->save();

        return $fullImagePath;
    }
}
