<?php

	namespace App\Http\Controllers;

	use App\Repositories\LeadRepository;
	use App\Repositories\PageRepository;
	use App\Repositories\ServiceRepository;
	use App\Repositories\SliderRepository;
	use App\Services\SMS\Sender;

	class AboutController extends SiteController
	{
		/**
		 * @var ServiceRepository
		 */
		private $serviceRepository;
		/**
		 * @var PageRepository
		 */
		private $pageRepository;
		/**
		 * @var SliderRepository
		 */
		private $sliderRepository;
		/**
		 * @var LeadRepository
		 */
		private $leadRepository;

		public function __construct(
			PageRepository $pageRepository,
			ServiceRepository $serviceRepository,
			SliderRepository $sliderRepository,
			LeadRepository $leadRepository
		)
		{
			parent::__construct();
			$this->serviceRepository = $serviceRepository;
			$this->pageRepository = $pageRepository;
			$this->sliderRepository = $sliderRepository;
			$this->leadRepository = $leadRepository;
		}

		public function index()
		{
			$page = $this->pageRepository->findPageByPageType('about');
			$with = compact('page');
			$this->setTitle($page->name)->addBreadCrumb($page->name);
			$this->shareIndex();
			$data['content'] = view('public.about.about')->with($with);
			return $this->main($data);
		}

		protected function shareIndex()
		{
			$leads = $this->leadRepository->getListPublic();
			$slider = $this->sliderRepository->findForPageAbout();
			$services = $this->serviceRepository->getListPublic();
			view()->share(compact(array_keys(get_defined_vars())));
		}
	}
