@if(isAdmin())
    @php
        $title = $title ?? 'Редактировать';
        $key = $key ?? '';
    @endphp
    <a class="badge badge-success" style="border-radius: 0;" href="/admin/settings?search={{ $key }}"
       target="_blank">{{ $title }}</a>
@endif