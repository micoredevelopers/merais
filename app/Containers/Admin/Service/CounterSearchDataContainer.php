<?php


	namespace App\Containers\Admin\Service;


	class CounterSearchDataContainer
	{
		private $search = '';


		private $serviceId;

		/**
		 * @return string
		 */
		public function getSearch(): string
		{
			return $this->search;
		}

		/**
		 * @param string $search
		 */
		public function setSearch(string $search): self
		{
			$this->search = $search;
			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getServiceId()
		{
			return $this->serviceId;
		}

		/**
		 * @param mixed $serviceId
		 */
		public function setServiceId($serviceId): self
		{
			$this->serviceId = $serviceId;
			return $this;
		}

	}