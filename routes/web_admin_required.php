<?php

use App\Http\Controllers\Admin\User\UserController;

Route::group(
	[
		'prefix'     => LaravelLocalization::setLocale(),
		'middleware' => ['localizationRedirect'],
	], function () {
	Route::group(['prefix' => \Config::get('app.admin-url'), 'namespace' => 'Admin'], function () {
		Route::get('/login', 'Auth\MyAuthController@showLogin')->name('admin.login');
		Route::post('/login', 'Auth\MyAuthController@authenticate');
		Route::post('/logout', 'Auth\MyAuthController@logout')->name('admin.logout');
	});
	Route::group(['prefix' => \Config::get('app.admin-url'), 'namespace' => 'Admin', 'middleware' => ['auth-admin', 'is-admin']], function () {
		Route::get('/', 'IndexController@index')->name('admin.index');
		/** @see UserController */
		Route::resource('users', 'User\UserController', ['as' => 'admin']);
		Route::post('/users/signsuperadmin', 'User\UserController@signSuperAdmin')->name('signsuperadmin');

		Route::group(['prefix' => 'profile', 'namespace' => 'User'], function () {
			Route::get('/', 'UserProfileController@profile')->name('admin.profile');
			Route::post('/', 'UserProfileController@profileUpdate')->name('admin.profile.update');
		});

		Route::resources([
			'translate' => 'TranslateController',
			'settings'  => 'SettingController',
		]);

		Route::resources([
			'roles' => 'RoleController',
		], ['as' => 'admin', 'except' => ['show']]
		);

		Route::post('/admin-menus/nesting', 'AdminMenuController@nesting')->name('admin.admin-menus.nesting');
		Route::patch('/admin-menus/save/all', 'AdminMenuController@updateAll')->name('admin-menus.updateAll');


		Route::group(['as' => 'settings.', 'prefix' => 'settings',], function () {
			Route::get('{id}/delete_value', [
				'uses' => 'SettingController@delete_value',
				'as'   => 'delete_value',
			]);
		});

		Route::group(['prefix' => 'ajax'], function () {
			Route::post('/sort', 'AjaxController@sort')->name('sort');
			Route::post('/delete', 'AjaxController@delete')->name('delete');
		});

		Route::prefix('dashboard')
			->group(function () {
				Route::group(['prefix' => 'cache'], function () {
					/** @see IndexController::clearCache() */
					Route::post('/clear', 'IndexController@clearCache')->name('cache.clear');
					/** @see IndexController::clearView() */
					Route::post('/clear/view', 'IndexController@clearView')->name('cache.view');
				});
				Route::post('/artisan/storage-link', 'IndexController@storageLink')->name('artisan.storage.link');
				Route::post('/artisan/refresh-db', 'IndexController@refreshDb')->name('artisan.db.refresh');
			})
		;

		Route::get('logs', 'Staff\\LogViewController@index');

	});
});
