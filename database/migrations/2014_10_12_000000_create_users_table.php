<?php

	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateUsersTable extends Migration
	{

		/**
		 * @var MigrationBuilder
		 */
		private $builder;

		public function __construct()
		{
			$this->builder = app(\App\Builders\Migration\MigrationBuilder::class);
		}

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('users', function (Blueprint $table) {
				$this->builder->setTable($table);
				$table->bigIncrements('id');
				$table->string('name')->nullable();
				$table->string('email')->nullable();
				$table->string('login')->unique()->nullable();
				$table->string('phone')->unique()->nullable();
				$table->timestamp('email_verified_at')->nullable();
				$table->timestamp('authenticated_at')->nullable();
				$table->string('password');
				$table->boolean('active')->default(1);
				$table->string('locale', 20)->default(\Config::get('app.locale'));
				$table->ipAddress('last_login_ip')->nullable();
				$table->string('user_agent', 1000)->nullable();
				$table->rememberToken();
				$this->addPersonColumns($table);

				$table->integer('balance')->default(0);

				$table->timestamps();
			});
		}

		private function addPersonColumns(Blueprint $table)
		{
			$this->builder
				->createNullableChar('fio')
			;
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('users');
		}
	}
