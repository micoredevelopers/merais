<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedback extends Migration
{

	private $table = 'feedback';
	/**
	 * @var MigrationBuilder
	 */
	private $builder;


	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}


	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);

			$table->id();
			$this->builder
				->createName()
				->createType('type', 'feedback')
				->createNullableChar('fio')
				->createNullableChar('phone')
				->createNullableChar('email')
				->createNullableString('message')
				->createNullableChar('ip')
				->createNullableString('referer')
			;
			$table->timestamps();
		});

	}


	public function down()
	{
		Schema::dropIfExists($this->table);
	}
}
