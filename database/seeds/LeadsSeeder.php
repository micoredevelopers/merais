<?php

use Illuminate\Database\Seeder;

class LeadsSeeder extends Seeder
{

    /**
     * @var \Faker\Generator
     */
    private $factory;
	/**
	 * @var \App\Repositories\LeadRepository
	 */
	private $repository;

	public function __construct( \Faker\Generator $factory, \App\Repositories\LeadRepository $repository)
    {
        $this->factory = $factory;
	    $this->repository = $repository;
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $leads = [
        	['image' => url('/images/leader1.png'),'name' => 'Анна' , 'description' => 'На тот момент у Марии был опыт в работе с клиентами, в том время как Анна - финансовый профессионал. Соединив экспертизу в итоге образовался хороший тандем - семейный бизнес стратега и тактика.', '© «Мы вовлекаемся в работу с клиентом по всей цепочке и тесно работаем по каждой его сделке.» '],
        	['image' => url('/images/leader2.png'),'name' => 'Мария' , 'description' => 'На тот момент у Марии был опыт в работе с клиентами, в том время как Анна - финансовый профессионал. Соединив экспертизу в итоге образовался хороший тандем - семейный бизнес стратега и тактика.', '© «Мы вовлекаемся в работу с клиентом по всей цепочке и тесно работаем по каждой его сделке.» '],
        ];
        foreach ($leads as $lead){
	        $this->repository->create($lead);
        }
    }
}
