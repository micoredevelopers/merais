<div class="point-wrap" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true"
     data-aos-delay="200">
    <div class="svg-wrap">
        <img class="svg" src="/images/icons/Rectangle.svg">
    </div>
    <span>{{ getTranslate('main-advantages.first', 'Тщательно подобранные специалисты с опытом работы в сфере логистики') }}</span>
</div>
<div class="point-wrap" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true"
     data-aos-delay="400">
    <div class="svg-wrap">
        <img class="svg" src="/images/icons/Rectangle.svg">
    </div>
    <span>{{ getTranslate('main-advantages.second', 'Автоматизированные логистические процессы') }}</span>
</div>
<div class="point-wrap" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true"
     data-aos-delay="600">
    <div class="svg-wrap">
        <img class="svg" src="/images/icons/Rectangle.svg">
    </div>
    <span>{{ getTranslate('main-advantages.third', 'Современный автомобильный парк') }}</span>
</div>