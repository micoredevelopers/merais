<?php

	namespace App\Builders\Menu;


	use Illuminate\Support\Str;

	class MenuBuilder implements \ArrayAccess
	{
		private $active = true;

		private $url = '';

		private $gateRule = '';

		private $name = '';

		private $iconFont = '';

		private $childrens = [];

		public function setName(string $_): self
		{
			$this->name = $_;
			return $this;
		}

		/**
		 * @param string $url
		 */
		public function setUrl(string $url): self
		{
			$this->url = $url;
			return $this;
		}

		/**
		 * @param string $_
		 */
		public function setGateRule(string $_): self
		{
			$this->gateRule = $_;
			return $this;
		}

		/**
		 * @param string $_
		 */
		public function setIconFont(string $_): self
		{
			$this->iconFont = $_;
			return $this;
		}

		/**
		 * @param bool $active
		 */
		public function setActive(bool $active): self
		{
			$this->active = $active;
			return $this;
		}

		/**
		 * @param array $_
		 */
		public function setChildrens(array $_): self
		{
			$this->childrens = $_;
			return $this;
		}

		public function build()
		{
			$menu = [
				'active'    => (int)$this->active,
				'url'       => $this->url,
				'gate_rule' => $this->gateRule,
				'name'      => $this->name,
				'icon_font' => $this->iconFont,
			];
			if ($this->childrens) {
				$menu['childrens'] = $this->childrens;
			}
			return $menu;
		}

		public function offsetExists($offset)
		{
			return property_exists($this, $offset);
		}

		public function offsetGet($offset)
		{
			return $this->offsetExists($offset) ? $this->{$offset} : null;
		}

		public function offsetSet($offset, $value)
		{
			$method = Str::camel('set' . ucfirst($offset));
			if (method_exists($this, $method)) {
				$this->$method($value);
			}
		}

		public function offsetUnset($offset)
		{
			if ($this->offsetExists($offset)) {
				$this->{$offset} = null;
			}
		}

	}