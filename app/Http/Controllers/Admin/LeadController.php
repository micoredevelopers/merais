<?php

	namespace App\Http\Controllers\Admin;

	use App\Helpers\Media\ImageRemover;
	use App\Http\Controllers\Admin\AdminController;
	use App\Http\Requests\Admin\LeadRequest;
	use App\Models\Lead\Lead;
	use App\Repositories\LeadRepository;
	use App\Traits\Authorizable;
	use App\Traits\Controllers\SaveImageTrait;

	class LeadController extends AdminController
	{
		use Authorizable;
		use SaveImageTrait;

		protected $withThumbnails = false;

		private $name;

		protected $key = 'leads';

		protected $routeKey = 'admin.leads';

		protected $permissionKey = 'leads';
		/**
		 * @var LeadRepository
		 */
		private $repository;

		public function __construct(LeadRepository $repository)
		{
			parent::__construct();
			$this->name = __('modules.leads.title');
			$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
			$this->shareViewModuleData();
			$this->repository = $repository;
		}

		public function index(LeadRepository $leadRepository)
		{
			$this->setTitle($this->name);
			$vars['list'] = $leadRepository->getListAdmin();
			$data['content'] = view('admin.leads.index', $vars);
			return $this->main($data);
		}

		public function create()
		{
			$data['content'] = view('admin.leads.create');
			return $this->main($data);
		}

		public function store(LeadRequest $request)
		{
			$input = $request->except('image');
			if ($lead = $this->repository->create($input)) {
				$this->setSuccessStore();
				$this->saveImage($request, $lead);
				$this->fireEvents();
			}
			return $this->redirectOnCreated($lead);
		}

		public function edit(Lead $lead)
		{
			$edit = $lead;
			$title = $this->titleEdit($edit);
			$this->addBreadCrumb($title)->setTitle($title);

			$data['content'] = view('admin.leads.edit', compact(
				'edit'
			));
			return $this->main($data);
		}

		public function update(LeadRequest $request, Lead $lead)
		{
			$input = $request->except('image');
			$this->saveImage($request, $lead);

			if ($this->repository->update($input, $lead)) {
				$this->setSuccessUpdate();
				$this->fireEvents();
			}

			return $this->redirectOnUpdated($lead);
		}

		public function destroy(Lead $lead, ImageRemover $imageRemover)
		{
			if (!$lead->canDelete()) {
				$this->setFailMessage('Удаление этой записи не доступно');
				return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
			}
			if ($this->repository->delete($lead->id)) {
				$imageRemover->removeImage($lead->image);
				$this->setSuccessDestroy();
				$this->fireEvents();
			}

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		private function fireEvents()
		{
		}
	}
