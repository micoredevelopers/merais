<?php

	namespace App\Http\Controllers\Admin\User;

	use App\Containers\Admin\User\SearchDataContainer;
	use App\Http\Controllers\Admin\AdminController;
	use App\Http\Requests\Admin\UserProfileRequest;
	use App\Repositories\UserRepository;
	use App\User;
	use Illuminate\Support\Facades\Hash;

	class UserProfileController extends AdminController
	{


		private $repository;
		/**
		 * @var SearchDataContainer
		 */

		public function __construct(UserRepository $repository)
		{
			parent::__construct();
			$this->name = __('modules.users.title');
			$this->addBreadCrumb(__('modules.users.title'), $this->resourceRoute('index'));
			$this->shareViewModuleData();
			$this->repository = $repository;
		}


		public function profile()
		{
			$locales = \LaravelLocalization::getSupportedLocales();
			$this->dropLastBreadCrumb();
			$title = __('modules.users.profile.title');
			$this->addBreadCrumb($title)->setTitle($title);
			$data['content'] = view('admin.user.profile', [
				'user'    => \Illuminate\Support\Facades\Auth::user(),
				'locales' => $locales,
			]);
			return $this->main($data);
		}

		/**
		 * @param UserProfileRequest $request
		 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 */
		public function profileUpdate(UserProfileRequest $request)
		{
			/** @var $user User */
			$user = \Auth::user();
			if ($request->isPasswordsWasSend()) {
				$passwordsMatches = Hash::check($request->get('password'), $user->password);
				if ($passwordsMatches) {
					$user->password = Hash::make($request->get('password_new'));
				} else {
					$this->setMessage(__('user.invalid-current-password'));
					return back()->with($this->getResponseMessage())->withInput($request->input());
				}
			}
			$this->setSuccessUpdate();
			$data = $request->only([
				'locale',
				'name',
			]);
			$user->fillExisting($data)->save();

			return redirect()->back()->with($this->getResponseMessage());
		}


	}
