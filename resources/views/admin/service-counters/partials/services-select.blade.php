<?php /** @var $edit \App\Models\Service\ServiceCounter */ ?>
<?php /** @var $service \App\Models\Service\Service */ ?>

@isset($services)
    <select name="service_id" id="service_id" class="form-control selectpicker" autocomplete="off">
        @if($allowEmpty ?? false)
            <option value="">- Выберите услугу -</option>
        @endif
        @foreach($services as $service)
            @php
                $isSelected = ($serviceId ?? null) == $service->getKey() ;
            @endphp
            <option {!! selectedIfTrue($isSelected) !!}
                    value="{{ $service->getKey() }}">{{ $service->getName() }}</option>
        @endforeach
    </select>
@endisset