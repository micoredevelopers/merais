<?php

	namespace App;

	use App\Contracts\Models\HasDeletedBy;
	use App\Contracts\Models\HasModifiedBy;
	use App\Observers\UserObserver;
	use App\Scopes\WhereActiveScope;
	use App\Traits\EloquentExtend;
	use App\Traits\EloquentScopes;
	use App\Traits\Models\Relations\DeletedByUserIdTrait;
	use App\Traits\Models\Relations\ModifiedByUserIdTrait;
	use App\Traits\Models\User\SuperAdminTrait;
	use App\Traits\Models\User\UserAccessorsTrait;
	use App\Traits\Models\User\UserHelpersTrait;
	use App\Traits\Models\User\UserMutatorTrait;
	use App\Traits\Models\User\UserRelationTrait;
	use Illuminate\Database\Eloquent\Builder;
	use Illuminate\Notifications\Notifiable;
	use Illuminate\Foundation\Auth\User as Authenticatable;
	use Spatie\Permission\Traits\HasRoles;

	/**
	 * App\User
	 *
	 * @property int                                                                                                            $id
	 * @property string                                                                                                         $name
	 * @property string                                                                                                         $email
	 * @property \Illuminate\Support\Carbon|null                                                                                $email_verified_at
	 * @property string                                                                                                         $password
	 * @property string|null                                                                                                    $remember_token
	 * @property \Illuminate\Support\Carbon|null                                                                                $created_at
	 * @property \Illuminate\Support\Carbon|null                                                                                $updated_at
	 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
	 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[]                           $permissions
	 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[]                                 $roles
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User notSuperAdmin()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User permission($permissions)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User role($roles, $guard = null)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
	 * @mixin \Eloquent
	 * @property string|null                                                                                                    $api_token
	 * @property int                                                                                                            $active
	 * @property string                                                                                                         $locale
	 * @property string|null                                                                                                    $last_login_ip
	 * @property string|null                                                                                                    $user_agent
	 * @property-read \App\Models\Staff\DeletedBy                                                                               $deletable
	 * @property mixed                                                                                                          $group
	 * @property-read \App\Models\Staff\ModifiedBy                                                                              $modifiable
	 * @property-read int|null                                                                                                  $notifications_count
	 * @property-read int|null                                                                                                  $permissions_count
	 * @property-read int|null                                                                                                  $roles_count
	 * @property-write mixed                                                                                                    $raw
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereActive($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereApiToken($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastLoginIp($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLocale($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUserAgent($value)
	 */
	class User extends Authenticatable implements HasModifiedBy, HasDeletedBy
	{
		use UserAccessorsTrait;
		use UserMutatorTrait;
		use UserHelpersTrait;
		use UserRelationTrait;

		use SuperAdminTrait;

		use EloquentExtend;
		use EloquentScopes;
		use Notifiable;

		use ModifiedByUserIdTrait;
		use DeletedByUserIdTrait;
		use HasRoles;

		public const SUPER_ADMIN_ID = 1;
		public const MODERATOR_USER_ID = 2;

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */

		protected $guarded = [
			'id',
		];

		protected $fillable = [
			'email',
			'password',
			'remember_token',
			'name',
			'fio',
			'phone',
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */
		protected $hidden = [
			'password', 'remember_token',
		];

		/**
		 * The attributes that should be cast to native types.
		 *
		 * @var array
		 */
		protected $casts = [
			'email_verified_at' => 'datetime',
		];

		public static function boot()
		{
			parent::boot();

			static::addGlobalScope(
				new WhereActiveScope()
			);

			if (class_exists(UserObserver::class)) {
				static::observe(UserObserver::class);
			}
		}

	}
