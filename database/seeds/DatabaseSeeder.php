<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(LanguageTableSeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(RolesAndPermissionsSeeder::class);
        $this->call(AdminMenuSeeder::class);
        $this->call(SliderTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(TranslateTableSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(MenuGroupSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(MetaTableSeeder::class);
        $this->call(RedirectsTableSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ServiceCounterSeeder::class);
        $this->call(AdvantageSeeder::class);
        $this->call(LeadsSeeder::class);
    }
}
