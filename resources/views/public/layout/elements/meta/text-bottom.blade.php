@if ($textBottom = showMeta('', 'text_bottom'))
    <div style="background:#f9f9f9" class="py-4">
        <div class="container">
            @if (isAdmin() && $meta = getMeta())
                <a href="{{ route('admin.meta.edit', $meta->id) }}#text_bottom" target="_blank" class="badge badge-danger">Редактировать
                    мета текст</a>
            @endif
            <div class="decodeWrapper">{!! $textBottom !!}</div>
        </div>
    </div>
@endif
