<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>ID</th>
            <th class="th-description">Комментарий</th>
            <th class="text-right">
                @can('create_' . $permissionKey)
                    <a href="{{ route($routeKey . '.create') }}" class="btn btn-primary">@lang('form.create')</a>
                @endcan
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>
                    <a href="{{ route($routeKey.'.edit',  $item->id) }}">
                        {{ $item->comment }}
                    </a>
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<div class="text-center">
    {{ $list->links()}}
</div>
