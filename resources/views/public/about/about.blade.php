<?php /** @var $page \App\Models\Page\Page */ ?>
<main id="about-page">
    <x-layout.breadcrumbs></x-layout.breadcrumbs>
    <section class="about-section">
        <div class="container">
            <div class="about-section_title" data-aos="offsetTop" data-aos-duration="600" data-aos-delay="200"
                 data-aos-once="true">
                {{ $page->title }}
            </div>
            <div class="row about-section_desc-wrap">
                <div class="col-12 col-lg-6">
                    @foreach(explodeByEol($page->getDescription()) as $text)
                        @php
                            $delay = 200 + (200 * $loop->iteration);
                        @endphp
                        <div class="about-section_desc" data-aos="offsetTop" data-aos-duration="600"
                             data-aos-delay="{{ $delay }}"
                             data-aos-once="true">{{ $text }}</div>
                    @endforeach
                </div>
                <div class="col-12 col-lg-6">
                    @foreach(explodeByEol($page->getSubDescription()) as $text)
                        @php
                            $delay = 800 + (200 * $loop->iteration);
                        @endphp
                        <div class="about-section_desc" data-aos="offsetTop" data-aos-duration="600"
                             data-aos-delay="{{ $delay }}"
                             data-aos-once="true">{{ $text }}</div>
                    @endforeach
                </div>
            </div>
            <div class="about-section_img">
                <img src="{{ getPathToImage(imgPathOriginal($page->image)) }}" data-aos="clipRight" data-aos-duration="600" data-aos-delay="1600"
                     data-aos-once="true"
                />
            </div>
        </div>
    </section>
    

    @includeIf('public.about.includes.valuables')

    @includeIf('public.about.includes.slider')

    @includeIf('public.about.includes.services')

</main>
@if($page ?? false)
    {!! editButtonAdmin(urlEntityEdit($page)) !!}
@endif