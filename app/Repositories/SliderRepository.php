<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Events\Admin\Image\ImageUploaded;
use App\Helpers\Media\ImageSaver;
use App\Models\Slider\Slider;
use App\Models\Slider\SliderItem;
use App\Models\Slider\SliderItemLang;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

/**
 * Class MetaRepository.
 */
class SliderRepository extends AbstractRepository
{
	/**
	 * @var SliderItemRepository
	 */
	private $sliderItemRepository;

	public function __construct(Application $app, SliderItemRepository $sliderItemRepository)
	{
		parent::__construct($app);
		$this->sliderItemRepository = $sliderItemRepository;
	}

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return Slider::class;
	}

	public function getListForAdmin(): LengthAwarePaginator
	{
		return $this->model->whereNull('sliderable_id')->paginate();
	}

	public function storeSlideItem(Request $request, Slider $slider): bool
	{
		$sliderItem = new SliderItem();
		$sliderItemLang = new SliderItemLang();
		$inputItem = $request->input(inputNamesManager($sliderItem)->getNameInputRequest(), []);
		$inputItem = array_merge($inputItem, $request->input(inputNamesManager($sliderItemLang)->getNameInputRequest(), []));
		$result = $this->sliderItemRepository->createRelated($inputItem, $slider);
		return (bool)$result;
	}

	public function save(Request $request, Slider $slider)
	{
		$sliderItem = new SliderItem();
		$input = $request->input(inputNamesManager($slider)->getNameInputRequest());
		$input['active'] = $request->input(inputNamesManager($slider)->getNameInputRequestByKey('active'), 0);
		$slider->fillExisting($input);
		if ($result = $slider->save()) {
			/** @var $language \App\Models\Language */
			if ($slider->wasRecentlyCreated) {
				$this->storeSlideItem($request, $slider);
			} else if ($request->has($sliderItem->getTable())) {
				/** @var  $sliderItem SliderItem */
				foreach ($slider->items as $sliderItem) {
					$itemKey = inputNamesManager($sliderItem)->getNameInputRequest();
					if ($input = $request->input($itemKey)) {
						$input['active'] = $request->input(inputNamesManager($sliderItem)->getNameInputRequestByKey('active'), 0);
						if ($request->hasFile(inputNamesManager($sliderItem)->getNameInputRequestByKey('src'))) {
							$input['src'] = (new ImageSaver($request, inputNamesManager($sliderItem)->getNameInputRequestByKey('src'), $sliderItem->getTable()))
								->setThumbnailSizes(...$this->getThumbnailSizesByTypeSlider($slider->getAttribute('key')))
								->saveFromRequest()
							;
							event(new ImageUploaded($sliderItem, $request, 'src'));
						}
						$sliderItem->fillExisting($input);
						$sliderItem->save();
						/** @var  $sliderItemLang SliderItemLang */
						if ($sliderItemLang = $sliderItem->lang) {
							$inputLang = $request->input(inputNamesManager($sliderItemLang)->getNameInputRequest(), []);
							$sliderItem->lang->fillExisting($inputLang)->save();
						}
					}
				}
			}
		}
		return $result;
	}

	private function getThumbnailSizesByTypeSlider(?string $type = ''): array
	{
		try {
			return array_values(config('images.sizes.sliders.' . $type, config('images.default')));
		} catch (\Exception $e) {
			return [];
		}
	}

	private function loadWithScopes(Slider $slider)
	{
		$slider->load(['items' => function ($query) {
			$query->active();
		}, 'items.lang',
		]);
		return $slider;
	}

	public function findForMainPage()
	{
		$this->pushCriteria($this->app->make(ActiveCriteria::class));
		$slider = $this->findOneByField('key', 'main-page');
		if (!$slider) {
			return $slider;
		}
		$this->loadWithScopes($slider);
		return $slider;
	}

	public function findForMainPageAbout()
	{
		$this->pushCriteria($this->app->make(ActiveCriteria::class));
		$slider = $this->findOneByField('key', 'main-page-about');
		if (!$slider) {
			return $slider;
		}
		$this->loadWithScopes($slider);
		return $slider;
	}

	public function findForPageAbout()
	{
		$this->pushCriteria($this->app->make(ActiveCriteria::class));
		$slider = $this->findOneByField('key', 'about');
		if (!$slider) {
			return $slider;
		}
		$this->loadWithScopes($slider);
		return $slider;
	}

}
