<?php

	namespace App\Models\Lead;


	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Models\Service\Service;
	use App\Traits\Models\HasGetterDescriptionTrait;
	use App\Traits\Models\HasGetterNameTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	class Lead extends Model implements HasLocalized
	{
		use RedirectLangColumn;
		use HasGetterDescriptionTrait;
		use HasGetterNameTrait;

		protected $langColumns = ['name', 'description', 'subscription'];

		protected $perPage = 40;

		protected $guarded = ['id'];

		protected $hasOneLangArguments = [LeadLang::class];

		public function service(): BelongsTo
		{
			return $this->belongsTo(Service::class);
		}

		public function canDelete(): bool
		{
			return true;
		}

		public function getSubscription()
		{
			$column = 'subscription';
			return $this->getAttribute($column);

		}

	}
