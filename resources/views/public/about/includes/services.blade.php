
<section class="services-about">
    <div class="container">
        <div class="about-section_title text-center" data-aos="clipTop" data-aos-duration="600"
             data-aos-once="true">
            {{ getTranslate('services.title', 'Услуги') }}
        </div>
        <div class="services-wrap row">
            @foreach($services as $service)
                @php
                    $delay = 200 + (200 * $loop->iteration)
                @endphp
            <div class="service col-12 col-lg-4" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true"
                 data-aos-delay="400">
                <div class="service-number">{{ $loop->iteration < 10 ? 0 : '' }}{{ $loop->iteration }}</div>
                <img class="svg service-icon" src="{{ storageFileExists($service->getIcon()) ? getPathToImage($service->getIcon()) : asset('images/icons/service.svg') }}"/>
                <div class="service-content">
                    <div class="service-title">{{ $service->getName() }}</div>
                    <div class="service-description">{!! $service->getExcerpt() !!}</div>
                </div>
                <a class="service_link" href="{{ route('service.show', $service->getUrl()) }}">
                    <span>{{ getTranslate('global.view-details', 'Подробнее') }}</span>
                    <img class="svg" src="{{ asset('images/icons/arrow.svg') }}" alt="">
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>