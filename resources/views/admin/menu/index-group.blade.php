<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="th-description">@lang('form.title')</th>
        </tr>
        </thead>
        <tbody data-sortable-container="" data-table="menu">
        @foreach($groups as $item)
            <tr>
                <td>
                    {{ $item->name }}
                </td>
                <td class="text-primary text-right">
                    <div class="dropdown menu_drop">
                        <button
                                class="btn btn-secondary dropdown-toggle" type="button"
                                id="dropdownMenuButton_{{ $item->id }}" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">menu</i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_{{ $item->id }}">
                            {{--<form class="dropdown-item p-0" href="#">--}}
                            {{--<button class="drop_menu_button text-left"--}}
                            {{--type="submit">Отключить/включить</button>--}}
                            {{--</form>--}}
                            @can('edit_' . $permissionKey)
                                <a href="{{ route($routeKey . '.index')  }}?group={{ $item->id }}" class="dropdown-item">@lang('form.edit')</a>
                            @endcan
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
