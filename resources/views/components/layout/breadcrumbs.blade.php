@if(isset($breadcrumbs) && $breadcrumbs)
    <div class="page-path_wrap aos-init" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true">
        <div class="container d-flex">
            @foreach($breadcrumbs as $breadcrumb)
                @if(!$loop->last)
                    <a class="path-link" href="{{ $breadcrumb['url'] ?? '' }}">{{ $breadcrumb['name'] ?? '' }}</a><span>/</span>
                @else
                    <div class="path-current_page">{{ $breadcrumb['name'] ?? '' }}</div>
                @endif
            @endforeach
        </div>
    </div>
@endif
