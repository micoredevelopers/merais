<?php

namespace App\Rules;

use App\Repositories\ServiceRepository;
use Illuminate\Contracts\Validation\Rule;

class ServiceExists implements Rule
{
	/**
	 * @var ServiceRepository
	 */
	private $serviceRepository;

	/**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(ServiceRepository $serviceRepository)
    {
        //
	    $this->serviceRepository = $serviceRepository;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    	if (!is_numeric($value)){
    		return false;
	    }
	    return $this->serviceRepository->addPublicCriteriaToQuery()->where('id', $value)->get()->isNotEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Услуга не найдена';
    }
}
