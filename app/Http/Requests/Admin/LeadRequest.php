<?php

	namespace App\Http\Requests\Admin;

	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\AbstractRequest;

	class LeadRequest extends AbstractRequest
	{
		protected $toBooleans = ['active'];

		public function rules()
		{
			$rules = [
				'name'        => ['required', 'string', 'max:255'],
				'position'    => ['nullable', 'string', 'max:500'],
				'description' => ['max:' . ValidationMaxLengthHelper::TEXT],
			];

			return $rules;
		}

	}
