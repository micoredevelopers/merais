<?php /** @var $service \App\Models\Service\Service */ ?>

<form action="" method="get">
    <div class="form-group">
        <div class="row">
            <div class="col-6">
                <input type="text" class="form-control mt-3" name="search" placeholder="Текст поиска (ключ,значение)" value="{{ request('search') }}" autocomplete="off">
            </div>
            <div class="col-4">
                @isset($services)
                    @includeIf('admin.service-counters.partials.services-select', ['serviceId' => request('service_id'), 'allowEmpty' => true])
                @endisset
            </div>
            <div class="col-2">
                <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i>
                    @lang('form.search')
                </button>
            </div>
        </div>
    </div>
</form>