@include('admin.partials.crud.elements.name')

<div class="row">
    <div class="col-6">
        @include('admin.partials.crud.elements.url')
    </div>
    <div class="col-6">
        @include('admin.partials.crud.elements.active')
    </div>
</div>

<div class="row">
    <div class="col-6">
        <label class=" control-label">Главное изображение</label>
        @include('admin.partials.crud.elements.image-upload-group')
    </div>
    <div class="col-6">
        <label class=" control-label">Значок</label>
        @include('admin.partials.crud.elements.image-upload-group', ['name' => 'icon'])
    </div>
</div>


@include('admin.partials.crud.textarea.except')

@include('admin.partials.crud.textarea.description')


<h3 style="padding-top: 40px;">Нижний блок с текстом</h3>
@include('admin.partials.crud.default', ['name' => 'name_second', 'title' => 'Заголовок нижнего блока'])

@include('admin.partials.crud.textarea.description', ['name' => 'text_first', 'title' => '1 колонка текста'])

@include('admin.partials.crud.textarea.description', ['name' => 'text_second', 'title' => '2 колонка текста'])

<div class="row">
    <div class="col-6">
        <label class=" control-label">Баннер под текстом</label>
        @include('admin.partials.crud.elements.image-upload-group', ['name' => 'banner', 'title' => 'Баннер под текстом (1310px x 740px)'])
    </div>
</div>