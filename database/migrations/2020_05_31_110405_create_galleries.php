<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleries extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'galleries';

   private $foreignKey = 'gallery_id';

   private $tableLang = 'gallery_langs';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }


   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $table->id();
            $this->builder
	            ->createType()
	            ->createSort()
	            ->createActive()
            ;
            $table->timestamps();
        });


        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->id();
            $table->unsignedBigInteger($this->foreignKey);

            $this->builder
                ->createName()
                ->createDescription()
                ->createLanguageKey()
            ;
            $table->foreign($this->foreignKey)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
