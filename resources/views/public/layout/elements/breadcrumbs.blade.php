@isset($breadcrumbs)
    <div class="container">
        <div class="breadcrumbs">
            @foreach($breadcrumbs as $breadcrumb)
                @if (!$loop->last)
                    <a href="{{ \Arr::get($breadcrumb, 'url') }}" class="breadcrumb-item" data-index="{{ $loop->iteration }}">{{ \Arr::get($breadcrumb, 'name') }}</a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                @else
                    <span class="breadcrumb-last">{{ \Arr::get($breadcrumb, 'name') }}</span>
                @endif
            @endforeach
        </div>
    </div>
@endisset

