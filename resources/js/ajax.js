$('form[data-ajax]').on('submit', (e) => {
    e.preventDefault();
    const form = $(e.target);

    if(form.hasClass('callback-modal__form')) {
        $('.error').remove();
        form.append(`<div class="sending">Отправка...</div>`)
    }

    $.ajax({
        method: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        success: function () {
            const modal = form.parents('.modal');

            if(form.attr('id') === 'footer-callback__form') {
                $('#callback-answer').modal('show');
                $('.sending').remove();
            }

            if(modal.hasClass('show')) {
                $(modal).modal('hide');
                $('#callback-answer').modal('show');
                $('.sending').remove();
            }
        },
        error: function (res) {
            const array = res.responseJSON.errors;
            const modal = form.parents('.modal');

            if(modal.hasClass('show')) {
                $('.sending').remove();
            }

            form.find('.input-errors').remove();

            let error = '';

            for(key in array) {

                for(let i = 0; i < array[key].length; i++) {
                    error += array[key];
                }


                form.append(`<div class="error">${error}</div>`)
                error = '';
            }

        }
    })

})