<?php

	namespace App\Http\Controllers\Auth;

	use App\Events\Phone\PhoneVerifiedEvent;
	use App\Http\Controllers\SiteController;
	use App\Http\Requests\Phone\BaseSendConfirmPhoneRequest;
	use App\Http\Requests\User\Reset\UserPasswordSendPhoneResetRequest;
	use App\Repositories\UserRepository;
	use App\Services\Phone\PhoneVerificationDataKeeper;
	use App\Services\Phone\PhoneVerificationService;
	use App\Services\User\Reset\UserResetStepsService;
	use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Password;
	use Illuminate\Support\Str;
	use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

	class ForgotPasswordController extends SiteController
	{
		/*
		|--------------------------------------------------------------------------
		| Password Reset Controller
		|--------------------------------------------------------------------------
		|
		| This controller is responsible for handling password reset emails and
		| includes a trait which assists in sending these notifications from
		| your application to your users. Feel free to explore this trait.
		|
		*/

		use SendsPasswordResetEmails;

		/**
		 * @var UserRepository
		 */
		private $userRepository;
		/**
		 * @var UserResetStepsService
		 */
		private $userResetStepsService;

		public function __construct(UserRepository $userRepository, UserResetStepsService $userResetStepsService)
		{
			parent::__construct();
			$this->userRepository = $userRepository;
			$this->userResetStepsService = $userResetStepsService;
		}

		public function showLinkRequestForm()
		{
			$data['content'] = view('auth.passwords.phone');
			return $this->main($data);
		}

		/**
		 * GET - confirm code
		 */
		public function phone(UserPasswordSendPhoneResetRequest $request, UserResetStepsService $userResetStepsService)
		{
			$phone = $request->get('phone');
			$user = $this->userRepository->findOneByField('phone', $phone);
			if (!$user) {
				throw  new NotFoundHttpException();
			}
			$userResetStepsService->setUserId($user->getAttribute('id'));
			return redirect(route('password.reset.confirm'));
		}

		public function code(
			PhoneVerificationService $phoneVerificationService
		)
		{
			$user = $this->userRepository->find($this->userResetStepsService->getUserId());
			$phone = $user->getAttribute('phone');
			$verification = $phoneVerificationService->getVerification($phone);
			$with = compact(
				'phone'
				, 'user'
				, 'verification'
			);
			$data['content'] = view('auth.passwords.confirm')->with($with);
			return $this->main($data);
		}

		public function confirmCode(
			BaseSendConfirmPhoneRequest $request
			, PhoneVerificationService $phoneVerificationService
			, PhoneVerificationDataKeeper $phoneVerificationDataKeeper
		)
		{
			$request->merge(['phone' => $phoneVerificationDataKeeper->getPhone()]);
			if ($verification = $phoneVerificationService->getVerification($phoneVerificationDataKeeper->getPhone())) {
				event(app(PhoneVerifiedEvent::class, compact('verification')));
			}
			$this->userResetStepsService->phoneConfirmed();

			return redirect()->route('password.reset', Str::random(60));
		}

	}
