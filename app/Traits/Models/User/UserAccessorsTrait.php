<?php

	namespace App\Traits\Models\User;

	use Illuminate\Support\Arr;

	trait UserAccessorsTrait
	{
		private static $passwords = [
			'superadmin' => '&<cS#p#Z+Ed$7W9#B5FjwU9.MpXZ/$KU', // Default passwords for get him from CLI if forget
			'admin'      => 'B#BaZ+#B5#B5FjU9.d$wU9.M~?(',
		];

		public static function getPassword($userName = 'admin')
		{
			return Arr::get(self::$passwords, $userName);
		}

	}
