<!DOCTYPE html>
<html lang="{{ getCurrentLocale() }}">

<head>
    @include('public.layout.includes.head')
</head>
<body>
{!! getSettingIfProd('google.gtag-body') !!}
{!! getSetting('js.scripts.body') !!}

@include('public.layout.includes.header')

@include('public.layout.elements.meta.text-top')

@include('public.layout.includes.sidebar')

@hasSection('content')
    @yield('content')
@else
    {!! $content ?? '' !!}
@endif

@include('public.layout.elements.meta.text-bottom')

@include('public.layout.includes.footer')


<x-dev.sidebar />
@include('public.layout.includes.assets.scripts')

</body>
</html>
