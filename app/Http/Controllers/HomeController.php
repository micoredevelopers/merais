<?php

	namespace App\Http\Controllers;

	use App\Repositories\AdvantageRepository;
	use App\Repositories\PageRepository;
	use App\Repositories\ServiceRepository;
	use App\Repositories\SliderRepository;

	class HomeController extends SiteController
	{

		/**
		 * @var ServiceRepository
		 */
		private $serviceRepository;
		/**
		 * @var PageRepository
		 */
		private $pageRepository;
		/**
		 * @var AdvantageRepository
		 */
		private $advantageRepository;
		/**
		 * @var SliderRepository
		 */
		private $sliderRepository;

		public function __construct(
			ServiceRepository $serviceRepository,
			PageRepository $pageRepository,
			AdvantageRepository $advantageRepository,
			SliderRepository $sliderRepository
		)
		{
			parent::__construct();
			$this->serviceRepository = $serviceRepository;
			$this->pageRepository = $pageRepository;
			$this->advantageRepository = $advantageRepository;
			$this->sliderRepository = $sliderRepository;
		}

		public function index()
		{
			$this->shareIndex();
			$page = $this->pageRepository->findPageByPageType('main');
			$this->setTitle($page->name);
			$with = compact('page');
			$data['content'] = view('public.home.home')->with($with);
			return $this->main($data);
		}

		protected function shareIndex()
		{
			$services = $this->serviceRepository->getListPublic();
			$slider = $this->sliderRepository->findForMainPage();
			$sliderAbout = $this->sliderRepository->findForMainPageAbout();
			$advantages = $this->advantageRepository->getListPublic();
			view()->share(compact(array_keys(get_defined_vars())));
		}

	}
