<div>
    <p>Install project:</p>
    <ol>
        <li><code>cp .env.example .env</code></li>
        <li>В .env файле укажите настройки подключения к базе данных DB_USERNAME,DB_PASSWORD</li>
        <li><code>/bin/bash install_project.sh</code></li>
    </ol>
    <p>Доступы в админку: <p>admin</p> пароль можно получить выполнив команду <code>php artisan auth:admin-pass admin</code></p>
</div>