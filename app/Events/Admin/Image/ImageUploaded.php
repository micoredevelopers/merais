<?php

namespace App\Events\Admin\Image;

use App\Listeners\Admin\Image\ImageUploadedAddThumbnailsListener;
use App\Listeners\Admin\Image\ImageUploadedAddWatermarkListener;
use App\Models\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class ImageUploaded
{
	use Dispatchable, SerializesModels;

	/**
	 * @var Model
	 */
	private $model;
	/**
	 * @var Request
	 */
	private $request;
	/**
	 * @var string|null
	 */
	private $requestKey;

	public function __construct(Model $model, Request $request, ?string $requestKey = null)
	{
		$this->model = $model;
		$this->request = $request;
		$this->requestKey = $requestKey;
	}

	public function getRequest(): Request
	{
		return $this->request;
	}

	public function getModel(): Model
	{
		return $this->model;
	}

	/**
	 * @return string|null
	 */
	public function getRequestKey(): ?string
	{
		return $this->requestKey;
	}

	public static function getListeners(): array
	{
		return [
			ImageUploadedAddThumbnailsListener::class,
			ImageUploadedAddWatermarkListener::class,
		];

	}
}
