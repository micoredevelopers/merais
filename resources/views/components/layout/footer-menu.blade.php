<?php /** @var $menu \App\Models\Menu */ ?>
@isset($footerMenu)
    @foreach($footerMenu as $menu)
        <a class="footer-link" {!! $menu->getRel() !!} href="{{ $menu->getFullUrl() }}">{{ $menu->getName() }}</a>
    @endforeach
@endisset