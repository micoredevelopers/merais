<?php

	namespace App\Http\Controllers\Auth;

	use App\Events\Phone\PhoneVerifiedEvent;
	use App\Events\User\Registration\SendPasswordEvent;
	use App\Http\Controllers\SiteController;
	use App\Http\Requests\Phone\BaseSendConfirmPhoneRequest;
	use App\Http\Requests\User\Register\UserRegisterPasswordRequest;
	use App\Http\Requests\User\Register\UserRegisterPersonalRequest;
	use App\Providers\RouteServiceProvider;
	use App\Repositories\UserRepository;
	use App\Services\Phone\PhoneVerificationDataKeeper;
	use App\Services\Phone\PhoneVerificationService;
	use App\Services\User\Registration\UserRegistrationStepsService;
	use App\User;
	use Illuminate\Auth\Events\Registered;
	use Illuminate\Foundation\Auth\RegistersUsers;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Hash;
	use Illuminate\Support\Facades\Validator;
	use Illuminate\Support\Str;

	class RegisterController extends SiteController
	{
		/*
		|--------------------------------------------------------------------------
		| Register Controller
		|--------------------------------------------------------------------------
		|
		| This controller handles the registration of new users as well as their
		| validation and creation. By default this controller uses a trait to
		| provide this functionality without requiring any additional code.
		|
		*/

		use RegistersUsers;

		/**
		 * Where to redirect users after registration.
		 *
		 * @var string
		 */
		protected $redirectTo = RouteServiceProvider::HOME;
		/**
		 * @var UserRepository
		 */
		private $userRepository;

		public function __construct(UserRepository $userRepository)
		{
			parent::__construct();
			$this->middleware('guest');
			$this->userRepository = $userRepository;
		}

		/**
		 * Get a validator for an incoming registration request.
		 *
		 * @param array $data
		 * @return \Illuminate\Contracts\Validation\Validator
		 */
		protected function validator(array $data)
		{
			return Validator::make($data, [
				'test' => ['nullable'],
			]);
		}

		/**
		 * Create a new user instance after a valid registration.
		 *
		 * @param array $data
		 * @return \App\User
		 */
		protected function create(array $data)
		{
			return User::create([
				'fio'      => $data['fio'],
				'phone'    => $data['phone'],
				'password' => Hash::make($data['password'] ?? Str::random()),
			]);
		}

		public function showRegistrationForm()
		{
			$data['content'] = view('auth.registration.register');
			return $this->main($data);
		}

		public function register(UserRegisterPersonalRequest $request)
		{
			debugInfo($request->get('phone'));
			$this->validator($request->all())->validate();

			event(new Registered($user = $this->create($request->all())));


			if ($response = $this->registered($request, $user)) {
				return $response;
			}

			return $request->wantsJson()
				? new Response('', 201)
				: redirect($this->redirectPath());
		}

		protected function registered(Request $request, $user)
		{
			UserRegistrationStepsService::resolveSelf()->registerStepOneSuccessSubmitted();
			return redirect()->route('registration.confirm');
		}

		public function showRegistrationFormCode(
			UserRegistrationStepsService $registrationStepsService
			, PhoneVerificationService $phoneVerificationService
		)
		{
			$user = $this->userRepository->find($registrationStepsService->getUserId());
			$phone = $user->getAttribute('phone');
			$verification = $phoneVerificationService->getVerification($phone);
			$with = compact(
				'phone'
				, 'user'
				, 'verification'
			);
			$data['content'] = view('auth.registration.confirm')->with($with);
			return $this->main($data);
		}

		public function confirmCode(
			BaseSendConfirmPhoneRequest $request
			, PhoneVerificationService $phoneVerificationService
			, PhoneVerificationDataKeeper $phoneVerificationDataKeeper
		)
		{
			if ($verification = $phoneVerificationService->getVerification($phoneVerificationDataKeeper->getPhone())) {
				event(app(PhoneVerifiedEvent::class, compact('verification')));
			}

			UserRegistrationStepsService::resolveSelf()->registerStepTwoSuccessSubmitted();
			return redirect()->route('registration.password');
		}

		public function showRegistrationFormPassword()
		{
			$data['content'] = view('auth.registration.password');
			return $this->main($data);
		}

		public function sendPassword(UserRegisterPasswordRequest $request, UserRegistrationStepsService $registrationStepsService)
		{
			$password = Hash::make($request->get('password'));
			$user = $this->userRepository->find($registrationStepsService->getUserId());

			$this->userRepository->update(compact('password'), $user);
			event(app(SendPasswordEvent::class));

			$this->guard()->login($user);

			$this->setSuccessMessage(getTranslate('registration.success', 'Вы успешно зарегистрированы'));

			return redirect($this->redirectTo)->with($this->getResponseMessage());
		}
	}
