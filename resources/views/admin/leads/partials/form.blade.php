@include('admin.partials.crud.elements.name')

@include('admin.partials.crud.default', ['name' => 'subscription', 'title' => __('Подпись')])

<div class="row">
    <div class="col-6">
        @include('admin.partials.crud.elements.active')
    </div>
</div>

<div class="w-75">@include('admin.partials.crud.elements.image-upload-group')</div>

@include('admin.partials.crud.textarea.description')

