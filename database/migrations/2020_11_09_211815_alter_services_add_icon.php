<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Builders\Migration\MigrationBuilder;

class AlterServicesAddIcon extends Migration
{
/**
	 * @var MigrationBuilder
	 */
	private $builder;

    public function __construct()
    {
      $this->builder = app(MigrationBuilder::class);
    }


    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $this->builder->setTable($table);
	        $this->builder->createImage('icon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('icon');
        });
    }
}
