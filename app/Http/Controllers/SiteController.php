<?php

namespace App\Http\Controllers;

use App\Models\Meta;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;

abstract class SiteController extends BaseController
{

	protected $lang;

	public function __construct()
	{
		parent::__construct();
		$this->addBreadCrumb(getTranslate('global.home-bread-name', 'Главная'), route('home'));
		$this->lang = getLang();
		$this->checkForCanonical();
	}

	public function main(array $data = [])
	{
		$this->checkMetaData();
		Arr::set($data, 'breadcrumbs', \Arr::get($data, 'breadcrumbs', $this->getBreadCrumbs()));
		return view('public.layout.app', $data);
	}


	public function checkMetaData(): void
	{
		if ($metadata = Meta::getMetaData()) {
			if ($metadata->title) {
				$this->setTitle($metadata->title, true);
			}
			if ($metadata->description) {
				$this->setDescription($metadata->description, true);
			}
			if ($metadata->keywords) {
				$this->setKeywords($metadata->keywords, true);
			}
			return;
		}
		if ((!$this->getTitle() || !$this->getDescription() || !$this->getKeywords()) && ($meta_default = Meta::getDefaultMeta())) {
			if (!$this->getTitle()) {
				$this->setTitle($meta_default->title);
			}
			if (!$this->getDescription()) {
				$this->setDescription($meta_default->description);
			}
			if (!$this->getKeywords()) {
				$this->setKeywords($meta_default->keywords);
			}
		}
	}


	protected function checkForCanonical(): void
	{
		if (request()->has('page')) {
			$this->setCanonical(url()->current());
		}
	}

	protected function setNextPrevLinkForPagination(LengthAwarePaginator $paginator): void
	{
		$nextUrl = $paginator->nextPageUrl();
		$prevUrl = $paginator->previousPageUrl();

		if ($paginator->currentPage() === 2) {
			$this->setPrev(url()->current());
		} else if ($prevUrl !== null) {
			$this->setPrev($prevUrl);
		}
		if ($paginator->hasMorePages()) {
			$this->setNext($nextUrl);
		}
	}
}











