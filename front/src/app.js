import AOS from 'aos'
require('./js/plugins.js')
require('./js/menu')
require('./js/header')
require('./js/sliders')



const header = $('header')
const menu = $('.aside-menu')
const languageMenu = $('.language-menu');
const headerLogo = $('.header-logo')
const headerLogo2 = $('.header-logo2')
const services = $('.service');

AOS.init({
    offset: 0,
    once: true,
    initClassName: 'aos-init',
    animatedClassName: 'aos-animate'
});




$('body').on('click', function (e) {
    const target = $(e.target);

    if(menu.hasClass('active')) {
        if( !target.parents('.aside-menu').hasClass('aside-menu') && !target.hasClass('.menu-nav_menu-toggle')) {
            menu.removeClass('active');
            $('.backdrop').removeClass('active')
                $('html, body').removeClass('active')
        }
    }

    if (languageMenu.hasClass('active')) {
        if(!target.parents('.language-toggler').hasClass('language-toggler')) {
            languageMenu.removeClass('active');
        }
    }

})

function checkServices() {
    if($(services[services.length-1]).hasClass('aos-animate')) {
        $('.services-wrap').addClass('animated');
    }
}

setTimeout(checkServices, 500)

window.addEventListener('scroll', function () {
    const trigger = 30;
    let top = Math.trunc(this.pageYOffset)
    const head = document.querySelector('header');
    if (top > trigger) {
        head.classList.add('active')
        AOS.refresh()
    } else {
        head.classList.remove('active')
    }

    checkServices()
})



// $(window).on('scroll', function () {
//     const trigger = 30;
//     let top = Math.trunc($(window).scrollTop())
//
//     if (top > trigger) {
//         header.addClass('active')
//     } else {
//         header.removeClass('active')
//     }
// })



$('.slick-dots button').text('')

const id = $('main').attr('id');

switch (id) {
    case 'services-page':
        $('.menu_item-services').addClass('active');
        headerLogo.remove();
        headerLogo2.addClass('active');
        break;
    case 'main-page':
        header.removeClass('other-page')
        break;
    case 'about-page':
        $('.menu_item2-about').addClass('active')
        headerLogo.remove();
        headerLogo2.addClass('active');
        break
    case 'main-contacts':
        $('.menu_item2-contacts').addClass('active')
        headerLogo.remove();
        headerLogo2.addClass('active');
        break;
    case 'main-service':
        $('.menu_item-service').addClass('active')
        headerLogo.remove();
        headerLogo2.addClass('active');
        break;
}