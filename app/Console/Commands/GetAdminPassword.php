<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class GetAdminPassword extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'auth:admin-pass {name=superadmin}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * @return int
	 */
	public function handle()
	{
		if (env('APP_ENV') !== 'local'){
			$this->warn('Dont run this on production');
			return 0;
		}
		$adminName = $this->argument('name');
		if ($adminName) {
			if (!$password = User::getPassword($adminName)) {
				$this->warn(sprintf('User %s not found', $adminName));
				return 0;
			}
			$this->info($password);
			$this->warn('Dont forget change this password on production');
		}

		return 1;
	}
}
