<?php

	namespace App\Http\Requests\Admin;

	use App\Contracts\Requests\RequestParameterModelable;
	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\AbstractRequest;
	use App\Traits\Requests\Helpers\GetActionModel;

	class ServiceRequest extends AbstractRequest implements RequestParameterModelable
	{
		use GetActionModel;

		protected $toBooleans = ['active'];

		protected $requestKey = 'service';

		public function rules(): array
		{
			$rules = [
				'url'         => ['required', 'unique' => 'unique:services,url', 'max:160'],
				'name'        => ['required', 'string', 'max:255'],
				'excerpt'     => ['max:' . ValidationMaxLengthHelper::TEXT],
				'description' => ['max:' . ValidationMaxLengthHelper::TEXT],
				'image'       => ['nullable', 'image'],
			];

			if ($parameterModel = $this->getActionModel()) {
				$rules['url']['unique'] .= ',' . $parameterModel->id;
			}
			return $rules;
		}

		public function mergeRequestValues()
		{
			$this->mergeUrlFromName();
		}
	}
