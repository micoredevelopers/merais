<div class="statistic">
    <span class="statistic-year">{{ getSetting('main-counters.first') }}</span>
    <span class="statistic-desc">{{ getTranslate('main-counters.first', 'Лет опыта') }}</span>
</div>
<div class="statistic">
    <span class="statistic-year">{{ getSetting('main-counters.second') }}</span>
    <span class="statistic-desc">{{ getTranslate('main-counters.second', 'Сотрудников') }}</span>
</div>
<div class="statistic">
    <span class="statistic-year">{{ getSetting('main-counters.third') }}</span>
    <span class="statistic-desc">{{ getTranslate('main-counters.third', 'Стран') }}</span>
</div>