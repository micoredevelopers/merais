<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Builders\Migration\MigrationBuilder;

class AddFieldsToServicesTables extends Migration
{
/**
	 * @var MigrationBuilder
	 */
	private $builder;

    public function __construct()
    {
      $this->builder = app(MigrationBuilder::class);
    }


    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->string('banner', 255)->nullable();
        });
        Schema::table('service_langs', function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->string('name_second', 255)->nullable();
            $table->text('text_first')->nullable();
            $table->text('text_second')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->dropColumn('banner');
        });

        Schema::table('service_langs', function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->dropColumn('name_second');
            $table->dropColumn('text_first');
            $table->dropColumn('text_second');
        });
    }
}
