<?php

	use App\Builders\Settings\SettingBuilder;
	use App\Models\Setting;
	use App\Repositories\Admin\SettingsRepository;
	use Illuminate\Database\Seeder;

	class SettingsTableSeeder extends Seeder
	{
		private $settings;
		/**
		 * @var SettingsRepository
		 */
		private $repository;

		public function __construct(SettingsRepository $repository)
		{
			$this->settings = $repository->withTrashed()->get()->keyBy('key');
			$this->repository = $repository;
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$groups = [
				'global'        => [
					$this->getBuilder('sitename')->setValue('Merais')->setDisplayName('Название сайта')->build(),
					$this->getBuilder('address')->setValue('Odessa, 4641 Briana Center')->setDisplayName('Адесс')->build(),
					$this->getBuilder('phone-one')->setValue('0 800 33 6278')->setDisplayName('Номер телефона #1')->build(),
					$this->getBuilder('phone-two')->setValue('+38 (063) 291 29 61')->setDisplayName('Номер телефона #2')->build(),
					$this->getBuilder('currency')->setValue('грн.')->setDisplayName('Название валюты')->build(),
				],
				'email'         => [
					$this->getBuilder('contact-email')->setValue(isLocalEnv() ? 'exceptions.manticore@gmail.com' : 'merais@gmail.com')->setDisplayName('e-mail Администратора (для уведомлений)')->build(),
					$this->getBuilder('name-from')->setValue('Merais')->setDisplayName('Имя отправителя в письме')->build(),
					$this->getBuilder('email-from')->setValue('info@merais.com.ua')->setDisplayName('E-mail отправителя в письмах')->build(),
					$this->getBuilder('public-email')->setValue('info@merais.com')->setDisplayName('E-mail для контактов')->build(),
				],
				'socials'       => [
					$this->getBuilder('facebook')->setDisplayName('Facebook account')->setValue('')->build(),
					$this->getBuilder('instagram')->setDisplayName('Instagram account')->setValue('')->build(),
				],
				'google'        => [
					$this->getBuilder('gtag-head')->setDisplayName('Google tag Head')->setTypeTextarea()->setValue('')->build(),
					$this->getBuilder('gtag-body')->setDisplayName('Google tag Body')->setTypeTextarea()->setValue('')->build(),
				],
				'feedback'      => [
					$this->getBuilder('download')->setDisplayName('Файл для скачивания в письме обратной связи')->setTypeFile()->setValue(null)->build(),
					$this->getBuilder('head-text')->setDisplayName('Подись в верху письма для клиента при обратной связи')->setTypeEditor()->build(),
				],
				'js'            => [
					$this->getBuilder('scripts.head')->setDisplayName('Скрипты в HEAD')->setTypeTextarea()->setValue(null)->build(),
					$this->getBuilder('scripts.body')->setDisplayName('Скрипты в BODY (в верху BODY)')->setTypeTextarea()->setValue(null)->build(),
					$this->getBuilder('scripts.body.footer')->setDisplayName('Скрипты в BODY (в футере)')->setTypeTextarea()->setValue(null)->build(),
				],
				'services'      => [
					$this->getBuilder('detail.image')->setDisplayName('Изображение в блоке Детальнее о подходе, на странице услуги')->setTypeFile()->setValue(null)->build(),
				],
				'main-counters' => [
					$this->getBuilder('first')->setDisplayName('Первый счетчик')->setValue('7')->build(),
					$this->getBuilder('second')->setDisplayName('Второй счетчик')->setValue('15')->build(),
					$this->getBuilder('third')->setDisplayName('Третий счетчик')->setValue('6')->build(),
				],
			];
			$groups = array_reverse($groups);
			foreach ($groups as $groupName => $settings) {
				foreach ($settings as $setting) {
					$setting['group'] = $groupName;
					$setting['key'] = implode('.', [$groupName, $setting['key']]);
					if ($this->settingExists($setting['key'])) {
						continue;
					}
					$this->repository->create($setting);
				}
			}
		}

		private function getSettingByKey(string $key): ?Setting
		{
			return \Arr::get($this->settings, $key);
		}

		private function settingExists(string $key): bool
		{
			return (bool)$this->getSettingByKey($key);
		}

		private function getBigText(string $id)
		{
			$arr = [
				'seo.head-global-codes' => '',
			];

			return \Arr::get($arr, $id);
		}

		private function getBuilder(?string $key): SettingBuilder
		{
			$builder = new SettingBuilder();
			if ($key) {
				$builder->setKey($key);
			}
			return $builder;
		}

	}
