<?php

namespace App\View\Components\Layout;

use App\Repositories\MenuRepository;
use Illuminate\Support\Arr;
use Illuminate\View\Component;

class Sidebar extends Component
{
	/**
	 * @var MenuRepository
	 */
	private $menuRepository;

	/**
	 * Create a new component instance.
	 *
	 * @return void
	 */
	public function __construct(MenuRepository $menuRepository)
	{
		$this->menuRepository = $menuRepository;
	}

	public function render()
	{
		$menus = $this->menuRepository->getMenus(getLang());
		$mainMenu = Arr::get($menus, 'main_menu', collect([]));
		return view('components.layout.sidebar')->with(compact('mainMenu'));
	}

}
