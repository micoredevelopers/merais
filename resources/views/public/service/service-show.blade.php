<?php /** @var $service \App\Models\Service\Service */ ?>
<?php ?>
<main id="main-service">
    <x-layout.breadcrumbs/>

    <section class="service-section">
        <div class="container">
            <div class="service-section_title">
                <div data-aos="clipTop" data-aos-duration="600" data-aos-delay="200" data-aos-once="true"
                     class="aos-init aos-animate">
                    {{ $service->getName() }}
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="service-section_desc">
                        @foreach($service->getArrayedDescription() as $itemDescription)
                            @php
                                /** @var $loop StdClass */
                                $delay = 200 + (200 * $loop->iteration);
                            @endphp
                            <div data-aos="offsetTop" data-aos-duration="600" data-aos-delay="{{ $delay }}"
                                 data-aos-once="true"
                                 class="aos-init aos-animate" style="white-space: pre-line">
                                {!! $itemDescription !!}
                            </div>
                        @endforeach
                    </div>
                    <div class="service-section_statistic">
                        @foreach($service->getCounters() as $counter)
                            @php
                                $delay =  1000 + (200 * $loop->index)
                            @endphp
                            <div class="statistic aos-init aos-animate" data-aos="offsetTop" data-aos-duration="600"
                                 data-aos-delay="{{ $delay }}"
                                 data-aos-once="true">
                                <span class="statistic-year">{{ $counter->getCounterKey() }}</span><span
                                        class="statistic-desc">{{ $counter->getCounterValue() }}</span>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    @if(storageFileExists($service->image))
                        <img class="svg service-section_img aos-init aos-animate" data-aos="clipRight"
                             data-aos-duration="1000"
                             data-aos-delay="1600" data-aos-once="true"
                             src="{{ getPathToImage(imgPathOriginal($service->image)) }}">
                    @endif
                </div>
            </div>
        </div>
    </section>

    @include('public.service.includes.advantages')

    @include('public.service.includes.about-skills')

    @include('public.service.includes.other-services')
</main>

{!! editButtonAdmin(route(routeKey('services', 'edit'), $service->getKey()), 'Редактировать услугу') !!}