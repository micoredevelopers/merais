<?php

	namespace App\Models\Advantage;


	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Traits\Models\HasGetterDescriptionTrait;
	use App\Traits\Models\HasGetterNameTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;

	class Advantage extends Model implements HasLocalized
	{
		use RedirectLangColumn;
		use HasGetterDescriptionTrait;
		use HasGetterNameTrait;

		protected $langColumns = ['name', 'description'];

		protected $guarded = ['id'];

		protected $hasOneLangArguments = [AdvantageLang::class];

	}
