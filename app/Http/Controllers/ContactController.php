<?php

	namespace App\Http\Controllers;

	use App\Repositories\PageRepository;
	use App\Repositories\ServiceRepository;
	use App\Services\SMS\Sender;
	use Illuminate\Filesystem\Filesystem;
	use Illuminate\Http\Request;

	class ContactController extends SiteController
	{
		/**
		 * @var ServiceRepository
		 */
		private $serviceRepository;
		/**
		 * @var PageRepository
		 */
		private $pageRepository;

		public function __construct(
			PageRepository $pageRepository, ServiceRepository $serviceRepository)
		{
			parent::__construct();
			$this->serviceRepository = $serviceRepository;
			$this->pageRepository = $pageRepository;
		}

		public function index()
		{
			$page = $this->pageRepository->findPageByPageType('contacts');
			$with = compact('page');
			$this->setTitle($page->name)->addBreadCrumb($page->name);
			$this->shareIndex();
			$data['content'] = view('public.contact.contact')->with($with);
			return $this->main($data);
		}

		protected function shareIndex()
		{
			view()->share(compact(array_keys(get_defined_vars())));
		}
	}
