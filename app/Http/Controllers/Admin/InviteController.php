<?php

	namespace App\Http\Controllers\Admin;

	use App\Http\Requests\Admin\InviteRequest;
	use App\Models\Invite;
	use App\Repositories\InviteRepository;

	class InviteController extends AdminController
	{

		private $moduleName;

		protected $key = 'invites';

		protected $routeKey = 'admin.invites';

		protected $permissionKey = 'invites';
		/**
		 * @var InviteRepository
		 */
		private $repository;

		public function __construct(InviteRepository $repository)
		{
			parent::__construct();
			$this->moduleName = __('modules.invites.title');
			$this->addBreadCrumb($this->moduleName, $this->resourceRoute('index'));
			$this->shareViewModuleData();
			$this->repository = $repository;
		}

		public function index()
		{
			$this->setTitle($this->moduleName);
			$list = $this->repository->getListAdmin();
			$with = compact('list');
			$data['content'] = view('admin.invites.index', $with);
			return $this->main($data);
		}


		public function destroy(Invite $invite)
		{
			if (!$invite->canDelete()) {
				$this->setFailMessage('Удаление этой записи не доступно');
				return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
			}
			if ($this->repository->delete($invite->id)) {
				$this->setSuccessDestroy();
				$this->fireEvents();
			}

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		private function fireEvents()
		{
		}
	}
