
function mainPageSlider() {
    const slider = $('.main-page_slider');
    const progress = $('.main-page_slider-wrap .slider-pag_progress');
    const currentSlidePag = $('.slider-pag_current');
    const slides = $('.main-page_slide');
    let slide = 0;
    let counter = 0;


    slider.on('init', function (e, { slideCount, currentSlide }) {
        const all = `${slideCount}`
        const current = `${currentSlide+1}`
        progress.css('width',(current/ all *100 + '%') );
        $('.slider-section_pag .slider-pag_all').text(all);
        $('.slider-section_pag .slider-pag_current').text(current);
    })

    slider.slick({
        fade: false,
        slidesToShow: 1,
        prevArrow: '.slider-arrow_prev-wrap',
        nextArrow: '.slider-arrow_next-wrap',
        speed: 0,
        cssEase: 'linear'
    });

    slider.on('afterChange', function (e, { slideCount,currentSlide} ) {
        const current = `${currentSlide + 1}`
        progress.css('width',(current/ slideCount *100 + '%') );

        setTimeout(function () {
            currentSlidePag.css('opacity', '1');
            $('.slider-section_pag .slider-pag_current').text(current)
        }, 750)

        counter = currentSlide;
        const currentItem = $(slides[currentSlide]);

         currentItem.addClass('active-slide')
        setTimeout(function () {
            slides.removeClass('next-slide');
            slides.removeClass('prev-slide')
            currentItem.removeClass('active-slide')
        }, 1700)
    })


    slider.on('beforeChange', function (e, slick ,currentSlide, nextSlide) {
        currentSlidePag.css('opacity', '0');
        slide = currentSlide;


        const next = $(slides[nextSlide]);


        if(currentSlide === slides.length-1 && nextSlide === 0) {
            next.addClass('next-slide')
        } else if(currentSlide === 0 && nextSlide === slides.length-1) {
            next.addClass('prev-slide')
        }
        else  if(nextSlide<currentSlide) {
            next.addClass('prev-slide')
        }
        else if(nextSlide > currentSlide){
            next.addClass('next-slide')
        }


        const current = $(slides[currentSlide]);
        current.css({ 'opacity': '1','zIndex': '199999'});
        setTimeout(function () {
            current.css({'opacity': '0', 'zIndex': '200'});
        }, 900)

    })
}

mainPageSlider()

function mainAboutSlider() {
    const slider = $('.about-section_slider');
    const slides = $('.about-section_slide');
    let slide = 0;

    slider.slick({
        fade: false,
        slidesToShow: 1,
        prevArrow: '.slider-pag_prev-wrap',
        nextArrow: '.slider-pag_next-wrap',
        dots: true,
        appendDots: '.slider-pag_dots',
        speed: 0,
        cssEase: 'linear'
    });

    slider.on('afterChange', function (e, {currentSlide}){
        slides.removeClass('animation');
        slides.removeClass('animation2');


        const currentItem = $(slides[currentSlide]);

        currentItem.addClass('active-slide')
        setTimeout(function () {
            slides.removeClass('next-slide');
            slides.removeClass('prev-slide')
            currentItem.removeClass('active-slide')
        }, 1200)
    })

    slider.on('beforeChange', function (e, slick ,currentSlide, nextSlide) {
        slide = currentSlide;


        const next = $(slides[nextSlide]);


        if(currentSlide === slides.length-1 && nextSlide === 0) {
            next.addClass('next-slide')
        } else if(currentSlide === 0 && nextSlide === slides.length-1) {
            next.addClass('prev-slide')
        }
        else  if(nextSlide<currentSlide) {
            next.addClass('prev-slide')
        }
        else if(nextSlide > currentSlide){
            next.addClass('next-slide')
        }


        const current = $(slides[currentSlide]);
        current.css({ 'opacity': '1','zIndex': '199999'});
        setTimeout(function () {
            current.css({'opacity': '0', 'zIndex': '200'});
        },600)
    })

}

mainAboutSlider();