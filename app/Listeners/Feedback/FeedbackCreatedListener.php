<?php

namespace App\Listeners\Feedback;

use App\Events\Feedback\FeedbackCreatedEvent;
use App\Helpers\Debug\LoggerHelper;
use App\Mail\Feedback\FeedbackDefault;
use App\Mail\Feedback\FeedbackUserSendFile;
use App\Models\Feedback\Feedback;
use Illuminate\Support\Facades\Mail;

class FeedbackCreatedListener
{

    /**
     * Handle the event.
     *
     * @param FeedbackCreatedEvent $event
     * @return void
     */
    public function handle(FeedbackCreatedEvent $event)
    {
        /** @var $feedback Feedback */
        if (!$feedback = $event->getFeedback()) {
            return;
        }

        try {
            if ($feedback->isTypeFeedback()) {
                $this->onFeedback($feedback);
            }
        } catch (\Exception $e) {
            app(LoggerHelper::class)->error($e);
        }
    }

    private function onFeedback(Feedback $feedback): void
    {
        Mail::queue(new FeedbackDefault($feedback));
//		Mail::queue(new FeedbackUserSendFile($feedback));
    }

}
