<?php

	namespace App\Models\Service;


	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Models\Service\Traits\ServiceHelpers;
	use App\Traits\Models\Localization\RedirectLangColumn;
	use Illuminate\Database\Eloquent\Relations\HasMany;
	use Illuminate\Support\Collection;

	class Service extends Model implements HasLocalized
	{
		use ServiceHelpers;
		use RedirectLangColumn;


		protected $hasOneLangArguments = [ServiceLang::class];

		protected $langColumns = ['name', 'title', 'description', 'excerpt', 'name_second', 'text_first', 'text_second'];

		protected $casts = [
			'fee' => 'integer',
		];

		protected $guarded = ['id'];

		protected $fillable = [
			'image',
			'type',
			'banner',
		];

		public function getName(): string
		{
			return (string)($this->name ?? '');
		}

		public function getDescription(): string
		{
			return (string)($this->description ?? '');
		}

		public function getExcerpt(): string
		{
			return (string)($this->excerpt ?? '');
		}

		public function getArrayedDescription(): array
		{
			return explodeByEol($this->getDescription());
		}

		public function getUrl()
		{
			return (string)($this->url ?? '');
		}

		public function counters(): HasMany
		{
			return $this->hasMany(ServiceCounter::class);
		}

        public function getNameSecond(): string
        {
            return (string)($this->name_second ?? '');
        }

        public function getTextFirst(): string
        {
            return (string)($this->text_first ?? '');
        }

        public function getTextSecond(): string
        {
            return (string)($this->text_second ?? '');
        }

        public function getBanner()
        {
            return $this->getAttribute('banner');
        }

		/**
		 * @return Collection | ServiceCounter[]
		 */
		public function getCounters(): Collection
		{
			return $this->counters;
		}

		public function getIcon()
		{
			return $this->getAttribute('icon');
		}
	}
