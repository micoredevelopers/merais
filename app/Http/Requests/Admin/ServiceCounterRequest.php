<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\AbstractRequest;

class ServiceCounterRequest extends AbstractRequest
{
	protected $toBooleans = ['active'];

	public function rules(): array
	{
		$rules = [
			'key'        => ['string', 'max:50'],
			'value'      => ['string', 'max:255'],
			'service_id' => ['required', 'exists:services,id'],
		];

		return $rules;
	}

	public function getFillable(): array
	{
		return $this->only(array_keys($this->rules()));
	}

}
