<?php

namespace App\Providers;

use App\Events\Admin\MenusChanged;
use App\Events\Feedback\FeedbackCreatedEvent;
use App\Events\Order\OrderPhoneConfirmSubmittedEvent;
use App\Events\Order\OrderServiceSubmittedEvent;
use App\Events\Order\Payment\InvoiceCreatedEvent;
use App\Events\Order\Payment\MonthlyFeePaymentGeneratedEvent;
use App\Events\Order\Payment\PaymentCallbackEvent;
use App\Events\Order\Steps\SuccessPageLoadEvent;
use App\Events\Phone\PhoneVerifiedEvent;
use App\Events\Phone\VerificationCreatedEvent;
use App\Events\Phone\VerificationGettedEvent;
use App\Events\User\Registration\SendPasswordEvent;
use App\Listeners\Admin\Menu\DropMenuCache;
use App\Listeners\Admin\User\OnUserAuth;
use App\Listeners\Feedback\FeedbackCreatedListener;
use App\Listeners\Order\OrderPhoneConfirmSubmittedListener;
use App\Listeners\Order\OrderServiceSubmittedCheckRegisterUser;
use App\Listeners\Order\OrderServiceSubmittedListener;
use App\Listeners\Order\OrderServiceSubmittedNotifyListener;
use App\Listeners\Order\Payment\MonthlyFeePaymentGeneratedListener;
use App\Listeners\Order\Payment\PaymentCallbackFeeListener;
use App\Listeners\Order\Payment\PaymentCallbackListener;
use App\Listeners\Order\Payment\PaymentCallbackOrderStatusListener;
use App\Listeners\Order\Payment\PaymentCallbackRemovePaymentOnFailListener;
use App\Listeners\Order\Payment\PaymentCallbackSendPaymentStatusListener;
use App\Listeners\Order\Steps\SuccessPageLoadListener;
use App\Listeners\Phone\VerificationCreatedListener;
use App\Listeners\Phone\VerificationGettedListener;
use App\Listeners\User\Registration\SendPasswordListener;
use App\Listeners\User\Registration\UserRegisteredFirstStepListener;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class EventServiceProvider extends ServiceProvider
{
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		Registered::class                                    => [
			SendEmailVerificationNotification::class,
			UserRegisteredFirstStepListener::class,
		],
		Login::class                                         => [
			OnUserAuth::class,
		],
		MenusChanged::class                                  => [
			DropMenuCache::class,
		],
		\App\Events\Admin\Image\ImageUploaded::class         => [
//            \App\Listeners\Admin\Image\ImageUploadedAddWatermarkListener::class,
            \App\Listeners\Admin\Image\ImageUploadedAddThumbnailsListener::class,
		],
		\App\Events\Admin\Image\MultipleImageUploaded::class => [
//				\App\Listeners\Admin\Image\MultipleImageUploadedListener::class,
		],
		OrderServiceSubmittedEvent::class                    => [
			OrderServiceSubmittedCheckRegisterUser::class,
			OrderServiceSubmittedListener::class,
			OrderServiceSubmittedNotifyListener::class,
		],
		OrderPhoneConfirmSubmittedEvent::class               => [
			OrderPhoneConfirmSubmittedListener::class,
		],
		InvoiceCreatedEvent::class                           => [
		],
		PaymentCallbackEvent::class                          => [
			PaymentCallbackFeeListener::class,
			PaymentCallbackListener::class,
			PaymentCallbackOrderStatusListener::class,
			PaymentCallbackSendPaymentStatusListener::class,
			PaymentCallbackRemovePaymentOnFailListener::class,
		],
		FeedbackCreatedEvent::class                          => [
			FeedbackCreatedListener::class,
		],
		VerificationGettedEvent::class                       => [
			VerificationGettedListener::class,
		],
		VerificationCreatedEvent::class                      => [
			VerificationCreatedListener::class,
		],
		PhoneVerifiedEvent::class                            => [
			\App\Listeners\Phone\PhoneVerifiedListener::class,
		],
		SendPasswordEvent::class                             => [
			SendPasswordListener::class,
		],
		SuccessPageLoadEvent::class                          => [
			SuccessPageLoadListener::class,
		],
		MonthlyFeePaymentGeneratedEvent::class               => [
			MonthlyFeePaymentGeneratedListener::class,
		],

	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
		parent::boot();
	}
}
