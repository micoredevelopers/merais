<?php

namespace App\Listeners\Admin\Menu;

use App\Events\Admin\MenusChanged;
use App\Models\Menu;
use App\Models\Page;
use App\Repositories\Admin\LanguageRepository;
use App\Repositories\MenuRepository;

class DropMenuCache
{
	private $menuRepository;
	/**
	 * @var LanguageRepository
	 */
	private $languageRepository;

	/**
	 * DropMenuCache constructor.
	 * @param MenuRepository $menuRepository
	 */
	public function __construct(MenuRepository $menuRepository, LanguageRepository $languageRepository)
	{
		$this->menuRepository = $menuRepository;
		$this->languageRepository = $languageRepository;
	}

	/**
	 * @param MenusChanged $event
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function handle(MenusChanged $event)
	{
		$cacheKey = Menu::$publicMenusCacheKey;
		\Cache::forget($cacheKey);
		try{
			foreach ($this->languageRepository->all() as $language) {
				$this->menuRepository->getMenus($language->id);
			}
		} catch (\Exception $exception){

		}
	}
}
