<?php /** @var $edit \App\Models\Equipment\Equipment */ ?>

<form action="{{ route($routeKey.'.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')

    @include('admin.partials.submit_update_buttons')

    @include('admin.advantages.partials.form')

    @include('admin.partials.submit_update_buttons')
</form>


