<?php

	namespace App\Http\Controllers\Admin\Service;

	use App\Containers\Admin\Service\CounterSearchDataContainer;
	use App\Helpers\Media\ImageRemover;
	use App\Http\Controllers\Admin\AdminController;
	use App\Http\Requests\Admin\ServiceCounterRequest;
	use App\Models\Service\ServiceCounter;
	use App\Repositories\ServiceCounterRepository;
	use App\Repositories\ServiceRepository;
	use App\Traits\Authorizable;
	use Illuminate\Http\Request;

	class ServiceCountersController extends AdminController
	{
		use Authorizable;

		private $moduleName;

		protected $key = 'service-counters';

		protected $routeKey = 'admin.service-counters';

		protected $permissionKey = 'service-counters';
		/**
		 * @var ServiceCounterRepository
		 */
		private $repository;

		public function __construct(ServiceCounterRepository $repository, ServiceRepository $serviceRepository)
		{
			parent::__construct();
			$this->moduleName = __('modules.service-counters.title');
			$this->addBreadCrumb($this->moduleName, $this->resourceRoute('index'));
			$this->shareViewModuleData();
			view()->share(['services' => $serviceRepository->getListSelect()]);
			$this->repository = $repository;
		}

		public function index(CounterSearchDataContainer $searchDataContainer, Request $request)
		{
			$searchDataContainer->setSearch((string)$request->get('search'))->setServiceId($request->get('service_id'));
			$this->setTitle($this->moduleName);
			$list = $this->repository->getListAdmin($searchDataContainer);
			$list->load('service');
			$with = compact('list');
			$data['content'] = view('admin.service-counters.index', $with);
			return $this->main($data);
		}

		/**
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function create()
		{
			$data['content'] = view('admin.service-counters.create');
			return $this->main($data);
		}

		public function store(ServiceCounterRequest $request)
		{
			$input = $request->getFillable();
			if ($serviceCounter = $this->repository->create($input)) {
				$this->setSuccessStore();
			}
			return $this->redirectOnCreated($serviceCounter);
		}

		public function edit(ServiceCounter $serviceCounter)
		{
			$serviceCounter->load('lang');
			$edit = $serviceCounter;
			$title = $this->titleEdit($edit);
			$this->addBreadCrumb($title)->setTitle($title);
			$data['content'] = view('admin.service-counters.edit', compact(
				'edit'
			));
			return $this->main($data);
		}

		public function update(ServiceCounterRequest $request, ServiceCounter $serviceCounter)
		{
			$input = $request->getFillable();

			if ($this->repository->update($input, $serviceCounter)) {
				$this->setSuccessUpdate();
			}

			return $this->redirectOnUpdated($serviceCounter);
		}

		/**
		 * @param ServiceCounter $service
		 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 * @throws \Exception
		 */
		public function destroy(ServiceCounter $service, ImageRemover $imageRemover)
		{
			if (!$service->canDelete()) {
				$this->setFailMessage('Удаление этой записи не доступно');
				return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
			}
			if ($this->repository->delete($service->id)) {
				$imageRemover->removeImage($service->image);
				$this->setSuccessDestroy();
			}

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

	}
