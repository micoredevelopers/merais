<header class="header other-page">
    <div class="container">
        <a class="header-logo_wrap" href="{{ route('home') }}">
            <img class="header-logo" src="{{ asset('images/logo2.png') }}" alt=""/>
            <img class="header-logo2" src="{{ asset('images/logo1.png') }}" alt=""/>
        </a>
        <a class="header-phone" href="tel: +{{ formatTel(getSetting('global.phone-one')) }}">
            <span>{{ getSetting('global.phone-one') }}</span>
            <img class="svg" src="{{ asset('images/icons/phone.svg') }}" alt=""/>
        </a>
        <button class="common-btn" type="button" data-toggle="modal" data-target="#callback-modal">
			{{ getTranslate('contacts.connect_with_us', "Зв`язатися з нами") }}
		</button>
        <x-layout.languages :arrows="false"/>
    </div>
</header>