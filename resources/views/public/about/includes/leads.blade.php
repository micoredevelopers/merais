<?php /** @var $leads \Tightenco\Collect\Support\Collection | \App\Models\Lead\Lead[] */ ?>
<section class="leaders-section">
    <div class="container">
        <div class="row leaders-section_content">
            <div class="col-12 col-lg-6">
                <div class="leaders-section_title" data-aos="clipTop" data-aos-duration="600" data-aos-once="true">
                    {{ getTranslate('about.leads', 'Руководители') }}
                </div>
                <div class="leaders-section_subtitle" data-aos="offsetTop" data-aos-duration="600"
                     data-aos-delay="200" data-aos-once="true">
                    {{ getTranslate('about.leads.title', 'Мы открылись в 2013 году. Два учредителя - Анна и Мария. ') }}
                    @includeIf('public.layout.elements.translate-edit', ['key' => 'about.leads'])
                </div>
            </div>
            <div class="col-12 col-lg-6 d-flex flex-column align-items-end">
                @foreach(explodeByEol(getTranslate('about.leads.description', '')) as $text)
                    @php
                        $delay = 200 + (200 * $loop->iteration)
                    @endphp
                    <div class="leaders-section_desc" data-aos="offsetTop" data-aos-duration="600"
                         data-aos-delay="{{ $delay }}"
                         data-aos-once="true">{{ $text }}</div>
                @endforeach
            </div>
        </div>
        @if($leads ?? false)

            <div class="row leaders-section_info-wrap">
                @foreach($leads as $lead)
                    @php
                        $class = $loop->even ? 'd-flex flex-column align-items-end' : '';
                    @endphp

                    <div class="col-12 col-lg-6 leaders-section_info {{ $class }}">
                        <div class="leader-info">
                            <img class="leaders-section_inf img"
                                 src="{{ getPathToImage(imgPathOriginal($lead->image)) }}" data-aos="offsetTop"
                                 data-aos-duration="600" data-aos-once="true"/>
                            <div class="leader-content">
                                <div class="leader-name" data-aos="offsetTop" data-aos-duration="600"
                                     data-aos-once="true">
                                    {{ $lead->getName() }}
                                </div>
                                <div class="leader-desc" data-aos="offsetTop" data-aos-duration="600"
                                     data-aos-once="true">{{ $lead->getDescription() }}
                                </div>
                                <div class="leader-tagline" data-aos="offsetTop" data-aos-duration="600"
                                     data-aos-once="true">{!! $lead->getSubscription() !!}</div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</section>