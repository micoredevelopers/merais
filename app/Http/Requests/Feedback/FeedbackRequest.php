<?php

namespace App\Http\Requests\Feedback;

use App\Services\Phone\PhoneUkraineFormatter;

class FeedbackRequest extends AbstractFeedbackRequest
{

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'phone'   => $this->getPhoneRule(),
			'name'    => $this->getRuleNullableChar(),
			'message' => $this->getRuleNullableChar(),
		];
		return $rules;
	}

	protected function mergeRequestValues()
	{
		$phone = PhoneUkraineFormatter::formatPhone((string)$this->get('phone'));
		$this->merge(['phone' => $phone]);
	}
}
