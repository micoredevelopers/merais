<?php

namespace App\View\Components\Layout;

use Illuminate\View\Component;

class Breadcrumbs extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
    	$breadcrumbs = \Breadcrumbs::getBreadCrumbs();
    	$with = compact('breadcrumbs');
        return view('components.layout.breadcrumbs')->with($with);
    }
}
