<?php

	namespace App\Models;


	use App\Contracts\HasLocalized;
	use App\Observers\Admin\MenuObserver;
	use App\Scopes\SortOrderScope;
	use App\Scopes\WhereActiveScope;
	use App\Traits\Models\ImageAttributeTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;
	use Illuminate\Support\Str;

	/**
	 * App\Models\Menu
	 *
	 * @property int                                                              $id
	 * @property int                                                              $menu_group_id
	 * @property int|null                                                         $parent_id
	 * @property int                                                              $active
	 * @property string|null                                                      $url
	 * @property string|null                                                      $icon Шрифтовые изображения
	 * @property string|null                                                      $image обычная картинка, впринципе любого типа
	 * @property int                                                              $sort
	 * @property string|null                                                      $options
	 * @property \Illuminate\Support\Carbon|null                                  $created_at
	 * @property \Illuminate\Support\Carbon|null                                  $updated_at
	 * @property-read \App\Models\MenuGroup                                       $menuGroup
	 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Menu[] $menus
	 * @property-read int|null                                                    $menus_count
	 * @property-read \App\Models\Page                                            $page
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu newModelQuery()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu newQuery()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu parentMenu($parentId = 0)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu query()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereActive($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereCreatedAt($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereIcon($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereId($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereImage($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereMenuGroupId($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereOptions($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereParentId($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereSort($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUpdatedAt($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUrl($value)
	 * @mixin \Eloquent
	 */
	class Menu extends Model implements HasLocalized
	{
		use RedirectLangColumn;

		protected $langColumns = ['name'];

		use ImageAttributeTrait;

		protected $hasOneLangArguments = [MenuLang::class, 'menu_id'];

		public static $publicMenusCacheKey = 'public.menus';

		protected $guarded = ['id'];

		public function menus()
		{
			return $this->hasMany(self::class, 'parent_id', 'id')->with('lang');
		}

		public function scopeParentMenu($query, $parentId = 0)
		{
			return $query->where(function ($query) use ($parentId) {
				return $query->where('parent_id', $parentId)->orWhereNull('parent_id');
			});
		}

		public function allMenus()
		{
			return $this->menus()->with('allMenus')->withGlobalScope('sort', new SortOrderScope());
		}

		public function getName()
		{
			return $this->getAttribute('name');
		}

		public function menuGroup()
		{
			return $this->belongsTo(MenuGroup::class, 'menu_group_id', 'id');
		}

		public function getUrlAttribute()
		{
			return $this->attributes['url'];
		}

		public function getFullUrl()
		{
			$url = $this->getUrlAttribute();
			if ($this->isAnchor() || $this->isExternal()) {
				return $url;
			}
			return langUrl($url);
		}

		public function isExternal()
		{
			return isStringUrl($this->getUrlAttribute());
		}

		public function isAnchor(): bool
		{
			return Str::startsWith($this->getUrlAttribute(), '#');
		}

		public function getRel()
		{
			return $this->isExternal() ? 'rel="nofollow"' : '';
		}

		public function getTarget()
		{
			return $this->isExternal() ? 'target="_blank"' : '';
		}

		public static function getForDisplay($onlyActive = true)
		{
			$query = $onlyActive ? self::active() : self::query();
			$query->with('lang');

			return $query->get();
		}

		public static function getForDisplayEdit()
		{
			return self::getForDisplay(false);
		}

		public static function nestable(array $menus, $parent_id = 0)
		{
			/** @var $findMenu Menu */
			foreach ($menus as $num => $menu) {
				if ($findMenu = static::find(\Arr::get($menu, 'id'))) {
					$data = [
						'sort'      => $num,
						'parent_id' => (int)$parent_id,
					];
					$findMenu->fillExisting($data)->save();
					if (isset($menu['children'])) {
						static::nestable($menu['children'], $menu['id']);
					}
				}
			}
		}

		public static function boot()
		{
			parent::boot();

			if (class_exists(MenuObserver::class)) {
				static::observe(MenuObserver::class);
			}

		}

		public static function initScopesPublic()
		{
			if (!self::hasGlobalScope(new WhereActiveScope())) {
				self::addGlobalScope(new WhereActiveScope());
			}
			if (!self::hasGlobalScope(new SortOrderScope())) {
				self::addGlobalScope(new SortOrderScope());
			}
		}
	}
