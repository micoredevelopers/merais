@includeWhen(false, 'public.layout.app'){{-- fast link to parent template--}}

<main id="main-page">
    @includeIf('public.home.includes.slider')

    @includeIf('public.home.includes.about-section')

    @includeIf('public.home.includes.services')

    @includeIf('public.home.includes.advantages')

</main>

@if($page ?? false)
    {!! editButtonAdmin(urlEntityEdit($page)) !!}
@endif


