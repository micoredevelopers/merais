<?php

	use Illuminate\Support\Facades\Artisan;

	function generateGetSetAttributes(array $array)
	{
		return (new \App\Helpers\Dev\AttributesSettersGettersCreatorFromArray())->generate($array);
	}


	function getTestField($field = null, int $length = 10)
	{
		if (!isLocalEnv()) {
			return '';
		}

		try {
			static $faker;
			if (null === $faker) {
				$faker = Faker\Factory::create();
			}

			switch ($field) {
				case 'phone':
					return '+38(093)' . mt_rand(1000000, 9999999);
					break;
				case 'name':
					return $faker->firstName();
					break;
				case 'email':
					return $faker->email;
					break;
				case 'fio':
					return $faker->firstName() . ' ' . $faker->lastName;
					break;
				case 'message':
				case 'comment':
					return $faker->realText();
					break;
				case 'table_number':
				case 'person':
					return $faker->randomNumber($length);
					break;
			}
		} catch (\Exception $e) {
			app(\App\Helpers\Debug\LoggerHelper::class)->error($e);
		}

		return \Illuminate\Support\Str::random($length);
	}


	function seedByClass(string $class)
	{
		try {
			Artisan::call('db:seed', ['--class' => $class, '--force' => 'true']);
		} catch (\Exception $e) {
			app('log')->error($e->getMessage());
		}
	}


	if (!function_exists('grepClassMethods')) {
		function grepClassMethods($class, $needle = ''): array
		{
			$methods = [];
			if (is_object($class) and $needle) {
				$methods = get_class_methods($class);
				$methods = array_filter($methods, function ($item) use ($needle) {
					$pos = stripos($item, $needle);
					return false !== $pos;
				});
			}
			return $methods;
		}
	}

	function extractDigits($input)
	{
		return preg_replace('/[^0-9]/', '', $input);
	}

	function getUserField(?string $field, ?string $default = null): string
	{
		if (!$field) {
			return '';
		}
		$user = \Illuminate\Support\Facades\Auth::user();
		if (!$user){
			return (string)$default;
		}
		return (string)$user->getAttribute($field);



	}