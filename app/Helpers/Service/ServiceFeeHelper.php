<?php


namespace App\Helpers\Service;


use App\Models\Service\Service;
use App\Traits\ResolveSelf;

class ServiceFeeHelper
{
	use ResolveSelf;

	public function isServiceHasFee(Service $service): bool
	{
		return $service->getFee() > 0;
	}

}