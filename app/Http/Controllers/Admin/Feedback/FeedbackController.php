<?php

	namespace App\Http\Controllers\Admin\Feedback;

	use App\Helpers\Media\ImageRemover;
	use App\Http\Controllers\Admin\AdminController;
	use App\Http\Requests\Admin\FeedbackRequest;
	use App\Models\Feedback\Feedback;
	use App\Repositories\ContentRepository;
	use App\Repositories\FeedbackRepository;
	use App\Traits\Authorizable;
	use App\Traits\Controllers\SaveImageTrait;

	class FeedbackController extends AdminController
	{
		protected $withThumbnails = false;

		use Authorizable;
		use SaveImageTrait;

		private $moduleName;

		protected $key = 'feedback';

		protected $routeKey = 'admin.feedback';

		protected $permissionKey = 'feedback';
		/**
		 * @var FeedbackRepository
		 */
		private $repository;

		public function __construct(FeedbackRepository $repository)
		{
			parent::__construct();
			$this->moduleName = __('modules.feedback.title');
			$this->addBreadCrumb($this->moduleName, $this->resourceRoute('index'));
			$this->shareViewModuleData();
			$this->repository = $repository;
		}

		public function index()
		{
			$this->setTitle($this->moduleName);
			$vars['list'] = $this->repository->getListAdmin();
			$data['content'] = view('admin.feedback.index', $vars);
			return $this->main($data);
		}


		public function destroy(Feedback $feedback)
		{
			if (!$feedback->canDelete()) {
				$this->setFailMessage('Удаление этой записи не доступно');
				return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
			}
			if ($this->repository->delete($feedback->id)) {
				$this->setSuccessDestroy();
				$this->fireEvents();
			}

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		private function fireEvents()
		{
		}
	}
