<?php

	namespace App\Models\Service;


	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Models\Service\Traits\ServiceHelpers;
	use App\Traits\Models\Localization\RedirectLangColumn;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	class ServiceCounter extends Model implements HasLocalized
	{
		use RedirectLangColumn;

		protected $langColumns = ['value'];

		protected $guarded = ['id'];

		public function getCounterKey(): string
		{
			return (string)($this->getAttribute('key'));
		}

		public function getCounterValue(): string
		{
			return (string)($this->getAttribute('value'));
		}

		public function service(): BelongsTo
		{
			return $this->belongsTo(Service::class);
		}

		public function getService(): ?Service
		{
			return $this->service;
		}

		protected $hasOneLangArguments = [ServiceCounterLang::class];
	}
