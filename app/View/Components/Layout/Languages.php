<?php

namespace App\View\Components\Layout;

use Illuminate\Support\Str;
use Illuminate\View\Component;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Tightenco\Collect\Support\Arr;

class Languages extends Component
{
	/**
	 * @var false
	 */
	public $arrows;

	public $languages = [];

	public $activeLanguage = [];

	public function __construct($arrows = false)
	{
		$this->arrows = $arrows;
		$this->loadLanguages();
		$this->loadActiveLanguage();
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\View\View|string
	 */
	public function render()
	{
		return view('components.layout.languages');
	}

	private function loadLanguages():void
	{
		$languages = LaravelLocalization::getSupportedLocales();
		$fn = function (&$arr, $langKey) {
			$active = $langKey === getCurrentLocale();
			$arr['active'] = $active;
			$arr['name'] = Str::upper($langKey);
			$arr['url'] = LaravelLocalization::getLocalizedURL($langKey);
			$arr['key'] = $langKey;
		};
		array_walk($languages, $fn);
		$this->languages = $languages;
	}

	private function loadActiveLanguage():void
	{
		$active = Arr::first(array_filter($this->languages, function(array $lang){
			return $lang['active'];
		}));
		$this->activeLanguage = $active;
	}
}
