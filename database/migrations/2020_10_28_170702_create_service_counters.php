<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceCounters extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'service_counters';

   private $foreignKey = 'service_counter_id';

   private $tableLang = 'service_counter_langs';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }


   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->unsignedBigInteger('service_id');

            $table->id();
            $this->builder
	            ->addBelongsTo('service_id', 'services')
                ->createSort()
                ->createNullableChar('key')
            ;
            $table->timestamps();
        });


        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->id();
            $table->unsignedBigInteger($this->foreignKey);

            $this->builder
                ->createNullableChar('value')
	            ->createLanguageKey()
	            ->addBelongsTo($this->foreignKey, $this->table)
            ;
        });
    }


    public function down()
    {
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
