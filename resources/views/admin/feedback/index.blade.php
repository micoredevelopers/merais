<?php /** @var $item \App\Models\Feedback\Feedback */ ?>
<?php /** @var $permissionKey string */ ?>
@php
    $canEdit = false;
@endphp
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="th-description">ID</th>
            <th>{{ __('validation.attributes.fio') }}</th>
            <th>{{ __('validation.attributes.phone') }}</th>
            <th>{{ __('validation.attributes.email') }}</th>
            <th>Дата создания</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->getAttribute('fio') }}</td>
                <td>{{ $item->getPhoneDisplay() }}</td>
                <td>{{ $item->getAttribute('email') }}</td>
                <td>{{ getDateFormatted($item->getCreatedAt()) }}</td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}