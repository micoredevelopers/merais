<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;

	class TestController extends SiteController
	{
		public function request(Request $request)
		{
			return $request->all();
		}

		public function headers(Request $request)
		{
			return $request->headers->all();
		}

	}
