<?php

	namespace App\Models\Service;

	use App\Models\ModelLang;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	class ServiceLang extends ModelLang
	{
		protected $primaryKey = ['service_id', 'language_id'];

		public function service(): BelongsTo
		{
			return $this->belongsTo(Service::class);
		}
	}
