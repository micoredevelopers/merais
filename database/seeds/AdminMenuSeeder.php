<?php

	use App\Builders\Menu\MenuBuilder;
	use App\Models\Admin\AdminMenu;
	use App\Repositories\Admin\AdminMenuRepository;
	use Illuminate\Database\Seeder;

	class AdminMenuSeeder extends Seeder
	{
		private $adminMenuRepository;

		private $menusByUrl;

		public function __construct(AdminMenuRepository $adminMenuRepository)
		{
			$this->adminMenuRepository = $adminMenuRepository;
			$this->menusByUrl = $this->adminMenuRepository->all()->keyBy('url');
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$menus = [
				[
					'name'      => 'Настройки',
					'url'       => '/settings',
					'gate_rule' => 'view_settings',
					'icon_font' => '<i class="material-icons">settings</i>',
				],
				[
					'name'      => 'Локализация',
					'url'       => '/translate',
					'gate_rule' => 'view_translate',
					'icon_font' => '<i class="fa fa-language" aria-hidden="true"></i>',
				], [
					'active'    => 0,
					'name'      => 'SEO',
					'url'       => '/meta',
					'gate_rule' => 'view_meta',
					'icon_font' => '<i class="fa fa-google-plus" aria-hidden="true"></i>',
					'childrens' => [
						[
							'name'      => 'Redirects',
							'url'       => '/redirects',
							'gate_rule' => 'view_redirects',
							'icon_font' => '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
						],
						[
							'name'      => 'Sitemap',
							'url'       => '/sitemap',
//						'gate_rule' => 'view_sitemap',
							'icon_font' => '<i class="fa fa-sitemap" aria-hidden="true"></i>',
						],
						[
							'name'      => 'Robots.txt',
							'url'       => '/robots',
							'gate_rule' => 'view_robots',
							'icon_font' => '<i class="fa fa-android" aria-hidden="true"></i>',
						],
					],
				],
				[
					'name'      => 'Пользователи',
					'url'       => '/users',
					'gate_rule' => 'view_users',
					'icon_font' => '<i class="fa fa-user" aria-hidden="true"></i>',
				],
				[
					'active'    => 0,
					'name'      => 'Роли',
					'url'       => '/roles',
					'gate_rule' => 'view_roles',
					'icon_font' => '<i class="fa fa-users" aria-hidden="true"></i>',
				],
				[
					'active'    => 0,
					'name'      => 'Галерея',
					'url'       => '/galleries',
					'gate_rule' => 'view_galleries',
					'icon_font' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
				],
				[
					'name'      => 'Admin Menu',
					'url'       => '/admin-menus',
					'gate_rule' => 'view_admin-menus',
					'icon_font' => '<i class="fa fa-bars" aria-hidden="true"></i>',
				],
				[
					'name'      => 'Меню',
					'url'       => '/menu',
					'gate_rule' => 'view_menu',
					'icon_font' => '<i class="fa fa-bars" aria-hidden="true"></i>',
				],
				[
					'name'      => 'Logs',
					'url'       => '/logs',
					'sort'      => '20',
					'gate_rule' => 'view_logs',
					'icon_font' => '<i class="fa fa-history" aria-hidden="true"></i>',
				],
				$this->getBuilder()->setName('Страницы')->setUrl('/pages')->setGateRule('view_pages')->build(),
				$this->getBuilder()->setName('Слайдеры')->setUrl('/sliders')->setGateRule('view_sliders')->build(),
				$this->getBuilder()->setName(__('modules.service.title'))->setUrl('/services')->setGateRule('view_services')->build(),
				$this->getBuilder()->setName('Счетчики услуг')->setUrl('/service-counters')->setGateRule('view_service-counters')->build(),
				$this->getBuilder()->setName(__('modules.leads.title'))->setUrl('/leads')->setGateRule('view_leads')->build(),
				$this->getBuilder()->setName(__('modules.advantages.title'))->setUrl('/advantages')->setGateRule('view_advantages')->build(),
				$this->getBuilder()->setName(__('modules.feedback.title_plural'))->setIconFont('<i class="fa fa-commenting" aria-hidden="true"></i>')->setUrl('/feedback')->setGateRule('view_feedback')->build(),
			];
			$this->loop($menus);

			Artisan::call('cache:clear');
		}

		private function loop(array $menus, ?AdminMenu $parentMenu = null)
		{
			foreach ($menus as $menu) {
				$menuModel = $this->createMenu($menu, $parentMenu);
				if ($menuModel && Arr::get($menu, 'childrens')) {
					$this->loop(\Arr::wrap(Arr::get($menu, 'childrens')), $menuModel);
				}
			}
		}

		private function createMenu(array $menu, ?AdminMenu $parentMenu = null): ?AdminMenu
		{
			if ($this->isMenuExistsByUrl($menu['url'] ?? '')) {
				return $this->getMenuExistsByUrl($menu['url'] ?? '');
			}
			$menu = array_merge(['active' => 1], $menu);
			if ($parentMenu) {
				$menu['parent_id'] = $parentMenu->getKey();
			}

			return tap($this->adminMenuRepository->create($menu), function ($adminMenu) {
				$this->onCreated($adminMenu);
			});
		}

		private function onCreated(AdminMenu $adminMenu)
		{

		}

		private function isMenuExistsByUrl(?string $url)
		{
			return $this->menusByUrl->offsetExists($url);
		}

		private function getMenuExistsByUrl(?string $url)
		{
			return $this->menusByUrl->get($url);
		}

		private function getBuilder(): MenuBuilder
		{
			return app(MenuBuilder::class);
		}
	}
