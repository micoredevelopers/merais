<?php

namespace App\Mail\Feedback;

use App\Mail\MailAbstract;
use App\Models\Feedback\Feedback;

class FeedbackDefault extends MailAbstract
{
	protected $feedback;

	public function __construct(Feedback $feedback)
	{
		parent::__construct();
		$this->feedback = $feedback;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$this->subject('Заявка на обратную связь');
		return $this->from($this->getEmailFrom(), $this->getNameFrom())
			->to($this->getEmailTo())
			->view('mail.feedback.feedback-default')
            ->with(['feedback' => $this->feedback]);
	}
}
