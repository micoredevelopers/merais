<?php /** @var $services \Illuminate\Support\Collection | \App\Models\Service\Service[] */ ?>
<main id="services-page">
    <x-layout.breadcrumbs />
    @if($services)
        <section class="services-section">
            <div class="container">
                <div class="services-section_title aos-init aos-animate" data-aos="offsetTop" data-aos-duration="1200" data-aos-once="true"
                     data-aos-delay="200">
                    {{ getTranslate('services.title', 'Услуги') }}
                </div>
                <div class="services-wrap row animated">
                    @foreach($services as $service)
                        @php
                            $delay = 200 + (200 * $loop->iteration)
                        @endphp
                        <div class="service col-12 col-lg-4 aos-init aos-animate" data-aos="offsetTop" data-aos-duration="600"
                             data-aos-once="true" data-aos-delay="{{ $delay }}">
                            <div class="service-number">{{ $loop->iteration < 10 ? 0 : '' }}{{ $loop->iteration }}</div>
                                <img  class="svg service-icon" src="{{ storageFileExists($service->getIcon()) ? getPathToImage($service->getIcon()) : asset('images/icons/service.svg') }}" alt="">
                            <div class="service-content">
                                <div class="service-title">{{ $service->getName() }}</div>
                                <div class="service-description">{!! $service->getExcerpt() !!}</div>
                            </div>
                            <a class="service_link"
                               href="{{ route('service.show', $service->getUrl()) }}">
                                <span>{{ getTranslate('global.view-details', 'Подробнее') }}</span>
                                <img class="svg" src="{{ asset('images/icons/arrow.svg') }}" alt="">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
</main>