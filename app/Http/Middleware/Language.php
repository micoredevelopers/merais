<?php

namespace App\Http\Middleware;

use App\Helpers\LanguageHelper;
use Closure;

class Language
{
	/**
	 * @param $request
	 * @param Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$languageList = \App\Models\Language::active(1)->get();
		if ($locale = getCurrentLocale()) {
			$language = $languageList->where('key', $locale)->first();
			if ($language) {
				LanguageHelper::setLanguageId((int)$language->id);
			}
		}
		if (!getLang()) {
			$language = $languageList->where('default', 1)->first();
			if (!$language) {
				return abort(404);
			}
		} else {
			$language = $languageList->where('id', getLang())->first();
		}
		LanguageHelper::setLanguageId((int)$language->id);

		return $next($request);
	}
}
