<?php

namespace App\Listeners\Admin\Image;

use App\Config\Media\ThumbnailImagesConfig;
use App\Events\Admin\Image\ImageUploaded;
use App\Helpers\Debug\LoggerHelper;
use App\Helpers\Media\ImageSaver;
use App\Helpers\Media\ImageWatermark;
use App\Models\Model;
use Intervention\Image\ImageManagerStatic;

class ImageUploadedAddThumbnailsListener
{
	/**
	 * @var ImageSaver
	 */
	private $imageSaver;

	public function __construct(ImageSaver $imageSaver)
	{
		$this->imageSaver = $imageSaver;
	}

	public function handle($event): void
	{
		/** @var $event ImageUploaded */
		$model = $event->getModel();
		$target = imgPathOriginal($model->getAttribute('image'));

		if (!storageFileExists($target)) {
			return;
		}
		$sizes = array_filter(ThumbnailImagesConfig::getSizesByKey($model->getTable()), 'is_numeric');
		if (!(($sizes) && (count($sizes) === 2))) {
			return;
		}
		$this->imageSaver->setThumbnailSizes(...$sizes);
		try {
			$image = ImageManagerStatic::make(\Storage::path($target));
			$this->imageSaver
				->setWithThumbnail($this->withThumbnails($model))
				->setFullImageName(imgPathOriginal($model->getAttribute('image')))
				->saveFromImage($image)
			;
		} catch (\Exception $e) {
			app(LoggerHelper::class)->error($e);
		}
	}

	private function supportsWatermarking(Model $model): bool
	{
		return true;
	}

	private function withThumbnails(Model $model): bool
	{
		return false;
	}
}
