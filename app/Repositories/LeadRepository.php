<?php

	namespace App\Repositories;

	use App\Criteria\ActiveCriteria;
	use App\Criteria\SortCriteria;
	use App\Models\Lead\Lead;
	use App\Models\Lead\LeadLang;


	class LeadRepository extends AbstractRepository
	{

		public function model()
		{
			return Lead::class;
		}

		public function modelLang()
		{
			return LeadLang::class;
		}

		public function addPublicCriteriaToQuery(): self
		{
			$this->pushCriteria($this->app->make(SortCriteria::class));
			$this->pushCriteria($this->app->make(ActiveCriteria::class));
			return $this;
		}

		public function addAdminCriteriaToQuery()
		{
			$this->pushCriteria($this->app->make(SortCriteria::class));
		}

		public function getListAdmin()
		{
			$this->addAdminCriteriaToQuery();
			return $this->with('lang')->paginate();
		}

		public function getListPublic()
		{
			$this->addPublicCriteriaToQuery();
			return $this->with('lang')->get();
		}

	}

