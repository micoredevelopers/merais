<?php

namespace App\Http\Controllers;

use App\Models\Service\Service;
use App\Repositories\ServiceRepository;

class ServiceController extends SiteController
{
	/**
	 * @var ServiceRepository
	 */
	private $serviceRepository;

	public function __construct(ServiceRepository $serviceRepository)
	{
		parent::__construct();
		$this->serviceRepository = $serviceRepository;
		$this->addBreadCrumb(getTranslate('services.title', 'Услуги'), route('service.index'));
	}

	public function index()
	{
		$services = $this->serviceRepository->getListPublic();
		$with = compact('services');
		$this->setTitle(getTranslate('services.title', 'Услуги'));
		$data['content'] = view('public.service.services-index')->with($with);
		return $this->main($data);

	}

	public function show($url)
	{
		/** @var  $service Service*/
		$service = $this->serviceRepository->findByUrl($url);
		if(!$service){
		    return view("errors.404");
        }
		$service->load('lang', 'counters.lang');
		$otherServices = $this->serviceRepository->getWithout($service);
		$with = compact('service', 'otherServices');
		$this->addBreadCrumb($service->getName())
		->setTitle($service->getName())
	;
		$data['content'] = view('public.service.service-show')->with($with);
		return $this->main($data);
	}
}
