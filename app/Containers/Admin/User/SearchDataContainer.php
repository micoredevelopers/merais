<?php


	namespace App\Containers\Admin\User;


	class SearchDataContainer
	{
		private $search = '';

		private $isSuperAdmin = false;

		private $withAdmins = false;

		/**
		 * @return string
		 */
		public function getSearch(): string
		{
			return $this->search;
		}

		/**
		 * @param string $search
		 */
		public function setSearch(string $search): void
		{
			$this->search = $search;
		}

		/**
		 * @return bool
		 */
		public function isSuperAdmin(): bool
		{
			return $this->isSuperAdmin;
		}

		/**
		 * @param bool $isSuperAdmin
		 */
		public function setIsSuperAdmin(bool $isSuperAdmin): void
		{
			$this->isSuperAdmin = $isSuperAdmin;
		}

		/**
		 * @return bool
		 */
		public function isWithAdmins(): bool
		{
			return $this->withAdmins;
		}

		/**
		 * @param bool $withAdmins
		 */
		public function setWithAdmins(bool $withAdmins): void
		{
			$this->withAdmins = $withAdmins;
		}

	}