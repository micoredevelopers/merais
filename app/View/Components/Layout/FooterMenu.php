<?php

	namespace App\View\Components\Layout;

	use App\Repositories\MenuRepository;
	use Illuminate\View\Component;

	class FooterMenu extends Component
	{
		/**
		 * Create a new component instance.
		 *
		 * @return void
		 */
		public function __construct(MenuRepository $menuRepository)
		{
			$this->menuRepository = $menuRepository;
		}

		/**
		 * Get the view / contents that represent the component.
		 *
		 * @return \Illuminate\View\View|string
		 */
		public function render()
		{
			$menus = $this->menuRepository->getMenus(getLang());
			$footerMenu = $menus['footer_menu'] ?? collect([]);
			return view('components.layout.footer-menu')->with(compact('footerMenu'));
		}
	}
