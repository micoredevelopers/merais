
<ul class="nav nav-tabs mb-5" role="tablist">
    <li role="presentation"><a class="active" href="#sliders-dataDefault" aria-controls="home" role="tab"
                               data-toggle="tab">@lang('modules._.tabs.main')</a>
    </li>
    <li role="presentation">
        <a href="#sliders-photosList" aria-controls="profile" role="tab"
           data-toggle="tab">Слайды ({{ $edit->items->count() }})</a></li>
</ul>
<section class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="sliders-dataDefault">
        <div class="panel-body">
            @include('admin.sliders.partials.form')
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="sliders-photosList">
        <div class="panel-body">
            @include('admin.sliders.partials.slides', ['slides' => $edit->items ])
        </div>
    </div>
</section>