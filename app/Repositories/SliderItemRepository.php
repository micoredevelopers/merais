<?php

	namespace App\Repositories;

	use App\Models\Model;
	use App\Models\Slider\SliderItem;
	use App\Models\Slider\SliderItemLang;

	/**
	 * Class MetaRepository.
	 */
	class SliderItemRepository extends AbstractRepository
	{
		/**
		 * @return string
		 *  Return the model
		 */
		public function model()
		{
			return SliderItem::class;
		}

		public function modelLang()
		{
			return SliderItemLang::class;
		}

		public function createRelated(array $attributes, Model $model): SliderItem
		{
			$attributes[ $model->getForeignKey() ] = $model->getKey();
			return parent::create($attributes);
		}

	}
