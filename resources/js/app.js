import AOS from 'aos'
require('./plugins.js')
require('./menu')
require('./header')
require('./sliders')
require ('./ajax')



const header = $('header')
const menu = $('.aside-menu')
const languageMenu = $('.language-menu');
const headerLogo = $('.header-logo')
const headerLogo2 = $('.header-logo2')
const services = $('.service');

AOS.init({
    offset: 0,
    once: true,
    initClassName: 'aos-init',
    animatedClassName: 'aos-animate'
});

//$(".phone").mask("+38(999) 999-9999");



$('body').on('click', function (e) {
    const target = $(e.target);

    if(menu.hasClass('active')) {
        if( !target.parents('.aside-menu').hasClass('aside-menu') && !target.hasClass('.menu-nav_menu-toggle')) {
            menu.removeClass('active');
            $('.backdrop').removeClass('active')
            $('html, body').removeClass('active')
        }
    }

    if (languageMenu.hasClass('active')) {
        if(!target.parents('.language-toggler').hasClass('language-toggler')) {
            languageMenu.removeClass('active');
        }
    }

})

function checkServices() {
    if($(services[services.length-1]).hasClass('aos-animate')) {
        $('.services-wrap').addClass('animated');
    }
}

setTimeout(checkServices, 500)

window.addEventListener('scroll', function () {
    const trigger = 30;
    let top = Math.trunc(this.pageYOffset)
    const head = document.querySelector('header');
    if (top > trigger) {
        head.classList.add('active')
        AOS.refresh()
    } else {
        head.classList.remove('active')
    }

    checkServices()
})

$('.slick-dots button').text('')

const id = $('main').attr('id');

switch (id) {
    case 'services-page':
        headerLogo.remove();
        headerLogo2.addClass('active');
        break;
    case 'main-page':
        header.removeClass('other-page')
        break;
    case 'about-page':
        headerLogo.remove();
        headerLogo2.addClass('active');
        break
    case 'main-contacts':
        headerLogo.remove();
        headerLogo2.addClass('active');
        break;
    case 'main-service':
        headerLogo.remove();
        headerLogo2.addClass('active');
        break;
}