


function toggleMenu() {
    $('.menu-nav_menu-toggle').on('click', function () {
        $('.aside-menu, .backdrop, html, body').toggleClass('active');
    })
}

toggleMenu();



function hoistingImage() {
    const links = $('.menu_item');

    links.hover(function () {
        const item = $(this);

        (function  () {
            $('.aside-menu').mousemove(function(e){

                let X = 0;
                let Y = 0;

                if(e.target.closest('.aside-menu')){
                    const target = e.target.closest('.aside-menu'); // Здесь что-то уникальное, что может указать на род. блок

                    const targetCoords = target.getBoundingClientRect();
                     X = e.clientX - targetCoords.left;
                     Y = e.clientY - targetCoords.top;
                }

                item.find('.menu-hoisting_photo').css({'left': (X+10) + 'px','top': (Y+10) + 'px'})
            });

            $('.menu-items').on('mouseleave', ()=> {
                $('.aside-menu').off('mousemove');
            })
        }())

    })

}

hoistingImage()

setTimeout(function () {
    $('.aside-menu').removeAttr('data-aos')
},1250)

