<?php

namespace App\Traits\Models;


trait HasGetterDescriptionTrait
{

    /**
     * @param string $column
     * @return mixed|string
     */
    public function getDescription()
    {
    	$column = 'description';
        return $this->getAttribute($column);
    }

}