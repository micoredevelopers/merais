@include('admin.partials.crud.elements.name')

<div class="w-50">
    @include('admin.partials.crud.elements.image-upload-group')
</div>

@include('admin.partials.crud.elements.active')

@include('admin.partials.crud.textarea.description')


