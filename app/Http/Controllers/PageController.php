<?php

	namespace App\Http\Controllers;

	use App\Repositories\ServiceRepository;
	use App\Services\SMS\Sender;
	use Illuminate\Filesystem\Filesystem;
	use Illuminate\Http\Request;

	class PageController extends SiteController
	{
		public function default($page)
		{
			$view = 'public.pages.' . $page;

			$data = [];
			if (view()->exists($view)){
				$data['content'] = view($view);
			}

			return $this->main($data);
		}
	}
