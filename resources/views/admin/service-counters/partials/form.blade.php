<?php /** @var $edit \App\Models\Service\ServiceCounter */ ?>
<?php /** @var $service \App\Models\Service\Service */ ?>
@include('admin.partials.crud.default', ['name' => 'key', 'title' => 'Ключ'])

@include('admin.partials.crud.default', ['name' => 'value', 'title' => 'Значение'])


<div class="form-group w-25">
    @includeIf('admin.service-counters.partials.services-select', ['serviceId' => ($edit ?? false) ? $edit->getService()->getKey() : null])
</div>