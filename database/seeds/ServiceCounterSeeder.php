<?php

	use App\Repositories\ServiceCounterRepository;
	use App\Repositories\ServiceRepository;
	use Illuminate\Database\Seeder;

	class ServiceCounterSeeder extends Seeder
	{

		/**
		 * @var \Faker\Generator
		 */
		private $factory;
		/**
		 * @var ServiceCounterRepository
		 */
		private $repository;

		private $services;

		private $items = [];

		public function __construct(\Faker\Generator $factory, ServiceRepository $serviceRepository, ServiceCounterRepository $repository)
		{
			$this->factory = $factory;
			$this->repository = $repository;
			$this->services = $serviceRepository->all();
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			foreach ($this->getServices() as $service) {
				$attributes = ['key' => $this->factory->randomNumber(2), 'value' => $this->factory->text(20)];
				foreach (range(1, 3)as $item){
					$this->repository->createRelated($attributes, $service);
				}
			}
		}


		/**
		 * @return \Illuminate\Support\Collection| \App\Models\Service\Service[]
		 */
		public function getServices()
		{
			return $this->services;
		}
	}
