<?php


	namespace App\Models\Slider;


	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Observers\Slider\SliderItemObserver;

	/**
	 * App\Models\Slider\SliderItem
	 *
	 * @property int                            $id
	 * @property int                            $slider_id
	 * @property bool                           $active
	 * @property int|null                       $sort
	 * @property string|null                    $link
	 * @property string                         $type
	 * @property string|null                    $src
	 * @property-read \App\Models\Slider\Slider $slider
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem newModelQuery()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem newQuery()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem query()
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereActive($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereId($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereLink($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereSliderId($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereSort($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereSrc($value)
	 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereType($value)
	 * @mixin \Eloquent
	 */
	class SliderItem extends Model implements HasLocalized
	{

		const TYPE_IMAGE = 'image';

		protected $hasOneLangArguments = [SliderItemLang::class];

		protected $guarded = [
			'id',
		];
		protected $casts = [
			'active' => 'boolean',
		];

		public $timestamps = false;

		public function slider()
		{
			return $this->belongsTo(Slider::class);
		}

		public function isTypeImage()
		{
			return $this->type === self::TYPE_IMAGE;
		}

		public function getName(): string
		{
			return (string)($this->lang->name ?? '');
		}

		public function getDescription(): string
		{
			return (string)($this->lang->description ?? '');
		}

		public function getLink(): string
		{
			return (string)($this->link ?? '');
		}

		public function getImagePath()
		{
			return $this->src;
		}

		public static function boot()
		{
			parent::boot();

			if (class_exists(SliderItemObserver::class)) {
				SliderItem::observe(SliderItemObserver::class);
			}
		}
	}