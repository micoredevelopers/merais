<?php /** @var $item \App\Models\Doctor\Doctor */ ?>
<?php /** @var $permissionKey string */ ?>
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>{{ __('form.sorting') }}</th>
            <th class="">{{ __('form.image.image') }}</th>
            <th class="th-description">Имя</th>
            <th class="th-description">Должность </th>
            <th>Отображение</th>
            <th class="text-right">
                @can('add_' . $permissionKey)
                <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">{{ __('form.create') }}</a>
                @endcan
            </th>
        </tr>
        </thead>
        <tbody data-sortable-container="true" data-table="{{ $table ??  $list->isNotEmpty() ? $list->first()->getTable(): '' }}">
        @foreach($list as $item)
            @php
                $canDelete = Gate::allows('delete_'. $permissionKey ?? '');
            	if (!$item->canDelete()){
                	$canDelete = false;
                }
            @endphp
            <tr class="draggable" data-id="{{ $item->id }}" data-sort="{{ $item->sort }}">
                <td>
                    @include('admin.partials.sort_handle')
                </td>
                <td>
                    <div class="img-container">
                        <a href="{{ imgPathOriginal(getPathToImage($item->image)) }}" class="fancy d-inline-block" data-fancybox="list-image">
                            <img alt="" class="img-fluid" src="{{ getPathToImage($item->image) }}" style="width:50px"/>
                        </a>
                    </div>
                </td>
                <td>
                    <a href="{{ route($routeKey.'.edit', $item->id) }}">{{ $item->name }}</a>
                </td>
                <td>
                    {{ $item->position }}
                </td>
                <td>
                    {{ translateYesNo((int)$item->getAttribute('active')) }}
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}