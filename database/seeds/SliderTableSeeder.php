<?php

	use App\Models\Slider\SliderItem;
	use App\Repositories\CategoryRepository;
	use App\Repositories\SliderItemRepository;
	use App\Repositories\SliderRepository;
	use Illuminate\Database\Seeder;

	class SliderTableSeeder extends Seeder
	{
		/** @var SliderRepository */
		private $sliderRepository;
		/**
		 * @var SliderItemRepository
		 */
		private $sliderItemRepository;

		public function __construct(SliderRepository $sliderRepository, SliderItemRepository $sliderItemRepository)
		{
			$this->sliderRepository = $sliderRepository;
			$this->sliderItemRepository = $sliderItemRepository;
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$request = new \Illuminate\Http\Request();
			$sliderItem = app(SliderItem::class);
			$slider = new \App\Models\Slider\Slider();
			$request->merge([
				inputNamesManager($slider)->getNameInputRequest()     => [
					'key' => 'main-page', 'comment' => 'Слайдер на главной странице', 'active' => 1,
				],
				inputNamesManager($sliderItem)->getNameInputRequest() => [
					'src' => url('/images/slide1.jpg'),
				],
			]);
			$this->sliderRepository->save($request, $slider);
			$this->sliderItemRepository->createRelated([
				'name'        => 'Выгодное транспортное решение для бизнеса',
				'description' => 'Контейнерные перевозки, таможенное оформление, наземные перевозки (ЖД/авто), негабариты, консалтинговые услуги, доставка малых партий',
				'src'         => url('/images/slide2.jpeg'),
			], $slider);
			$this->sliderItemRepository->createRelated([
				'src'  => url('/images/slide3.jpg'),
				'name' => 'Выгодное транспортное решение для бизнеса', 'description' => 'Контейнерные перевозки, таможенное оформление, наземные перевозки (ЖД/авто), негабариты, консалтинговые услуги, доставка малых партий'], $slider);
//
			$slider = new \App\Models\Slider\Slider();
			$request->merge([
				inputNamesManager($slider)->getNameInputRequest()     => [
					'key' => 'main-page-about', 'comment' => 'Слайдер на главной странице в описании (описание и название не выводится на странице)', 'active' => 1,
				],
				inputNamesManager($sliderItem)->getNameInputRequest() => [
					'src' => url('/images/slide1.jpg'),
				],
			]);
			$this->sliderRepository->save($request, $slider);
			$this->sliderItemRepository->createRelated([
				'name'        => '',
				'description' => '',
				'src'         => url('/images/slide2.jpeg'),
			], $slider);
			$this->sliderItemRepository->createRelated([
				'name'        => '',
				'description' => '',
				'src'         => url('/images/slide3.jpg'),
			], $slider);
//
			$slider = new \App\Models\Slider\Slider();
			$request->merge([
				inputNamesManager($slider)->getNameInputRequest()     => [
					'key' => 'about', 'comment' => 'Слайдер на странице о компании (описание и название не выводится на странице)', 'active' => 1,
				],
				inputNamesManager($sliderItem)->getNameInputRequest() => [
					'src' => url('/images/slide1.jpg'),
				],
			]);
			$this->sliderRepository->save($request, $slider);
			$this->sliderItemRepository->createRelated([
				'name'        => '',
				'description' => '',
				'src'         => url('/images/slide2.jpeg'),
			], $slider);
			$this->sliderItemRepository->createRelated([
				'name'        => '',
				'description' => '',
				'src'         => url('/images/slide3.jpg'),
			], $slider);
		}
	}
