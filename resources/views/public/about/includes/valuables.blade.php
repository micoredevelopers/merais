<?php
/** @var $leads \Tightenco\Collect\Support\Collection | \App\Models\Lead\Lead[] */
$valuesText = getTranslate('about.leads.values', ' ');

?>


<section class="valuables-section">
    <div class="container">
        <div class="valuables-section_inner">
            <div class="valuables-section_info">
                <div class="valuables-section_title" data-aos="clipTop" data-aos-duration="600" data-aos-once="true">{{ getTranslate('about.leads.header', 'Цінності') }}</div>
                <div class="valuables-section_content">
                    <div class="valuables-section_name" data-aos="offsetTop" data-aos-duration="600" data-aos-delay="200" data-aos-once="true">{{ getTranslate('about.leads.description', 'Керівників Merais') }}</div>
                    <div class="valuables-section_desc-wrap">

                        @foreach(explodeByEol($valuesText) as $text)
                            @php
                                $delay = 400 + (200 * $loop->iteration);
                            @endphp
                            <div class="valuables-section_desc" data-aos="offsetTop" data-aos-duration="600"
                                 data-aos-delay="{{ $delay }}"
                                 data-aos-once="true">{{ $text }}</div>
                        @endforeach

                    </div>
                </div>
            </div>

            <div class="valuables-section_preview">
                <img class="valuables-section_img" src="{{ getPathToImage(imgPathOriginal( getSetting('global.leads.block.image') )) }}" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true"/>
            </div>
        </div>
    </div>
</section>