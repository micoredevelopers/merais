<?php

namespace App\Http\Controllers;

use App\Events\Feedback\FeedbackCreatedEvent;
use App\Helpers\ResponseHelper;
use \App\Http\Requests\Feedback\FeedbackRequest;
use App\Repositories\FeedbackRepository;
use Illuminate\Http\Request;

class FeedbackController extends SiteController
{
    private $isSuccess = false;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var FeedbackRepository
     */
    private $repository;

    public function __construct(FeedbackRepository $repository, Request $request)
    {
        parent::__construct();
        $this->request = $request;
        $this->repository = $repository;
    }

    public function feedback(FeedbackRequest $request)
    {
        $data = $request->only([
            'fio', 'name', 'phone', 'email', 'message',
        ]);

        $res = $this->saveFeedbackWithSendMail($data);

        return $this->response($res);
    }

    private function saveFeedbackWithSendMail(array $data): array
    {
        try {
            $ip = $this->request->getClientIp();
            $res = $this->getSuccess();
            $data['referer'] = $data['referer'] ?? url()->previous();
            $data['ip'] = $data['ip'] ?? $ip;
            $feedback = $this->repository->create($data);
            if ($feedback) {
                event(new FeedbackCreatedEvent($feedback));
                $this->isSuccess = true;
            } else {
                $res = $this->getFail();
            }
        } catch (\Exception $e) {
            logger()->error($e);
            $res = $this->getFail();
            $this->isSuccess = false;
        }
        return $res;
    }

    private function response(array $res)
    {
        if ($this->request->expectsJson()) {
            return $res;
        }

        return redirect()->back()->with($res)->withInput(isLocalEnv() ? $this->request->input() : []);
    }

    /**
     * @return array
     */
    private function getSuccess(): array
    {
        $status = ResponseHelper::SUCCESS_KEY;
        $message = getTranslate('feedback.send-success');
        return [$status => $message];
    }

    /**
     * @param string $message
     * @return array
     */
    private function getFail($message = 'feedback.send-failed'): array
    {
        $status = ResponseHelper::ERROR_KEY;
        $message = getTranslate($message);
        return [$status => $message];
    }

}
