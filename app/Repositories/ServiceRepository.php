<?php

	namespace App\Repositories;

	use App\Criteria\ActiveCriteria;
	use App\Criteria\SortCriteria;
	use App\Models\Content\Content;
	use App\Models\Content\ContentLang;
	use App\Models\Language;
	use App\Models\Model;
	use App\Models\Service\Service;
	use App\Models\Service\ServiceLang;
	use App\Repositories\Admin\LanguageRepository;
	use App\Validators\ServiceValidator;
	use Illuminate\Support\Arr;
	use Illuminate\Support\Collection;


	class ServiceRepository extends AbstractRepository
	{

		/**
		 * Specify Model class name
		 *
		 * @return string
		 */
		public function model()
		{
			return Service::class;
		}

		public function modelLang()
		{
			return ServiceLang::class;
		}

		public function find($id, $columns = ['*']): ?Service
		{
			return parent::find($id, $columns);
		}

		public function addPublicCriteriaToQuery(): self
		{
			$this->pushCriteria($this->app->make(SortCriteria::class));
			$this->pushCriteria($this->app->make(ActiveCriteria::class));
			return $this;
		}

		public function addAdminCriteriaToQuery()
		{
			$this->pushCriteria($this->app->make(SortCriteria::class));
		}

		public function getListAdmin()
		{
			$this->addAdminCriteriaToQuery();
			return $this->with('lang')->paginate();
		}

		public function getListSelect()
		{
			$this->addAdminCriteriaToQuery();
			return $this->with('lang')->get();
		}

		public function getListPublic()
		{
			$this->addPublicCriteriaToQuery();
			return $this->with('lang')->get();
		}

		public function getWithout(Service $service):Collection
		{
			$this->addPublicCriteriaToQuery();
			return $this->where('id', '!=', $service->getKey())->with('lang')->get();
		}

	}

