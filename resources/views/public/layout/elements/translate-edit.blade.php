@if(isAdmin())
    @php
        $title = $title ?? 'Редактировать';
        $key = $key ?? '';
    @endphp
    <a class="badge badge-info" style="border-radius: 0;" href="/admin/translate?search={{ $key }}"
       target="_blank">{{ $title }}</a>
@endif