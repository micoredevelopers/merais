<?php

	namespace App\Http\Controllers\Admin;

	use App\Helpers\Media\ImageRemover;
	use App\Http\Requests\Admin\GalleryRequest;
	use App\Models\Gallery\Gallery;
	use App\Repositories\GalleryRepository;
	use App\Traits\Authorizable;

	class GalleryController extends AdminController
	{
		use Authorizable;

		private $name;

		protected $key = 'galleries';

		protected $routeKey = 'admin.galleries';

		protected $permissionKey = 'galleries';
		/**
		 * @var GalleryRepository
		 */
		private $repository;

		public function __construct(GalleryRepository $repository)
		{
			parent::__construct();
			$this->name = __('modules.galleries.title');
			$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
			$this->shareViewModuleData();
			$this->repository = $repository;
		}

		public function index()
		{
			$this->setTitle($this->name);
			$vars['list'] = $this->repository->getListAdmin();
			$data['content'] = view('admin.galleries.index', $vars);
			return $this->main($data);
		}

		public function create()
		{
			$data['content'] = view('admin.galleries.create');
			return $this->main($data);
		}

		public function store(GalleryRequest $request)
		{
			$input = $request->except('image');
			if ($gallery = $this->repository->create($input)) {
				$this->setSuccessStore();
				$this->fireEvents();
			}
			return $this->redirectOnCreated($gallery);
		}

		public function edit(Gallery $gallery)
		{
			$edit = $gallery;
			$photosList = $gallery->images;
			$title = $this->titleEdit($edit);
			$this->addBreadCrumb($title)->setTitle($title);

			$data['content'] = view('admin.galleries.edit', compact(
				'edit'
				,'photosList'
			));
			return $this->main($data);
		}

		public function update(GalleryRequest $request, Gallery $gallery)
		{
			$input = $request->except('image');

			if ($this->repository->update($input, $gallery)) {
				$this->saveAdditionalImages($gallery, $request);
				$this->setSuccessUpdate();
				$this->fireEvents();
			}

			return $this->redirectOnUpdated($gallery);
		}

		public function destroy(Gallery $gallery, ImageRemover $imageRemover)
		{
			if ($this->repository->delete($gallery->id)) {
				$imageRemover->removeImage($gallery->image);
				$this->setSuccessDestroy();
				$this->fireEvents();
			}

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		private function fireEvents()
		{
		}
	}
