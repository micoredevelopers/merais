<?php

	if (!function_exists('getDateCarbon')) {
		function getDateCarbon($date, $format = 'Y-m-d H:i:s'): Carbon\Carbon
		{
			if (isDateValid($date)) {
				return \Carbon\Carbon::parse($date);
			}
			return \Carbon\Carbon::parse(now()->format($format));
		}
	}

	function getDateFormatted($date)
	{
		$date = getDateCarbon($date);
		return $date->formatLocalized('%d %B %Y, в %R');
	}

	if (!function_exists('isDateValid')) {
		function isDateValid($date): bool
		{
			try {
				\Carbon\Carbon::parse($date);
			} catch (\Exception $e) {
				return false;
			}
			return true;
		}
	}


	if (!function_exists('dateFrom')) {
		function dateFrom($date): \Carbon\Carbon
		{
			$date = getDateCarbon($date);
			return \Carbon\Carbon::parse($date->format('Y-m-d H:i:s'));
		}
	}
	if (!function_exists('dateTo')) {
		function dateTo($date): \Carbon\Carbon
		{
			$date = getDateCarbon($date);
			return \Carbon\Carbon::parse($date->format('Y-m-d 23:59:59'));
		}

	}


	/**
	 * @param \Carbon\Carbon $dateFrom
	 * @param \Carbon\Carbon $dateTo
	 * @param string         $format
	 * @return array
	 */
	if (!function_exists('generateDaysByDateRange')) {
		function generateDaysByDateRange(Carbon\Carbon $dateFrom, Carbon\Carbon $dateTo, $format = 'Y-m-d'): array
		{
			$days = [$dateFrom->format($format)];
			$dateFromCopy = clone $dateFrom;
			while ($dateFromCopy <= $dateTo) {
				$dateFromCopy = $dateFromCopy->addDay();
				$pickupString = $dateFromCopy;
				$days[] = $pickupString->format($format);
			}
			return $days;
		}
	}

	function isNightModeEnabled()
	{
		$nowH = (int)now()->format('H');
		if ($nowH >= 21) {
			return true;
		}
		return ($nowH < 6);
	}

	function getNightMode()
	{
		return (isSuperAdmin() && isNightModeEnabled()) ? 'night-mode' : '';
	}


