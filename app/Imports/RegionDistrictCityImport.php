<?php

	namespace App\Imports;

	use App\Models\Region;
	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Support\Collection;
	use Maatwebsite\Excel\Concerns\ShouldAutoSize;
	use Maatwebsite\Excel\Concerns\ToArray;
	use Maatwebsite\Excel\Concerns\ToCollection;
	use Maatwebsite\Excel\Concerns\ToModel;
	use Maatwebsite\Excel\Concerns\WithBatchInserts;
	use Maatwebsite\Excel\Concerns\WithChunkReading;
	use Maatwebsite\Excel\Concerns\RemembersRowNumber;
	use Maatwebsite\Excel\Concerns\WithCustomStartCell;
	use Maatwebsite\Excel\Concerns\WithLimit;
	use Maatwebsite\Excel\Concerns\WithMappedCells;
	use Maatwebsite\Excel\Concerns\WithMapping;
	use Maatwebsite\Excel\Concerns\WithStartRow;

	class RegionDistrictCityImport implements ToModel, WithLimit, ShouldAutoSize, WithMapping, WithBatchInserts, WithStartRow
	{

		private $limitRows = 500;
		private $chunkSize = 500;
		private $batchSize = 500;
		private $startRow = 1;
//		public function array(array $rows)
//		{
//			dump(count($rows));
//			return $rows;
//		}

		public function batchSize(): int
		{
			return $this->batchSize;
		}

		public function chunkSize(): int
		{
			return $this->chunkSize;
		}

		public function limit(): int
		{
			return $this->limitRows;
		}

		public function map($row): array
		{
			return array_slice($row, 0, 3);
		}

		public function model(array $row)
		{
			// TODO: Implement model() method.
		}


		public function startRow(): int
		{
			return $this->startRow;
		}

		/**
		 * @param int $startRow
		 */
		public function setStartRow(int $startRow): void
		{
			$this->startRow = $startRow;
		}

		public function incrementStartRow()
		{
			$this->setStartRow($this->startRow() + $this->chunkSize());
		}
	}
