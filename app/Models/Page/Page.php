<?php

	namespace App\Models\Page;


	use App\Contracts\HasImagesContract;
	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Models\Page\Traits\PageHelpers;
	use App\Traits\Models\HasImages;
	use App\Traits\Models\ImageAttributeTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;
	use App\Traits\Singleton;
	use Illuminate\Support\Arr;

	/**
	 * App\Models\Page
	 *
	 * @property int                                                               $id
	 * @property int|null                                                          $parent_id
	 * @property int|null                                                          $sort
	 * @property mixed                                                             $url
	 * @property bool                                                              $manual
	 * @property int                                                               $active
	 * @property string|null                                                       $image
	 * @property string|null                                                       $page_type
	 * @property string|null                                                       $options
	 * @property \Illuminate\Support\Carbon|null                                   $created_at
	 * @property \Illuminate\Support\Carbon|null                                   $updated_at
	 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
	 * @mixin \Eloquent
	 */
	class Page extends Model implements HasImagesContract, HasLocalized
	{
		use Singleton;
		use ImageAttributeTrait;
		use HasImages;
		use RedirectLangColumn;
		protected $langColumns = ['title', 'description', 'sub_description', 'name'];

		protected $hasOneLangArguments = [PageLang::class];

		private static $pagesByType;

		protected $casts = [
			'manual' => 'boolean',
		];
		protected $guarded = [
			'id',
		];

		protected static $pageTypes = [
			'main'     => 'Main',
			'about'    => 'About',
			'contacts' => 'Contacts',
		];

		public static function getPageTypes()
		{
			return self::$pageTypes;
		}

		public function pageTypeExists($pageType)
		{
			return Arr::exists(self::$pageTypes, $pageType);
		}

		/**
		 * @return mixed
		 */
		public function getUrl()
		{
			$column = 'url';
			return \Arr::get($this->attributes, $column);
		}

		public function getTitle()
		{
			return $this->title;
		}

		public function getDescription()
		{
			return $this->description;
		}

		public function getSubDescription()
		{
			return $this->sub_description;
		}

		public function getRouteUrl()
		{
			if ($this->isManualLink()) {
				return url($this->getUrl());
			}
			return route('page.show', $this->getUrl());
		}

		public function isManualLink()
		{
			return $this->getAttribute('manual');
		}

		public function setPageTypeAttribute($type)
		{
			if (!$this->pageTypeExists($type)) {
				$type = null;
			}
			$this->attributes['page_type'] = $type;
		}

		public static function getManualPagesUrlRoutes(): array
		{
			try {
				$pages = self::where('manual', true)->get('url');
			} catch (\Exception $exception) {
				$pages = [];
			}
			$urls = [];
			foreach ($pages as $page) {
				$urls[] = \Route::get($page->url, 'PageController@manualUrl');
			}
			return $urls;
		}



	}
