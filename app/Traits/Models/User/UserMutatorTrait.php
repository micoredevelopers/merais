<?php

	namespace App\Traits\Models\User;


	trait UserMutatorTrait
	{

		public function getBalance(): int
		{
			return (int)$this->getAttribute('balance');
		}

		public function getFio(): string
		{
			return (string)$this->getAttribute('fio');
		}

		public function getPhone(): string
		{
			return (string)$this->getAttribute('phone');
		}
		public function getEmail(): string
		{
			return (string)$this->getAttribute('email');
		}

		public function getPhoneDisplay(): string
		{
			return (string)extractDigits($this->getAttribute('phone'));
		}

	}
