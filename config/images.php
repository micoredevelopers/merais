<?php
return [
	'default'   => [
		'width'  => 500,
		'height' => 500,
	],
	'watermark' => [
		'path'     => public_path('images/staff/watermark.png'),
		'position' => 'center',
	],
	'sizes'     => [
		'sliders' => [
			'main-page'       => ['width' => '1920', 'height' => '1080'],
			'main-page-about' => ['width' => '1680', 'height' => '945'],
			'about'           => ['width' => '1920', 'height' => '1080'],
		],
	],
];