<?php /** @var $item \App\Models\Service\ServiceCounter */ ?>
<?php /** @var $permissionKey string */ ?>

@includeIf('admin.service-counters.partials.search-bar')

<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>{{ __('form.sorting') }}</th>
            <th class="th-description">@lang('form.title')</th>
            <th>Значение</th>
            <th>{{ __('modules.service.title_singular') }}</th>
            <th class="text-right">
                @can('create_' . $permissionKey)
                    <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
                @endcan
            </th>
        </tr>
        </thead>
        <tbody data-sortable-container="true" data-table="{{ $table ?? $list->isNotEmpty() ? $list->first()->getTable(): '' }}">
        @foreach($list as $item)
            @php
              $trClass = '';
                $canDelete = Gate::allows('delete_'. $permissionKey ?? '');
            	if (!$item->canDelete()){
                	$canDelete = false;
                	$trClass ='';
                }
            @endphp
            <tr class="draggable {{ $trClass }}" data-id="{{ $item->id }}" data-sort="{{ $item->sort }}">
                <td>
                    @include('admin.partials.sort_handle')
                </td>
                <td>
                    <a href="{{ route($routeKey.'.edit', $item->id) }}">{{ $item->key }}</a>
                </td>
                <td>
                    {{$item->getAttribute('value')}}
                </td>
                <td>
                    @if($item->getService())
                        <a href="{{ urlEntityEdit($item->getService()) }}">{{ $item->getService()->getName() }}</a>
                    @endif
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}