@extends('public.layout.app')

@section('content')
    <div class="purchase-container footerNone" id=wrapper__home>
        <div class="container">
            <section class="section-404">
                <div class="container-fluid">
                    <div class="section-404_inner">
                        <div class="error_context">
                            <h3 class="title" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true" >Ошибка 404</h3>
                            <p class="text" data-aos="offsetTop" data-aos-duration="600" data-aos-delay="200" data-aos-once="true">Страницу не найдено</p>
                            <div class="links_container" data-aos="offsetTop" data-aos-duration="600" data-aos-delay="400" data-aos-once="true">
                                <a href="/"> <span>На головную </span> <span><img  class="svg" src="{{ asset('images/icons/arrow.svg') }}" alt=""></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection