<?php

namespace App\Traits\Models;


trait HasGetterNameTrait
{
	public function getName()
	{
		$column = 'name';
		return $this->getAttribute($column);
	}

}