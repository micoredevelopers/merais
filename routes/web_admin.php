<?php

	use App\Http\Controllers\Admin\User\UserController;

	Route::group(
		[
			'prefix'     => LaravelLocalization::setLocale(),
			'middleware' => ['localizationRedirect'],
		], function () {

		Route::group(['prefix' => \Config::get('app.admin-url'), 'namespace' => 'Admin', 'middleware' => ['auth-admin', 'is-admin']], function () {

			Route::resources([
				'sliders'          => 'SliderController',
				'menu'             => 'MenuController',
				'leads'            => 'LeadController',
				'pages'            => 'PageController',
				'admin-menus'      => 'AdminMenuController',
				'services'         => 'Service\\ServiceController',
				'advantages'       => 'AdvantageController',
				'service-counters' => 'Service\\ServiceCountersController',
			], ['as' => 'admin', 'except' => ['show']]
			);
			Route::post('/sliders/item/{slider}', 'SliderController@createSliderItem')->name('admin.sliders.store-item');

			Route::post('/sliders/item/{slider}', 'SliderController@createSliderItem')->name('admin.sliders.store-item');

			Route::resource('feedback', 'Feedback\\FeedbackController', ['only' => ['index', 'destroy'], 'as' => 'admin']);

			Route::resource('menu-group', 'MenuGroupController', ['parameters' => ['menu-group' => 'menuGroup']]);

			Route::group(['namespace' => 'Meta'], function () {
				/** @see \App\Http\Controllers\Admin\Meta\MetaController */
				Route::resource('meta', 'MetaController', ['parameters' => ['meta' => 'meta'], 'as' => 'admin', 'except' => ['show']]);
				Route::resource('redirects', 'RedirectController', ['as' => 'admin', 'except' => ['show']]);

				/** @see \App\Http\Controllers\Admin\Meta\RobotController::index() */
				Route::get('/robots', 'RobotController@index')->name('admin.robots.index');
				Route::put('/robots', 'RobotController@update')->name('admin.robots.update');

				Route::resource('/sitemap', 'SitemapController', ['only' => ['index', 'store'], 'as' => 'admin']);
			});

			Route::group(['prefix' => 'photos'], function () {
				Route::post('/edit', ['uses' => 'PhotosController@edit']);
				Route::post('/delete', ['uses' => 'PhotosController@delete']);
				Route::match(['get', 'post'], '/get-cropper', ['uses' => 'PhotosController@getPhotoCropper']);
			});

			Route::post('/menu/nesting', 'MenuController@nesting')->name('admin.menu.nesting');
		});
	});
