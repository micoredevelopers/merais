<?php

namespace App\Scopes;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class WhereIsPublishedScope implements Scope
{
    protected $column = 'published_at';
    protected $value;

    public function __construct()
    {
        $this->setValue(now());
    }

    public function apply(Builder $builder, Model $model)
    {
        $builder->where($this->column, '<', $this->value);
    }

    /**
     * @param string $column
     * @return $this
     */
    public function setColumn(string $column): self
    {
        $this->column = $column;
        return $this;
    }

    /**
     * @param Carbon $value
     * @return $this
     */
    public function setValue(Carbon $value): self
    {
        $this->value = $value;
        return $this;
    }
}