<?php

namespace App\Traits\Models\User;


use App\User;

trait SuperAdminTrait
{


	public function scopeNotSuperAdmin($query)
	{
		$id = config('permission.super_admin_id');
		return $query->where('id', '!=', $id);
	}

	public function isSuperAdmin(): bool
	{
		return (int)$this->id === (int)config('permission.super_admin_id');
	}

	public function isAdmin(): bool
	{
		static $isAdmin = null;
		if ($isAdmin === null) {
			/** @var  $user User*/
			$user = \Auth::user();
			$isAdmin = ($user AND $user->hasAnyRole(\App\Role::getAllRoles()));
		}
		return $isAdmin;
	}
}