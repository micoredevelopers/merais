<?php

namespace App\Observers\Slider;

use App\Helpers\Media\ImageRemover;
use App\Models\Slider\SliderItem;

class SliderItemObserver
{
	/** @var ImageRemover  */
	private $imageRemover;

	public function __construct(ImageRemover $imageRemover)
	{
		$this->imageRemover = $imageRemover;
	}

	/**
	 * Handle the slider item "created" event.
	 *
	 * @param \App\Models\Slider\SliderItem $sliderItem
	 * @return void
	 */
	public function created(SliderItem $sliderItem)
	{
		//
	}

	/**
	 * Handle the slider item "updated" event.
	 *
	 * @param \App\Models\Slider\SliderItem $sliderItem
	 * @return void
	 */
	public function updated(SliderItem $sliderItem)
	{
		//
	}

	/**
	 * @param SliderItem $sliderItem
	 * @param ImageRemover $imageRemover
	 */
	public function deleted(SliderItem $sliderItem)
	{
		if ($sliderItem->isTypeImage()) {
			$this->imageRemover->removeImage($sliderItem->src);
		}
	}

	/**
	 * Handle the slider item "restored" event.
	 *
	 * @param \App\Models\Slider\SliderItem $sliderItem
	 * @return void
	 */
	public function restored(SliderItem $sliderItem)
	{
		//
	}

	/**
	 * Handle the slider item "force deleted" event.
	 *
	 * @param \App\Models\Slider\SliderItem $sliderItem
	 * @return void
	 */
	public function forceDeleted(SliderItem $sliderItem)
	{
		//
	}
}
