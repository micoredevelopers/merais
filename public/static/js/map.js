function initMap() {
    var map = L.map('contact-map').setView([46.41437320626, 30.726817846298], 14);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    L.marker([46.41654784142846, 30.721651911735538]).addTo(map);
}
initMap();