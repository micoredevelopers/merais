<?php

	namespace App\Models\Lead;

	use App\Models\ModelLang;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	class LeadLang extends ModelLang
	{
		protected $primaryKey = ['lead_id', 'language_id'];

		public function lead(): BelongsTo
		{
			return $this->belongsTo(Lead::class);
		}
	}
