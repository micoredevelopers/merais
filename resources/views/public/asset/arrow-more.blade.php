<svg xmlns="http://www.w3.org/2000/svg" width="13" height="24" viewBox="0 0 13 24" class="svg">
    <defs>
        <linearGradient id="x0g2a" x1="-.62" x2="13.17" y1="10.75" y2="13.78"
                        gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#eb8900"></stop>
            <stop offset=".28" stop-color="#f1a600"></stop>
            <stop offset=".52" stop-color="#f5b800"></stop>
            <stop offset=".7" stop-color="#f6be00"></stop>
            <stop offset=".77" stop-color="#f6b900"></stop>
            <stop offset="1" stop-color="#f5ad00"></stop>
        </linearGradient>
    </defs>
    <g>
        <g>
            <path fill="url(#x0g2a)"
                  d="M.959 24a.793.793 0 0 1-.58-.235.764.764 0 0 1-.237-.563c0-.219.079-.406.237-.563l10.656-10.557L.237 1.384A.778.778 0 0 1 0 .81C0 .583.079.391.237.235a.806.806 0 0 1 .26-.176.799.799 0 0 1 .604 0c.103.039.193.097.272.176l11.39 11.284a.764.764 0 0 1 .237.563.764.764 0 0 1-.237.563L1.54 23.765A.793.793 0 0 1 .96 24z"></path>
        </g>
    </g>
</svg>