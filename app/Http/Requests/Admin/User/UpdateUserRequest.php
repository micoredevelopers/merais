<?php

	namespace App\Http\Requests\Admin\User;

	use App\Contracts\Requests\RequestParameterModelable;
	use App\Http\Requests\User\AbstractUserRequest;
	use App\Traits\Requests\Helpers\GetActionModel;
	use App\User;

	class UpdateUserRequest extends AbstractUserRequest implements RequestParameterModelable
	{
		protected $toIntegers = ['balance'];
		use GetActionModel;

		protected $requestKey = 'user';

		public function rules()
		{
			$phone = $this->getPhoneRule();
			unset($phone['phone']['exists']);
			$phone['phone']['unique'] = 'unique:users,phone';
			if ($this->isActionUpdate() && $user = $this->getActionModel()) {
				/** @var  $user User */
				$phone['phone']['unique'] = 'unique:users,phone,' . $user->id;
			}
			return array_merge(
				$phone,
				['balance' => 'int'],
				$this->getPersonal()
			);

		}

		protected function mergeRequestValues()
		{
			$this->mergeFormatPhone();
		}
	}
