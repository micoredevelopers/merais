<?php


namespace App\Traits\Requests\User;


use App\User;

/**
 * Trait IsPasswordWasSend
 * @package App\Traits\Requests\User
 * applies to Illuminate\Http\Request
 */
trait GetUser
{
	/**
	 * @return User|null
	 */
	public function getUser():?User
	{
		return \Auth::user();
	}
}
