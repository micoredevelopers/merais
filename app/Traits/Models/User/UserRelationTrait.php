<?php

	namespace App\Traits\Models\User;


	use App\Models\Order\Order;
	use App\Models\User\UserDiscount;
	use App\Models\User\UserField;
	use App\Models\User\UserGroup;
	use Illuminate\Database\Eloquent\Relations\HasMany;
	use Illuminate\Support\Collection;

	trait UserRelationTrait
	{
		public function orders(): HasMany
		{
			return $this->hasMany(Order::class);
		}

		/**
		 * @return Collection| Order[]
		 */
		public function getOrders(): ?Collection
		{
			return $this->orders;
		}

	}
