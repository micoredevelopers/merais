<?php /** @var $otherServices \Illuminate\Support\Collection | \App\Models\Service\Service[] */ ?>
<section class="another-services_section">
    <div class="container">
        <div class="another-services_title">
            <div data-aos="clipTop" data-aos-duration="600" data-aos-delay="200" data-aos-once="true" class="aos-init">
                {{ getTranslate('services.others', 'Другие услуги') }}
            </div>
        </div>
        <div class="services-wrap row">
            @foreach($otherServices as $service)
                @php
                    $delay = 200 + (200 * $loop->iteration);
                    $image = storageFileExists($service->getIcon()) ? getPathToImage($service->getIcon()) : asset('images/icons/service.svg');
                @endphp
                <div class="service col-12 col-lg-4 aos-init" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true"
                     data-aos-delay="{{ $delay }}">
                    <div class="service-number">{{ $loop->iteration < 10 ? 0 : '' }}{{ $loop->iteration }}</div>
                    <img class="svg service-icon" src="{{ $image }}" alt="">
                    <div class="service-content">
                        <div class="service-title">{{ $service->getName() }}</div>
                        <div class="service-description">{!! $service->getExcerpt() !!}</div>
                        <a class="service_link" href="{{ route('service.show', $service->getUrl()) }}">
                            <span>{{ getTranslate('global.view-details', 'Подробнее') }}</span>
                            @include('public.asset.arrow-more')
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>