<?php


namespace App\Models\Service\Traits;


use App\Models\Service\Service;
use Illuminate\Support\Arr;

trait ServiceHelpers
{
	public function canDelete(): bool
	{
		return $this->getAttribute('type') === null;
	}

	public function getType(): ?string
	{
		return $this->getAttribute('type');
	}

}