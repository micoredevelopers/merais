<?php /** @var $edit \App\Models\Service\Service */ ?>

@if(isset($edit) && $edit->getCounters()->isNotEmpty())
    @foreach($edit->getCounters() as $counter)
        <a class="badge badge-info" href="{{ urlEntityEdit($counter) }}">{{ $counter->getCounterKey() }} {{ $counter->getCounterValue() }}</a>
    @endforeach
@endif
<div>
    <a href="{{ route('admin.service-counters.create') }}"><i class="fa fa-plus"></i> Добавить </a>
</div>
