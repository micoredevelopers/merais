<?php /** @var $slider \App\Models\Slider\Slider */ ?>


@if(($slider ?? false) && $slider->getSlides()->isNotEmpty())
    <section class="about-slider_section">
        <div class="container position-relative">
            <div class="position-absolute d-none d-md-block" style="bottom: 0; z-index: 5;">
                {!! linkEditIfAdmin($slider, 'Редактировать слайдер', 'danger') !!}
            </div>
            <div class="about-section_slider-wrap" data-aos="clipRight" data-aos-duration="1250" data-aos-once="true">
                <div class="about-section_slider" data-aos="offsetRight" data-aos-duration="1250" data-aos-once="true"
                     data-aos-delay="1450">
                    @foreach($slider->getSlides() as $slide)
                        <div class="about-section_slide">
                            <img src="{{ getPathToImage(imgPathOriginal($slide->getImagePath())) }}">
                        </div>
                    @endforeach
                </div>
                <div class="about-section_slider-pag">
                    <div class="slider-pag_prev-wrap">
                        <img class="svg" src="/images/icons/arrow2.svg"><img class="svg" src="/images/icons/arrow2.svg">
                    </div>
                    <div class="slider-pag_dots"></div>
                    <div class="slider-pag_next-wrap">
                        <img class="svg" src="/images/icons/arrow2.svg"><img class="svg" src="/images/icons/arrow2.svg">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif