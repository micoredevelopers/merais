<?php

	namespace App\View\Components\Dev;

	use App\Helpers\Sidebar\PageUsedSettingsHelper;
	use App\Helpers\Sidebar\PageUsedTranslateHelper;
	use App\Models\Meta;
	use Illuminate\Contracts\Auth\Access\Gate;
	use Illuminate\Support\Str;
	use Illuminate\View\Component;

	class Sidebar extends Component
	{
		private $gate;

		public function __construct(Gate $gate)
		{
			$this->gate = $gate;
		}

		public function render(): string
		{
			if (!isAdmin()) {
				return '';
			}
			$data = [];
			if ($meta = Meta::getMetaData()) {
				$data['meta'] = $meta;
			} else {
				$data['metaUrlCreate'] = getUrlWithoutHost(getNonLocaledUrl());
			}
			$rendered = $this->getRendered();
			$robotsClass = file_exists('robots.txt') ? 'btn-success' : 'btn-danger';
			$sitemapClass = file_exists('sitemap.xml') ? 'btn-success' : 'btn-danger';
			$productionLink = $this->getProductionLink();
			$usersSearch = $this->gate->allows('view_users');
			$settingsSearch = $this->gate->allows('view_settings');
			$usedSettings = $settingsSearch ? PageUsedSettingsHelper::getUsedSettingsList() : [];
			$usedTranslates = $settingsSearch ? PageUsedTranslateHelper::getUsedTranslatesList() : [];
			$canViewLogs = $this->gate->allows('view_logs');
			$data = array_merge($data, compact(
				'robotsClass'
				, 'sitemapClass'
				, 'productionLink'
				, 'usersSearch'
				, 'settingsSearch'
				, 'usedSettings'
				, 'usedTranslates'
				, 'canViewLogs'
				, 'rendered'
			));

			try {
				return view('public.dev.sidebar')
					->with($data)
					->render()
					;
			} catch (\Exception $e) {
				logger()->error($e);
				if (isLocalEnv()) {
					throw $e;
				}
			}
			return '';
		}

		private function getProductionLink(): string
		{
			if (!env('PRODUCTION_APP_URL')) {
				return '';
			}
			return Str::replaceFirst(env('APP_URL'), env('PRODUCTION_APP_URL'), request()->fullUrl());
		}

		private function getRendered()
		{
			return [
				$this->defaultViews(),
			];
		}

		private function defaultViews()
		{
			return '';
		}
	}
