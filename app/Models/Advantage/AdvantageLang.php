<?php

	namespace App\Models\Advantage;

	use App\Models\ModelLang;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	class AdvantageLang extends ModelLang
	{
		protected $primaryKey = ['advantage_id', 'language_id'];

		public function advantage(): BelongsTo
		{
			return $this->belongsTo(Advantage::class);
		}
	}
