<div class="language-toggler">
    <div class="selected-language">{{ $activeLanguage['name'] }}</div>
    @if($arrows)
        <img class="svg language-arrow" src="{{ asset('images/icons/arrow3.svg') }}" alt=""/>
    @endif
    <div class="language-menu">
        @foreach($languages as $localeCode => $language)
            <a class="language-item {{ $language['active'] ? 'active' : ''}}" href="{{ $language['url'] }}" hreflang="{{ $localeCode }}"
               rel="alternate">{{ $language['name'] }}</a>
        @endforeach
    </div>
</div>
