<?php

	namespace App\Mail\Feedback;

	use App\Helpers\Debug\LoggerHelper;
	use App\Mail\MailAbstract;
	use App\Models\Feedback\Feedback;
	use Illuminate\Contracts\Queue\ShouldQueue;

	class FeedbackUserSendFile extends MailAbstract implements ShouldQueue
	{
		protected $feedback;

		public function __construct(Feedback $feedback)
		{
			parent::__construct();
			$this->feedback = $feedback;
		}

		/**
		 * Build the message.
		 *
		 * @return $this
		 */
		public function build()
		{
			try{
				if (settingFileFilesystemPath('feedback.download')) {
					$this->attach(settingFileFilesystemPath('feedback.download'));
				}
				$this->subject('Заявка на обратную связь');
				return $this->from($this->getEmailFrom(), $this->getNameFrom())
					->to($this->feedback->getAttribute('email'))
					->view('mail.feedback.feedback-default')
					->with(['feedback' => $this->feedback])
					;
			} catch (\Exception $e){
			    app(LoggerHelper::class)->error($e);
			}
		}
	}
