<?php

	namespace App\Repositories;

	use App\Criteria\ActiveCriteria;
	use App\Criteria\SortCriteria;
	use App\Models\Gallery\Gallery;
	use App\Models\Gallery\GalleryLang;
	use App\Models\Language;
	use App\Repositories\Admin\LanguageRepository;


	class GalleryRepository extends AbstractRepository
	{
		public function modelLang()
		{
			return GalleryLang::class;
		}

		/**
		 * Specify Model class name
		 *
		 * @return string
		 */
		public function model()
		{
			return Gallery::class;
		}

		public function addPublicCriteriaToQuery(): self
		{
			$this->pushCriteria($this->app->make(SortCriteria::class));
			$this->pushCriteria($this->app->make(ActiveCriteria::class));
			return $this;
		}

		public function addAdminCriteriaToQuery()
		{
			$this->pushCriteria($this->app->make(SortCriteria::class));
		}

		public function getListAdmin()
		{
			$this->addAdminCriteriaToQuery();
			return $this->with('lang')->paginate();
		}

		public function create(array $attributes)
		{
			$languages = $this->app->make(LanguageRepository::class)->getForCreateEntity();
			($entity = $this->makeModel())->fillExisting($attributes)->save();
			foreach ($languages as $language) {
				$entityLang = $this->makeModelLang();
				$entityLang->fillExisting($attributes)
					->associateWithLanguage($language)
				;
				$entityLang->gallery()->associate($entity)->save();
			}

			return $entity;
		}

		public function update(array $attributes, $id)
		{
			$model = ($id instanceof Gallery) ? $id : $this->find($id);
			if (!$model) {
				return null;
			}
			$model->fillExisting($attributes)->save();
			/** @var  $entityLang Gallery */
			if ($entityLang = $model->lang) {
				$entityLang->fillExisting($attributes)->save();
			}
			return $model;
		}

		public function getOneByType(string $type)
		{
			return $this->addPublicCriteriaToQuery()->with('images')->where('type', $type)->first();
		}

		public function getListPublic()
		{
			$this->addPublicCriteriaToQuery();
			return $this->with('lang')->get();
		}

	}

