<?php /** @var $page \App\Models\Page */ ?>
<main id="main-contacts">
    <section class="contacts-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <x-layout.breadcrumbs></x-layout.breadcrumbs>
                    <div class="contacts-wrap">
                        <div class="contacts-title">
                            <div data-aos="clipTop" data-aos-duration="1200" data-aos-delay="200" data-aos-once="true">
                                {{ $page->title }}
                            </div>
                        </div>
                        <div class="phones-wrap">
                            <div class="phones-title" data-aos="offsetTop" data-aos-duration="600" data-aos-delay="600"
                                 data-aos-once="true">
                                {{ getTranslate('contacts.phone', 'Телефон') }}
                            </div>
                            @if(getSetting('global.phone-one'))
                                <a class="phone" href="tel: +{{ formatTel(getSetting('global.phone-one')) }}"
                                   data-aos="offsetTop" data-aos-duration="600"
                                   data-aos-delay="800"
                                   data-aos-once="true"><img class="svg" src="/images/icons/phone.svg"/>
                                    <span>{{ getSetting('global.phone-one') }}</span>
                                </a>
                            @endif
                            @if(getSetting('global.phone-two'))
                            <a class="phone" href="tel: +{{ formatTel(getSetting('global.phone-two')) }}" data-aos="offsetTop" data-aos-duration="600"
                               data-aos-delay="1000"
                               data-aos-once="true"><img class="svg" src="/images/icons/phone.svg"/>
                                <span>{{ getSetting('global.phone-two') }}</span>
                            </a>
                            @endif
                        </div>
                        <div class="mail-wrap">
                            <div class="mail-title" data-aos="offsetTop" data-aos-duration="600" data-aos-delay="1200"
                                 data-aos-once="true">{{ getTranslate('contacts.email', 'Почта') }}
                            </div>

                            <div style="padding: 15px 0;"  data-aos="offsetTop" data-aos-duration="600" data-aos-delay="1400" data-aos-once="true">{{ getTranslate('contacts.email_for_proposals', 'Для пропозицій') }}</div>
                            <a class="mail" href="mailto: {{ getSetting('email.email_for_proposals') }}" data-aos="offsetTop"
                               data-aos-duration="600" data-aos-delay="1400"
                               data-aos-once="true"><img class="svg" src="/images/icons/mail.svg"/>
                                <span>{{ getSetting('email.email_for_proposals') }}</span>
                            </a>

                            <div style="padding: 15px 0;"  data-aos="offsetTop" data-aos-duration="600" data-aos-delay="1600" data-aos-once="true">{{ getTranslate('contacts.email_for_inquiries', 'Для запитів') }}</div>
                            <a class="mail" href="mailto: {{ getSetting('email.public-email') }}" data-aos="offsetTop"
                               data-aos-duration="600" data-aos-delay="1600"
                               data-aos-once="true"><img class="svg" src="/images/icons/mail.svg"/>
                                <span>{{ getSetting('email.public-email') }}</span>
                            </a>

                        </div>
                        <div class="adress-wrap">
                            <div class="adress-title" data-aos="offsetTop" data-aos-duration="600" data-aos-delay="1600"
                                 data-aos-once="true">{{ getTranslate('contacts.address', 'Адрес') }}
                            </div>
                            <a class="adress" href="" data-aos="offsetTop" data-aos-duration="600" data-aos-delay="1800"
                               data-aos-once="true"><img class="svg" src="/images/icons/location.svg"/>
                                <span>{{ getTranslate('contacts.office-address', ' ') }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div
                class="contacts-map_wrap"
                data-aos="clipDown" data-aos-duration="1000" data-aos-delay="2000" data-aos-once="true">
            <div
                    class="contacts-map"
                    id="contact-map"
                    data-aos="offsetDown" data-aos-duration="1000" data-aos-delay="2200" data-aos-once="true"></div>
        </div>
    </section>
</main>

@if($page ?? false)
    {!! editButtonAdmin(urlEntityEdit($page)) !!}
@endif
