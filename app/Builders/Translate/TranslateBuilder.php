<?php

	namespace App\Builders\Translate;

	use App\Models\Translate\Translate;
	use Illuminate\Support\Str;

	class TranslateBuilder implements \ArrayAccess
	{
		private $key = '';

		private $value = '';

		private $type = 'text';

		private $displayName = '';

		/** @var array */
		private $variables = [];

		public function setKey(string $key): self
		{
			$this->key = $key;
			return $this;
		}

		public function setTypeText(): self
		{
			$this->type = Translate::TYPE_TEXT;
			return $this;
		}

		public function setTypeTextarea(): self
		{
			$this->type = Translate::TYPE_TEXTAREA;
			return $this;
		}

		public function setTypeEditor(): self
		{
			$this->type = Translate::TYPE_EDITOR;
			return $this;
		}

		public function setValue(?string $value = null): self
		{
			$this->value = $value;
			return $this;
		}

		public function setDisplayName(string $displayName): self
		{
			$this->displayName = $displayName;
			return $this;
		}

		/**
		 * @param array $variables
		 */
		public function setVariables(array $variables): self
		{
			$this->variables = $variables;
			return $this;
		}

		public function build()
		{
			return [
				'key'          => $this->key,
				'value'        => $this->value,
				'type'         => $this->type,
				'display_name' => $this->displayName,
				'variables'    => $this->variables,
			];
		}

		public function offsetGet($offset)
		{
			return $this->offsetExists($offset) ? $this->{$offset} : null;
		}

		public function offsetExists($offset)
		{
			return property_exists($this, $offset);
		}

		public function offsetSet($offset, $value)
		{
			$method = Str::camel('set' . ucfirst($offset));
			if (method_exists($this, $method)) {
				$this->$method($value);
			}
		}

		public function offsetUnset($offset)
		{
			if ($this->offsetExists($offset)) {
				$this->{$offset} = null;
			}
		}

	}