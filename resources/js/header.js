
function toggleLanguage () {
    const selected = $('.selected-language');
    const selector = $('.language-menu span');
    selected.on('click', function () {
        const menu = $(this).parents('.language-toggler').find('.language-menu');
        const item = $(this);
        item.parents('.language-toggler').find(menu).toggleClass('active');
    })

    selector.on('click', function () {
        const menu = $(this).parents('.language-toggler').find('.language-menu');
        const item = $(this);
        const select =  item.parents('.language-toggler').find('.selected-language');
        const firstElem = menu.find('span').first();
        select.text(item.text());
        item.text(firstElem.text());
        firstElem.html(select.text());
        menu.removeClass('active')
    })
}

toggleLanguage()

function changeHeader() {
    if(window.innerWidth <= 768) {
        $('header').addClass('active');
    }
}

changeHeader()