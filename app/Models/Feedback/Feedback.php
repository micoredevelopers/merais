<?php


	namespace App\Models\Feedback;


	use App\Models\Model;

	class Feedback extends Model
	{
		public function __construct(array $attributes = [])
		{
		    parent::__construct($attributes);
		    $this->setAttribute('type', self::TYPE_FEEDBACK);

		}
		public const TYPE_FEEDBACK = 'feedback';

		protected $guarded = ['id'];

		public function isTypeFeedback(): bool
		{
			return $this->getAttribute('type') === static::TYPE_FEEDBACK;
		}

		public function getPhoneDisplay():string
		{
			return (string)extractDigits($this->getAttribute('phone'));
		}

	}