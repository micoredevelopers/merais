<?php /** @var $orders \Illuminate\Support\Collection | \App\Models\Order\Order[] */ ?>
@isset($orders)
    <div class="card">
        <div class="card-header">
            Заказы пользователя
        </div>
        <div class="card-body">
            @foreach($orders as $order)
                <a href="{{ CRUDLinkByModel($order)->show() }}" class="badge badge-info">#{{ $order->id }}</a>
            @endforeach
        </div>
    </div>
@endisset