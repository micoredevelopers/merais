<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"
        integrity="sha384-1CmrxMRARb6aLqgBO7yyAxTOQE2AKb9GfXnEo760AUcUmFx3ibVJJAzGytlQcNXd"
        crossorigin="anonymous"
></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqWssuOd8vAlM1VuDxsb0GwrVpY3RTtgk"></script>
<script src="{{ asset('static/js/map.js') }}"></script>

<script type="text/javascript" src="{{ mix('static/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/bootstrap-notify.js') }}" defer></script>
