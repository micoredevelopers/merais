<?php

	use App\Models\Advantage\Advantage;
	use App\Repositories\AdvantageRepository;
	use Illuminate\Database\Seeder;

	class AdvantageSeeder extends Seeder
	{
		/**
		 * @var AdvantageRepository
		 */
		private $repository;
		/**
		 * @var \Faker\Factory
		 */
		private $factory;

		public function __construct(AdvantageRepository $repository, \Faker\Generator $factory)
		{
			$this->repository = $repository;
			$this->factory = $factory;
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			/** @see  Advantage */
			$factory = $this->factory;
			$names = [
				'Наш клиент',
				'Люди',
				'Конкуренция',
			];

			foreach (range(0, 2) as $num) {
				$doctor = [
					'name'        => $names[$num] ?? $factory->name,
					'image'       => $factory->imageUrl(),
					'description' => $factory->text(400),
				];
				$this->repository->create($doctor);
			}

		}
	}
