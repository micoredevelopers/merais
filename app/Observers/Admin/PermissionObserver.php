<?php

namespace App\Observers\Admin;

use App\Permission;

class PermissionObserver
{
    /**
     * Handle the permission "created" event.
     *
     * @param  \App\Permission  $permission
     * @return void
     */
    public function created(Permission $permission)
    {
        //
    }

    public function creating(Permission $permission)
    {
        if (!$permission->getAttribute('group')){
        	$groups = explode('_', $permission->getAttribute('name'));
			$group = array_pop($groups);
			$permission->setAttribute('group', $group);
		}
        if (!$permission->getAttribute('display_name')){
	        $name = str_replace('_', ' ', $permission->getAttribute('name'));
	        $name = ucwords($name);
	        $permission->setAttribute('display_name', $name);
        }
    }

    /**
     * Handle the permission "updated" event.
     *
     * @param  \App\Permission  $permission
     * @return void
     */
    public function updated(Permission $permission)
    {
        //
    }

    /**
     * Handle the permission "deleted" event.
     *
     * @param  \App\Permission  $permission
     * @return void
     */
    public function deleted(Permission $permission)
    {
        //
    }

    /**
     * Handle the permission "restored" event.
     *
     * @param  \App\Permission  $permission
     * @return void
     */
    public function restored(Permission $permission)
    {
        //
    }

    /**
     * Handle the permission "force deleted" event.
     *
     * @param  \App\Permission  $permission
     * @return void
     */
    public function forceDeleted(Permission $permission)
    {
        //
    }
}
