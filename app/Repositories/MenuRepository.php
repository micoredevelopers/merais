<?php


namespace App\Repositories;


use App\Helpers\Debug\LoggerHelper;
use App\Models\Menu;
use App\Models\MenuGroup;

class MenuRepository extends AbstractRepository
{
	/**
	 * @inheritDoc
	 */
	public function model()
	{
		return Menu::class;
	}


	protected function initScopes()
	{
		Menu::initScopesPublic();
	}

	/**
	 * @return array|null
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function getMenus($langId): ?array
	{
		$this->initScopes();
		$cacheKey = Menu::$publicMenusCacheKey .'.'. $langId;
		if ((!$menus = \Cache::get($cacheKey))) {
			$menus = [];
			$groups = MenuGroup::all();
			foreach ($groups as $group){
				try{
					$menus[$group->role] = $group->getGroupWithNestedMenu()->menus;
				} catch (\Exception $e){
					app(LoggerHelper::class)->error($e);
					continue;
				}
			}
			try {
				\Cache::set($cacheKey, $menus);
			} catch (\Exception $e) {
				\Cache::set($cacheKey, []);
			}
		}


		return $menus;
	}
}
