<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	 * @var \App\Repositories\UserRepository
	 */
	private $repository;

	public function __construct(\App\Repositories\UserRepository  $repository)
	{

		$this->repository = $repository;
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 0)->create();
        $user  = $this->repository->find((int)config('permission.super_admin_id'));
        if ($user){
        	auth()->login($user);
        }
    }

}
