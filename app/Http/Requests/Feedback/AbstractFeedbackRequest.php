<?php


	namespace App\Http\Requests\Feedback;


	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\AbstractRequest;
	use App\Rules\EmptyOrNotExists;

	abstract class AbstractFeedbackRequest extends AbstractRequest
	{
		public function getMinimalRules(array $except = []): array
		{
			$personal = [
				'email'   => ['required', 'max:255', 'string', 'email:rfc,dns'],
				'phone'   => ['required', 'max:255', 'phone:UA,mobile'],
				'fio'     => ['required', 'string', 'max:255'],
				'message' => ['string', 'max:' . ValidationMaxLengthHelper::TEXT],
				'check'   => [new EmptyOrNotExists()],
			];
			return $this->exceptFields($personal, $except);
		}

		public function getPhoneRule(): array
		{
			return ['required', 'max:255', 'phone:UA,mobile'];
		}

	}
