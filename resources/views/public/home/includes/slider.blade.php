<?php /** @var $slider \App\Models\Slider\Slider */ ?>
@if($slider ?? false)
    <section class="slider-section" data-aos="clipRight" data-aos-duration="1250" data-aos-once="true"
             data-aos-delay="150">
        <div class="position-absolute d-none d-md-block" style="bottom: 0; z-index: 5;">
            {!! linkEditIfAdmin($slider, 'Редактировать слайдер', 'danger') !!}
        </div>
        <div class="main-page_slider-wrap">
            <div class="main-page_slider">
                @if($slider->getSlides()->isNotEmpty())
                    @foreach($slider->getSlides() as $sliderItem)
                        <div class="main-page_slide">
                            <div class="page-slide">
                                <img src="{{ getPathToImage(imgPathOriginal($sliderItem->getImagePath())) }}"
                                     alt="{{ $sliderItem->getName() }}">
                            </div>
                            <div class="container">
                                <div class="slider-section_content" data-aos="offsetRight" data-aos-duration="1250"
                                     data-aos-once="true" data-aos-delay="300">
                                    <h5 class="slider-section_title">{{ $sliderItem->getName() }}</h5>
                                    <div class="slider-section_desc">{{ $sliderItem->getDescription() }}</div>
                                    <a class="slider-section_link" href="{{ langUrl($sliderItem->getLink()) }}"><span>{{ getTranslate('global.see-more', 'Показати ще') }}</span>
                                        <img class="svg" src="{{ asset('images/icons/arrow.svg') }}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="slider-section_pag">
                <div class="slider-pag_index">
                    <div class="slider-pag_current"></div>
                    <span>/</span>
                    <div class="slider-pag_all"></div>
                </div>
                <div class="slider-pag_progress-wrap">
                    <div class="slider-pag_progress"></div>
                </div>
                <div class="slider-arrows">
                    <div class="slider-arrow_prev-wrap">
                        <img class="svg" src="{{ asset('images/icons/arrow.svg') }}"><img class="svg"
                                                                                          src="{{ asset('images/icons/arrow.svg') }}">
                    </div>
                    <div class="slider-arrow_next-wrap">
                        <img class="svg" src="{{ asset('images/icons/arrow.svg') }}"><img class="svg"
                                                                                          src="{{ asset('images/icons/arrow.svg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-section_scroll-img">
            <img class="svg" src="{{ asset('images/icons/scroll.svg') }}"><span>
				{{ getTranslate('global.scroll', "Скролл") }}
			</span>
        </div>
    </section>
@endisset