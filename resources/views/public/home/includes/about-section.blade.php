<?php /** @var $page \App\Models\Page\Page */ ?>
<?php /** @var $sliderAbout \App\Models\Slider\Slider */ ?>

<section class="about-section">
    <div class="container">
        <div class="about-section_name" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true">
            {{ getTranslate('main.about.title', 'О компании ') }}
        </div>
        <div class="about-section_content-wrap">
            @if($page)
                <div class="about-section_content1">
                    <div class="about-section_title" data-aos="offsetTop" data-aos-duration="600" data-aos-once="true"
                         data-aos-delay="200">
                        {{ $page->getTitle() }}
                    </div>
                    <div class="about-section_description">
                        @foreach(explodeByEol($page->getDescription()) as $text)
                            @php
                                $delay = 200 + (200 * $loop->iteration)
                            @endphp
                            <span data-aos="offsetTop" data-aos-duration="600" data-aos-once="true"
                                  data-aos-delay="{{ $delay }}"
                                  data-aos-offset="0">{!! $text !!}</span>
                        @endforeach
                    </div>
                    <a class="about-section_link" href="" data-aos="offsetTop" data-aos-duration="600"
                       data-aos-once="true" data-aos-delay="800"
                       data-aos-offset="0"><span>{{ getTranslate('global.view-details', 'Подробнее') }}</span><img
                                class="svg" src="/images/icons/arrow.svg"></a>
                </div>
                <div class="about-section_content2">
                    <div class="about-section_description">
                        @foreach(explodeByEol($page->getSubDescription()) as $text)
                            @php
                                $delay = 200  * $loop->iteration;
                            @endphp
                            <span data-aos="offsetTop" data-aos-duration="600" data-aos-once="true"
                                  data-aos-delay="{{ $delay }}"
                                  data-aos-offset="0">{!! $text !!}</span>
                        @endforeach
                    </div>
                    <a class="about-section_link" href="" data-aos="offsetTop" data-aos-duration="600"
                       data-aos-once="true"
                       data-aos-delay="800"><span>{{ getTranslate('global.view-details', 'Подробнее') }}</span>
                        <img class="svg" src="/images/icons/arrow.svg">
                    </a>

                    <div class="about-section_statistic position-relative" data-aos="offsetTop" data-aos-duration="600"
                         data-aos-once="true" data-aos-delay="800">
                        @includeIf('public.home.includes.about.statistic')
                        <div class="position-absolute" style="bottom: -30px;">
                            <div class="row">
                                <div class="col">
                                    @includeIf('public.layout.elements.settings-edit', ['key' => 'main-counters.', 'title' => 'Редактировать цифры'])
                                </div>
                                <div class="col">
                                    @includeIf('public.layout.elements.translate-edit', ['key' => 'main-counters.', 'title' => 'Редактировать текст'])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="about-section_content3 advantages">
                <div class="about-section_description">
                    @includeIf('public.home.includes.about.advantages')
                    @includeIf('public.layout.elements.translate-edit', ['key' => 'main-advantages.'])
                </div>
            </div>
        </div>
        @if(($sliderAbout ?? false) && $sliderAbout->getSlides()->isNotEmpty())
            <div class="position-absolute d-none d-md-block" style="bottom: 0; z-index: 5;">
                {!! linkEditIfAdmin($slider, 'Редактировать слайдер', 'danger') !!}
            </div>
            <div class="about-section_slider-wrap" data-aos="clipRight" data-aos-duration="1250" data-aos-once="true">
                <div class="about-section_slider" data-aos="offsetRight" data-aos-duration="1250" data-aos-once="true"
                     data-aos-delay="1450">
                    @foreach($sliderAbout->getSlides() as $slide)
                        <div class="about-section_slide">
                            <img src="{{ getPathToImage(imgPathOriginal($slide->getImagePath())) }}">
                        </div>
                    @endforeach
                </div>
                @if(count($sliderAbout->getSlides()) > 1)
                    <div class="about-section_slider-pag">
                        <div class="slider-pag_prev-wrap">
                            <img class="svg" src="/images/icons/arrow2.svg"><img class="svg" src="/images/icons/arrow2.svg">
                        </div>
                        <div class="slider-pag_dots"></div>
                        <div class="slider-pag_next-wrap">
                            <img class="svg" src="/images/icons/arrow2.svg"><img class="svg" src="/images/icons/arrow2.svg">
                        </div>
                    </div>
                @endif
            </div>
        @endif
    </div>
</section>
