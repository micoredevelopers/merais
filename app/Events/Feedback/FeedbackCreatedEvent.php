<?php

namespace App\Events\Feedback;

use App\Listeners\Feedback\FeedbackCreatedListener;
use App\Models\Feedback\Feedback;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FeedbackCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Feedback
     */
    private $feedback;

    /**
     * Create a new event instance.
     *
     * @see FeedbackCreatedListener
     */
    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }

    public function getFeedback(): Feedback
    {
        return $this->feedback;
    }

    private static function listeners()
    {
        return [
            FeedbackCreatedListener::class,
        ];
    }

}
