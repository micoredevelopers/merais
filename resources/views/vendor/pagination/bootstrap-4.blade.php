@if ($paginator->hasPages())
    <div class="pagination">
        <div class="pag_container">
            @if ($paginator->onFirstPage())
                <div class="pag_item arrow disabled">
                    <a href="" disabled="">
                        <i class="left"></i>
                    </a>
                </div>
            @else
                @if(explode('=',parse_url($paginator->previousPageUrl())['query'])[1] == 1)
                    <a href="{{ url()->current()}}" class="pagin-arrow pagin-prev">
                        <i class="left"></i>
                    </a>
                @else
                    <div class="pag_item">
                        <a href="{{ $paginator->previousPageUrl() }}" class="pagin-arrow pagin-prev">
                            <i class="left"></i>
                        </a>
                    </div>

                @endif
            @endif

            <div class="pagin-num-box d-flex justify-content-between mx-4">
                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="pag_item"><span class="pagination-ellipsis"><span>{{ $element }}</span></span></li>
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="pag_item active">
                                    <a class="" href="">{{ $page }}</a>
                                </li>
                            @else
                                @if($page == 1)
                                    <li class="pag_item">
                                        <a class="" href="{{ url()->current() }}">{{ $page }}</a>
                                    </li>
                                @else
                                    <li class="pag_item">
                                        <a class="" href="{{ $url }}">{{ $page }}</a>
                                    </li>
                                @endif
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </div>
            @if ($paginator->hasMorePages())
                <div class="pag_item">
                    <a href="{{ $paginator->nextPageUrl() }}" class="pagin-arrow pagin-next">
                        <i class="right"></i>
                    </a>
                </div>
            @else
                <div class="pag_item">
                    <a href="" class="pagin-arrow pagin-next" disabled>
                        <i class="right"></i>
                    </a>
                </div>

            @endif
        </div>
    </div>
@endif

