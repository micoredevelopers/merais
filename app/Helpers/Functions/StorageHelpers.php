<?php
if (!function_exists('getStorageFilePath')) {
	function getStorageFilePath($filePath, $disk = null)
	{
		return \App\Helpers\StorageHelper::filePath($filePath, $disk);
	}
}
if (!function_exists('storageFileExists')) {
	function storageFileExists($filePath, $disk = null)
	{
		return \App\Helpers\StorageHelper::fileExists($filePath, $disk);
	}
}
if (!function_exists('storageFilemtime')) {
	function storageFilemtime($filePath, $disk = null)
	{
		return \App\Helpers\StorageHelper::lastModified($filePath, $disk);
	}
}
if (!function_exists('storageDelete')) {
	function storageDelete($filePath, $disk = null)
	{
		return \App\Helpers\StorageHelper::delete($filePath, $disk);
	}
}
if (!function_exists('storageDisk')) {
	function storageDisk($disk = null)
	{
		return \Storage::disk($disk);
	}
}
