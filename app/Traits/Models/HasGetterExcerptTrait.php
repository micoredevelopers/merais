<?php

namespace App\Traits\Models;


use App\Contracts\HasLocalized;

trait HasGetterExcerptTrait
{

    /**
     * @param string $column
     * @return mixed|string
     */
    public function getExcerpt()
    {
    	$column = 'excerpt';
	    return $this->getAttribute($column);
    }

}