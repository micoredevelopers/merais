<?php

	namespace App\Criteria\Service;

	use App\Containers\Admin\Service\CounterSearchDataContainer;
	use Illuminate\Database\Eloquent\Builder;
	use Illuminate\Database\Eloquent\Model;
	use Prettus\Repository\Contracts\CriteriaInterface;
	use Prettus\Repository\Contracts\RepositoryInterface;

	/**
	 * Class ActiveCriteria.
	 *
	 * @package namespace App\Criteria;
	 */
	class ServiceCounterFilterCriteria implements CriteriaInterface
	{
		/**
		 * @var SearchDataContainer
		 */
		private $searchDataContainer;

		public function __construct(CounterSearchDataContainer $searchDataContainer)
		{
			$this->searchDataContainer = $searchDataContainer;
		}

		/**
		 * Apply criteria in query repository
		 *
		 * @param string | Model      $model
		 * @param RepositoryInterface $repository
		 *
		 * @return mixed
		 */
		public function apply($model, RepositoryInterface $repository)
		{
			if ($search = $this->searchDataContainer->getSearch()) {
				$model = $model->where(function (Builder $builder) use ($search) {
					$builder->whereLike('key', $search)
						->orWhereHas('lang', function (Builder $builder) use ($search) {
							return $builder->whereLike('value', $search);
						})
					;
				});
			}
			if ($serviceId = $this->searchDataContainer->getServiceId()) {
				$model = $model->where('service_id', $serviceId);
			}
			return $model;
		}
	}
