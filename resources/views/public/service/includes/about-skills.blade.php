<section class="section-about_skill">
    <div class="container">
        @includeIf('public.layout.elements.translate-edit', ['key' => 'service-detail.'])
        <div class="about-skill_title">
            <div data-aos="clipTop" data-aos-duration="600" data-aos-delay="200" data-aos-once="true"
                 class="aos-init">{{ $service->getNameSecond() }}</div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="about-skill_desc">
                    @foreach(explodeByEol($service->getTextFirst()) as $text)
                        @php
                            $delay = 200 * $loop->index;
                        @endphp
                        <span data-aos="offsetTop" data-aos-duration="600"
                              data-aos-once="true"
                              data-aos-delay="{{ $delay }}" class="aos-init">{!! $text !!}</span>
                    @endforeach
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="about-skill_desc">
                    @foreach(explodeByEol($service->getTextSecond()) as $text)
                        @php
                            $delay = 600 + (200 * $loop->index);
                        @endphp
                        <span data-aos="offsetTop" data-aos-duration="600"
                              data-aos-once="true"
                              data-aos-delay="{{ $delay }}" class="aos-init">{!! $text !!}</span>
                    @endforeach
                </div>
            </div>
            <div class="col-12">

                <img class="about-skill_img aos-init" data-aos="clipRight" data-aos-duration="600"
                     data-aos-delay="1200"
                     data-aos-once="true"
                     src="{{ getPathToImage($service->getBanner(), 'images/slide1.jpg') }}">
            </div>
        </div>
    </div>
</section>