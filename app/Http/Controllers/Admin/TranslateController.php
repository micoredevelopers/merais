<?php

	namespace App\Http\Controllers\Admin;

	use App\Helpers\Debug\LoggerHelper;
	use App\Models\Language;
	use App\Models\Translate\Translate;
	use App\Models\Translate\TranslateLang;
	use App\Repositories\TranslateRepository;
	use App\Traits\Authorizable;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;

	class TranslateController extends AdminController
	{
		use Authorizable;

		private $name;
		protected $tb;

		protected $routeKey = 'translate';

		protected $permissionKey = 'translate';

		public function __construct()
		{
			parent::__construct();
			$this->name = __('modules.localization.title');
			$this->tb = 'translate';
			$this->addBreadCrumb($this->name, $this->resourceRoute('index'));

			$this->shareViewModuleData();
		}

		/**
		 * @param Request             $request
		 * @param TranslateRepository $translateRepository
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index(Request $request, TranslateRepository $translateRepository)
		{
			if ($request->has('seed')) {
				seedByClass('TranslateTableSeeder');
				return redirect($this->resourceRoute('index'));
			}
			$data = [];
			$vars['list'] = $translateRepository->getForAdminDisplay($request);

			$groups = $vars['list']->pluckDistinct('group');
			$groups = collect($groups);
			$vars = array_merge($vars, compact('request'));
			$this->setTitle($this->name);
			$active = request()->session()->get('setting_tab', old('setting_tab', ($groups->first())));
			$vars = array_merge($vars, compact(
				'active'
				, 'groups'
			));
			if (view()->exists('admin.translate.index')) {
				$data['content'] = view('admin.translate.index', $vars);
			}

			return $this->main($data);
		}

		/**
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function create()
		{
			$vars['groups'] = Translate::getGroups();
			$this->addBreadCrumb(__('form.creating'));
			$vars['controller'] = $this->tb;
			$data['content'] = view('admin.translate.add')->with($vars);
			return $this->main($data);
		}

		/**
		 * @param Request $request
		 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 */
		public function store(Request $request)
		{
			$data = $request->all();
			$translate = new Translate();
			if ($translate->fillExisting($data)->save()) {
				$this->setSuccessStore();
				$data[ $translate->getForeignKey() ] = $translate->getKey();
				foreach ($this->languagesList as $language) {
					/** @var $language Language */
					$data[ $language->getForeignKey() ] = $language->getKey();
					(new TranslateLang())->fillExisting($data)->save();
				}
			}

			return $this->redirectOnCreated($translate);
		}

		/**
		 * @param Request $request
		 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 */
		public function update(Request $request)
		{
			if ($request->has('translate')) {
				$ids = array_keys($request->get('translate'));
				$this->setSuccessUpdate();
				$translates = Translate::with('lang')->find($ids);
				$translateRequest = $request->get('translate');
				foreach ($translates as $translate) {
					/** @var $translate TranslateLang */
					try{
						$id = $translate->getKey();
						$data = \Arr::get($translateRequest, $id);
						$translate->fillExisting($data)->save();
						$translate->lang->fillExisting($data)->save();
						//touch for observer works
						if (!$translate->wasChanged() && $translate->lang->wasChanged()) {
							$translate->touch();
						}
					} catch (\Exception $e){
					    app(LoggerHelper::class)->error($e);
					}
				}
			}
			request()->flashOnly('setting_tab');
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());

		}

	}
