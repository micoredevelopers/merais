<?php

	use App\Builders\Migration\MigrationBuilder;
	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateServices extends Migration
	{

		/**
		 * @var MigrationBuilder
		 */
		private $builder;

		private $table = 'services';

		private $foreignKey = 'service_id';

		private $tableLang = 'service_langs';

		public function __construct()
		{
			$this->builder = app(MigrationBuilder::class);
		}

		public function models()
		{
			return [
				\App\Models\Service\Service::class,
				\App\Models\Service\ServiceLang::class,
			];
		}

		public function up()
		{
			Schema::create($this->table, function (Blueprint $table) {
				$this->builder->setTable($table);

				$table->id();
				$this->builder
					->createUrl()
					->createImage()
					->createSort()
					->createActive()
					->createType()
					->createIntPrice()
				;
				$table->timestamps();
			});


			Schema::create($this->tableLang, function (Blueprint $table) {
				$this->builder->setTable($table);
				$table->id();
				$table->unsignedBigInteger($this->foreignKey);

				$this->builder
					->addBelongsTo($this->foreignKey, $this->table)
					->createName()
					->createExcerpt()
					->createDescription()
					->createLanguageKey()
				;
			});
		}

		public function down()
		{
			Schema::dropIfExists($this->tableLang);
			Schema::dropIfExists($this->table);
		}
	}
