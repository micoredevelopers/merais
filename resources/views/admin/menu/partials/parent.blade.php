<?php /** @var $menu \App\Models\Menu */ ?>
<?php /** @var $edit \App\Models\Model */ ?>
@isset($menus)
	@php
		$name = $name ?? 'title';
	@endphp
	{!! errorDisplay('parent_id') !!}
	<label for="parent_id">{{ __('modules._.parent') }}</label>
	<select name="parent_id" id="parent_id" class="form-control selectpicker" data-live-search="true">
		<option value="">None</option>
		@foreach($menus->where('menu_group_id', $group->id) as $menu)
			@php
				if (isset($edit) AND $edit->getAttribute('id') === $menu->getKey()){
					continue;
				}
				$selected = (isset($edit)) ? $edit->getAttribute('parent_id') == $menu->getKey() : false;
			@endphp
			<option value="{{ $menu->getKey() }}" {!! selectedIfTrue($selected) !!}
			>{{ $menu->getName() }}
			</option>
		@endforeach
	</select>
@endisset
