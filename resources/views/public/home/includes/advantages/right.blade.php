<?php /** @var $advantage \App\Models\Advantage\Advantage */ ?>
@if($advantage ?? false)
    <div class="information-wrap row">
        <div class="col-12 col-lg-6 d-flex align-items-center information-content_wrap">
            <div class="information-content">
                <div class="information-title">
                    <div data-aos="clipTop" data-aos-duration="1200" data-aos-once="true">
                        {{ $advantage->getName() }}
                    </div>
                </div>
                @includeIf('public.home.includes.advantages.edit')
                <div class="information-description" data-aos="offsetTop" data-aos-duration="600"
                     data-aos-once="true" data-aos-delay="400">
                    {{ $advantage->getDescription() }}
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 information-img_wrap d-flex justify-content-end" data-aos="clipLeft"
             data-aos-duration="600" data-aos-once="true" data-aos-delay="400">
            <img class="information-img" src="{{ getPathToImage(imgPathOriginal($advantage->image)) }}">
        </div>
    </div>
@endif