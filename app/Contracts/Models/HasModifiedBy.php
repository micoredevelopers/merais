<?php


namespace App\Contracts\Models;


interface HasModifiedBy
{

	public function getKey();

	public function getForeignKey();

	public function modifiable();

}