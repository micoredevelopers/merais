<?php

use App\Events\Admin\MenusChanged;
use App\Models\Menu;
use App\Models\MenuGroup;
use App\Models\MenuLang;
use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
	private $languages;

	public function __construct()
	{
		$this->languages = \App\Models\Language::all();
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$menuGroups = [
			'main_menu'   => [
				['name' => 'Услуги', 'url' => 'services'],
				['name' => 'Контейнерные перевозки', 'url' => 'container-shipping'],
				['name' => 'Наземные перевозки (ЖД/авто)', 'url' => 'ground-transportations'],
				['name' => 'Негабариты', 'url' => 'over-sized'],
				['name' => 'Консалтинговые услуги', 'url' => 'consulting-services'],
				['name' => 'Доставка малых партий', 'url' => 'delivery-small-qty'],
			],
			'footer_menu' => [
				['name' => 'О компании', 'url' => 'about'],
				['name' => 'Услуги', 'url' => 'services'],
				['name' => 'Контакты', 'url' => 'contacts'],
			],
		];
		$menuGroups = $this->arrayReverseRecursive($menuGroups);
		foreach ($menuGroups as $role => $menus) {
			if (!$menus) {
				continue;
			}
			$group = MenuGroup::getByRoleName($role);
			if (!$group) {
				continue;
			}
			foreach ($menus as $item) {
				$item = $this->fillDefaults($item);
				$menu = $this->saveMenu($item, $group);
				if ($this->hasChildrens($item)) {
					foreach ($this->getChildrens($item) as $itemChild) {
						$itemChild = $this->fillDefaults($itemChild);
						$itemChild['parent_id'] = $menu->id;
						$this->saveMenu($itemChild, $group);
					}
				}
			}
		}


		event(new MenusChanged());
	}

	private function saveMenu(array $item, MenuGroup $menuGroup): Menu
	{
		($menu = new Menu())->fillExisting($item)
			->menuGroup()->associate($menuGroup)->save()
		;
		try{
			foreach ($this->languages as $language) {
				($menuLang = new MenuLang())->fillExisting($item);
				$menuLang->menu()->associate($menu);
				$menuLang->associateWithLanguage($language)
					->save()
				;
			}
		} catch (\Exception $e){
		}
		return $menu;
	}

	private function hasChildrens(array $menu): bool
	{
		return \Illuminate\Support\Arr::has($menu, 'childrens');
	}

	private function getChildrens(array $menu): array
	{
		return \Illuminate\Support\Arr::get($menu, 'childrens', []);
	}

	// for remove inspection warning - using merge in loop
	private function arrayMerge()
	{
		return array_merge(...func_get_args());
	}

	private function fillDefaults(array $menu): array
	{
		static $now;
		if (null === $now) {
			$now = now();
		}
		$default = ['active' => 1, 'created_at' => $now, 'updated_at' => $now];
		return array_merge($default, $menu);
	}


	private function arrayReverseRecursive($arr)
	{
		foreach ($arr as $key => $val) {
			if (is_array($val)) {
				$arr[$key] = $this->arrayReverseRecursive($val);
			}
		}
		return array_reverse($arr);
	}
}
