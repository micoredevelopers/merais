<?php

use App\Models\Service\Service;
use App\Models\Service\ServiceLang;
use App\Repositories\ServiceRepository;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{

	/**
	 * @var ServiceRepository
	 */
	private $repository;
	/**
	 * @var \Faker\Generator
	 */
	private $factory;

	public function __construct(ServiceRepository $repository, \Faker\Generator $factory)
	{
		$this->factory = $factory;
		$this->repository = $repository;
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$services = [
			[
				'name' => 'Морские перевозки',
				'url'  => 'container-shipping',
			], [
				'name' => 'Авиа перевозки',
				'url'  => 'avia-shipping',
			], [
				'name' => 'Наземные перевозки',
				'url'  => 'ground-shipping',
			], [
				'name' => 'Транспортно-экспедиторское обслуживание',
				'url'  => 'expeditor-service',
			],
		];
		$services = array_reverse($services);
		foreach ($services as $service) {
			$this->repository->create($service);
		}
	}

	private function models(): void
	{
		[
			Service::class,
			ServiceLang::class,
		];
	}
}
