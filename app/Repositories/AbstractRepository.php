<?php


namespace App\Repositories;


use App\Contracts\HasLocalized;
use App\Models\Language;
use App\Models\Model;
use App\Models\ModelLang;
use App\Repositories\Admin\LanguageRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Prettus\Repository\Eloquent\BaseRepository;

abstract class AbstractRepository extends BaseRepository
{
	public function findOneByField($field, $value = null, $columns = ['*']): ?\Illuminate\Database\Eloquent\Model
	{
		$this->applyCriteria();
		$this->applyScope();
		$model = $this->model->where($field, '=', $value)->first($columns);
		$this->resetModel();

		return $this->parserResult($model);
	}

	protected function getLanguagesList(): Collection
	{
		return Language::all();
	}

	public function WhereIsPublished($column = 'published_at')
	{
		return $this->where($column, now(), '<');
	}

	public function whereUrl(string $url): Builder
	{
		return $this->where('url', $url);
	}

	public function findByUrl(string $url)
	{
		return $this->whereUrl($url)->first();
	}

	public function makeModelLang(): ?Model
	{
		return $this->app->make($this->modelLang());
	}

	public function update(array $attributes, $id)
	{
		$methodExists = (is_object($id) && method_exists($id, 'fillExisting'));
		$isModel = ($id instanceof Model);

		$model = ($methodExists || $isModel) ? $id : $this->find($id);
		if (!$model) {
			return null;
		}
		$model->fillExisting($attributes)->save();
		/** @var  $entityLang ModelLang */
		if (classImplementsInterface($model, HasLocalized::class) && $entityLang = $model->lang) {
			$entityLang->fillExisting($attributes)->save();
		}
		return $model;
	}

	public function create(array $attributes)
	{
		($entity = $this->makeModel())->fillExisting($attributes)->save();
		if (!$entity) {
			return $entity;
		}
		if (classImplementsInterface($entity, HasLocalized::class)) {
			$languages = $this->app->make(LanguageRepository::class)->getForCreateEntity();
			foreach ($languages as $language) {
				$attributes[$entity->getForeignKey()] = $entity->getAttribute($entity->getKeyName());
				$entityLang = $this->makeModelLang();
				if ($entityLang) {
					$entityLang->fillExisting($attributes)
						->language()->associate($language)
					;
					$entityLang->save();
				}
			}
		}

		return $entity;
	}

	/**
	 * @return Builder
	 */
	public function getBuilder()
	{
		/** @var  $builder Builder */
		$builder = $this;
		return $builder;
	}

}
