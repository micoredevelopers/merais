<?php

	use App\User;
	use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Permission;
use Spatie\Permission\PermissionRegistrar;

class RolesAndPermissionsSeeder extends Seeder
{
	private $actions = [
		'view',
		'add',
		'edit',
		'delete',
	];

	public function run()
	{
// Reset cached roles and permissions
		app(PermissionRegistrar::class)->forgetCachedPermissions();

// create permissions

		$permissions = $this->getExistsPermissions();
		foreach ($permissions as $permission) {
			(new Permission(['name' => $permission]))->save();
		}

// create roles and assign created permissions
		$roleWriter = Role::create(['name' => 'writer']);
//		$roleWriter->givePermissionTo('edit_articles', 'view_articles');

// or may be done by chaining
		$roleModerator = Role::create(['name' => 'moderator'])->givePermissionTo($this->getRolesModerator());
		if (User::MODERATOR_USER_ID AND $moderator = User::find(User::MODERATOR_USER_ID)) {
			$moderator->assignRole($roleModerator);
		}
		$roleModerator = Role::create(['name' => 'tester'])->givePermissionTo($this->getRolesModerator());

		$role = Role::create(['name' => 'admin'])->givePermissionTo(Permission::all());
		if (User::SUPER_ADMIN_ID) {
			User::find(User::SUPER_ADMIN_ID)->assignRole($role);
		}
	}

	/**
	 * @return array
	 */
	private function getRolesModerator()
	{
		$permissions = [];
		$permissions = array_merge($permissions, $this->_getPermissionModify('settings'));
		$permissions = array_merge($permissions, $this->_getPermissionModify('translate'));
		$permissions = array_merge($permissions, $this->_getPermissionModify('sliders'));
		$permissions = array_merge($permissions, $this->_getPermissionModify('pages'));
//
		$permissions = array_merge($permissions, $this->_getPermissionCrud('leads'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('advantages'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('services'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('service-counters'));
		$permissions = array_merge($permissions, $this->_getPermissionCrud('menu'));
//		$permissions = array_merge($permissions, $this->_getPermissionCrud('meta'));
		$permissions = array_merge($permissions, $this->_getPermission('feedback', ['view', 'delete']));
		$permissions = array_merge($permissions, $this->_getPermission('index', 'view'));

		return $permissions;
	}

	private function getExistsPermissions()
	{
		$permissions = [];
		$permissions[] = 'view_index';
		$create = [
			'users',
			'roles',
			'settings',
			'sliders',
			'menu',
			'meta',
			'translate',
			'pages',
			'redirect',
			'services',
			'service-counters',
			'leads',
			'feedback',
			'advantages',
		];

		foreach ($create as $entity) {
			foreach ($this->_getPermissionCrud($entity) as $perm) {
				$permissions[] = $perm;
			}
		}

		return $permissions;
	}

	private function _getPermission($entity, $permissions)
	{
		$res = [];
		foreach ((array)$permissions as $permission) {
			$res[] = $permission . '_' . $entity;
		}
		return $res;
	}

	private function _getPermissionCrud($entity, $except = [])
	{

		$perms = $this->actions;
		if ($except = (array)$except) {
			$perms = array_diff($perms, $except);
		}
		return $this->_getPermission($entity, $perms);
	}

	private function _getPermissionModify($entity)
	{
		$perms = [
			'view',
			'edit',
		];
		return $this->_getPermission($entity, $perms);
	}
}
