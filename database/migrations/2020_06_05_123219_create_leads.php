<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeads extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'leads';

   private $foreignKey = 'lead_id';

   private $tableLang = 'lead_langs';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }

	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);

			$table->id();
			$this->builder->createImage()
				->createSort()
				->createActive()
			;
			$table->timestamps();
		});

		Schema::create($this->tableLang, function (Blueprint $table) {
			$this->builder->setTable($table);
			$table->id();
			$table->unsignedBigInteger($this->foreignKey);

			$this->builder
				->createName()
				->createDescription()
				->createNullableString('subscription')
				->createLanguageKey()
				->addBelongsTo($this->foreignKey, $this->table)
			;
		});
	}


    public function down()
    {
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
