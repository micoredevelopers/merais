<?php
/**
 * Created by PhpStorm.
 * User: aljajazva
 * Date: 2019-04-12
 * Time: 12:04
 */

namespace App\Helpers;


class ResponseHelper
{
    const ERROR_KEY = 'error';
    const STATUS_KEY = 'status';
    const SUCCESS_KEY = 'success';
    const MESSAGE_KEY = 'message';
}