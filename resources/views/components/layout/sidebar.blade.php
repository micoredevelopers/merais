<?php /** @var $mainMenu \Illuminate\Support\Collection | \App\Models\Menu[] */ ?>
<div class="aside-menu">
    <div class="menu-nav_wrapper" data-aos="offsetDown" data-aos-delay="150" data-aos-duration="1250" data-aos-once="true">
        <img class="aside-menu_logo" src="{{ asset('images/logo1.png') }}" alt=""/>
        <x-layout.languages :arrows="true"/>
        <div class="menu-nav_menu-toggle">
            <img class="svg" src="{{ asset('images/icons/menu.svg') }}" alt=""/>
            <span>{{ getTranslate('global.menu', 'Меню') }}</span>
        </div>
        <div class="menu-nav_snetworks">
            <a class="menu-nav_snetwork" href="{{ getSetting('socials.facebook') }}" target="_blank">Facebook</a>
            <a class="menu-nav_snetwork" href="{{ getSetting('socials.instagram') }}" target="_blank">Instagram</a>
        </div>
    </div>
    @if(isset($mainMenu) && $mainMenu->isNotEmpty())
        <div class="menu-content">
            <img class="menu-logo" src="{{ asset('images/logo1.png') }}" alt=""/>
            <div class="menu-items">
                @foreach($mainMenu as $menu)
                    <a class="menu_item" href="{{ langUrl("services/" . $menu->getUrlAttribute())  }}">
                        <span>{{ $menu->getName() }}</span>
                        @if(storageFileExists($menu->image))
                            <img class="menu-hoisting_photo" src="{{ getPathToImage($menu->image)}}" alt=""/>
                        @endif
                    </a>
                @endforeach
            </div>
            <div class="menu2-items">
                <a class="menu_item2 menu_item2-about"
                   href="{{ langUrl('/about') }}">{{ getTranslate('pages.about', 'Про нас') }}</a>
                <a class="menu_item2 menu_item2-contacts"
                   href="{{ langUrl('/contacts') }}">{{ getTranslate('pages.contacts', 'Контакти') }}</a>
            </div>
            <div class="menu-contacts">
                @if(getSetting('global.phone-one'))
                    <a class="header-phone"
                       href="tel: +{{ formatTel(getSetting('global.phone-one')) }}"><span>{{ getSetting('global.phone-one') }}</span></a>
                @endif
                <div class="menu-nav_snetworks ">
                    @if(getSetting('socials.facebook'))
                        <a class="menu-nav_snetwork" href="{{ getSetting('socials.facebook') }}" rel="nofollow"
                           target="_blank">Facebook</a>
                    @endif
                    @if(getSetting('socials.instagram'))
                        <a class="menu-nav_snetwork" href="{{ getSetting('socials.instagram') }}" rel="nofollow"
                           target="_blank">Instagram</a>
                    @endif
                </div>
                <button class="header-btn" type="button" data-toggle="modal" data-target="#callback-modal">
                    {{ getTranslate('contacts.connect_with_us', "Зв`язатися з нами") }}
                </button>
            </div>
        </div>
    @endif
</div>