<?php /** @var $services \Illuminate\Support\Collection | \App\Models\Service\Service[] */ ?>

@if(isset($services) && $services->isNotEmpty())
    <section class="services-section">
        <div class="container">
            <div class="services-section_title">
                <div data-aos="clipTop" data-aos-duration="1200" data-aos-once="true">
                    {{ getTranslate('services.title', 'Услуги') }}
                </div>
            </div>
            <div class="services-wrap row">
                @foreach($services as $service)
                    @php
                            $delay = 200 + (200 * $loop->iteration)
                    @endphp
                    <div class="service col-12 col-lg-4" data-aos="offsetTop" data-aos-duration="600"
                         data-aos-once="true" data-aos-delay="{{ $delay }}">
                        <div class="service-number">{{ $loop->iteration < 10 ? 0 : '' }}{{ $loop->iteration }}</div>
                        <img class="svg service-icon"
                             src="{{ storageFileExists($service->getIcon()) ? getPathToImage($service->getIcon()) :asset('images/icons/service.svg') }}">
                        <div class="service-content">
                            <div class="service-title">{{ $service->getName() }}</div>
                            <div class="service-description">{!! $service->getDescription() !!}</div>
                        </div>
                        <a class="service_link" href="{{ route('service.show', $service->getUrl()) }}">
                            <span>{{ getTranslate('global.view-details', 'Подробнее') }}</span>
                            <img class="svg" src="{{ asset('images/icons/arrow.svg') }}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
