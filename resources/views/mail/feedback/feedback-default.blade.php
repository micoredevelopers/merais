@extends('mail.layout.layout')

@section('title')
    Обратная связь
@stop

@section('content')
	<?php /** @var $feedback \App\Models\Feedback\Feedback */ ?>
    @php
        $keys = [
            'name', 'phone', 'email', 'fio', 'message'
        ];
    @endphp

    {!! getSetting('feedback.head-text') !!}

    <table class="table table-striped" style="max-width: 800px">
        <tbody>
        @foreach($keys as $key)
            @if( $feedback->getAttribute($key) )
                <tr>
                    <td>{{ getTranslate('forms.' . $key) }}</td>
                    <td>{{ $feedback->getAttribute($key) }}</td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@stop
