<?php

	use App\Http\Controllers\HomeController;

	Route::group(
		[
			'prefix'     => LaravelLocalization::setLocale(),
			'middleware' => ['localizationRedirect'],
		], function () {

		Route::get('/login', 'Auth\LoginController@redirect')->name('login');
		/** @see HomeController */
		Route::get('/', 'HomeController@index')->name('home');
		Route::get('/about', 'AboutController@index')->name('about');
		Route::get('/contacts', 'ContactController@index')->name('contacts');
		Route::get('/test', 'HomeController@test')->name('test');
		Route::post('feedback', 'FeedbackController@feedback')->name('feedback');

		Route::get('/pages/{page}', 'PageController@default')->name('pages.default');
		Route::get('/services/{service}', 'ServiceController@show')->name('service.show');
		Route::get('/services', 'ServiceController@index')->name('service.index');
	});
