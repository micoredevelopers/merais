<?php

use Illuminate\Database\Seeder;

class MetaTableSeeder extends AbstractLanguageableSeeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$metas = [
			['url' => '*', 'active' => 1, 'meta_title' => 'Бессарабский дворик'],
		];
		foreach ($metas as $meta) {
			($metaModel = new \App\Models\Meta())->fillExisting($meta)->save();
			/** @var  $language \App\Models\Language */
			foreach ($this->languages as $language) {
				($metaLang = new \App\Models\MetaLang())->fillExisting($meta)->meta()->associate($metaModel);
				$metaLang->language()->associate($language)->save();
			}
		}
	}
}
